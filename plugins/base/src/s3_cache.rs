use crate::cfg::{Config, S3Cache, JSON_LEN_BUFFER_SIZE};
use crate::get_s3_client;
use anyhow::{anyhow, Result};
use aws_sdk_s3::error::{ProvideErrorMetadata, SdkError};
use aws_sdk_s3::operation::get_object::GetObjectError;
use aws_sdk_s3::primitives::ByteStream;
use aws_sdk_s3::types::error::NoSuchKey;
use bytes::Bytes;
use std::fmt::Debug;
use tokio::io::{AsyncRead, AsyncReadExt};

use futures::{FutureExt, TryFutureExt};
use hex;
use hyper::http::{HeaderName, HeaderValue};
use imaginator::cfg::config_ref;
use imaginator::filter::{exec_filter, Args, Context, FilterArg, FilterResult, Future, NotAnImage};
use imaginator::img::Image;
use imaginator::metrics::CACHE_HIT_COUNTER;
use imaginator::HardwareLocker;
use serde_json;
use sha1::{Digest, Sha1};
use std::fmt::{self, Formatter};
use std::io::Write;
use thiserror::Error;
use tracing::{error, Instrument};

#[derive(Error, Debug)]
enum S3CacheError {
    #[error("This entry {0} is not present in the cache.")]
    NotInCache(String),
    #[error("Could not save to s3 cache: {0}.")]
    S3ErrorSave(String, #[source] Box<dyn std::error::Error + Send + Sync>),
    #[error("Could not get object from s3 cache: {0}.")]
    S3ErrorGet(
        &'static str,
        #[source] Box<dyn std::error::Error + Send + Sync>,
    ),
    #[error("Could not obtain cache size")]
    CacheSizeError,
}
#[derive(Serialize, Deserialize, PartialEq, Clone, Debug)]
struct CacheMetadata {
    content_type: String,
    dpi: Option<(f64, f64)>,
}
#[derive(Clone)]
struct CacheEntry {
    metadata: CacheMetadata,
    buffer: Bytes,
    locker: HardwareLocker,
}

impl Debug for CacheEntry {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("CacheEntry")
            .field("metadata", &self.metadata)
            .field("buffer", &self.buffer.len())
            .field("locker", &self.locker)
            .finish()
    }
}

impl PartialEq for CacheEntry {
    fn eq(&self, o: &Self) -> bool {
        self.metadata == o.metadata && self.buffer == o.buffer
    }
}
impl FilterResult for CacheEntry {
    fn content_type(&self) -> Result<String> {
        Ok(self.metadata.content_type.clone())
    }
    fn dpi(&self) -> Result<(f64, f64)> {
        Ok(self.metadata.dpi.ok_or(NotAnImage())?)
    }

    fn content(&self) -> Result<Bytes> {
        Ok(self.buffer.clone())
    }

    fn image(self: Box<Self>) -> Result<Image> {
        Ok(Image::new(
            Some(&*self.buffer),
            self.metadata.dpi.map(|dpi| dpi.0),
            self.locker.clone(),
        )?)
    }
}

fn get_cache_config(cache_name: &String) -> Result<S3Cache> {
    config_ref::<Config>()
        .s3_caches
        .get(cache_name)
        .cloned()
        .ok_or_else(|| anyhow!("unknown cache {cache_name}"))
}

fn get_cache_filename(params: &String) -> String {
    let mut hasher = Sha1::new();

    hasher.update(params);

    hex::encode(hasher.finalize().as_slice())
}

fn get_path(params: &String) -> String {
    get_cache_filename(params)
}

async fn read_raw_cache_entry(
    mut data: impl AsyncRead + Unpin,
    ctx: Context,
    total_length: usize,
) -> Result<CacheEntry> {
    // Read the size of JSON data
    let mut size_buf = [0u8; JSON_LEN_BUFFER_SIZE];
    data.read_exact(&mut size_buf).await?;
    let size = u32::from_ne_bytes(size_buf);

    // Read JSON data from size bytes from data
    let mut json_buf = vec![0; size as usize];
    data.read_exact(&mut json_buf).await?;
    let content_type: CacheMetadata = serde_json::from_slice(&json_buf)?;

    let image_length = total_length - size as usize - JSON_LEN_BUFFER_SIZE;
    let mut buf = vec![0; image_length];
    data.read_exact(&mut buf).await?;

    Ok(CacheEntry {
        metadata: content_type,
        buffer: Bytes::from(buf),
        locker: ctx.cpu_core_guard.clone(),
    })
}

#[tracing::instrument(name = "s3_cache_get", skip(ctx, cache, client, _is_img), fields(cache_name = %cache.bucket_name))]
async fn get_cache_entry(
    ctx: Context,
    path: &String,
    _is_img: bool,
    cache: &S3Cache,
    client: aws_sdk_s3::Client,
) -> Result<CacheEntry> {
    let project_id = cache.project_id.clone();
    let s3_entry = if cache.path_style_host {
        client.get_object().bucket(&cache.bucket_name).key(path)
    } else {
        client
            .get_object()
            .key(format!("{}/{}", &cache.bucket_name, path))
    }
    .customize()
    .mutate_request(move |req| {
        req.headers_mut().insert(
            HeaderName::from_static("x-amz-project-id"),
            HeaderValue::from_str(&project_id).expect("invalid project id"),
        );
    })
    .send()
    .in_current_span()
    .await;

    match s3_entry {
        Ok(data) => {
            let total_length =
                data.content_length()
                    .ok_or_else(|| S3CacheError::CacheSizeError)? as usize;
            read_raw_cache_entry(data.body.into_async_read(), ctx, total_length)
                .in_current_span()
                .await
        }
        Err(err) => {
            match err {
                aws_sdk_s3::error::SdkError::ServiceError(err) => match err.into_err() {
                    GetObjectError::NoSuchKey(NoSuchKey { .. }) => Err(S3CacheError::NotInCache(
                        "Failed to download image from S3 cache NoSuchKey".into(),
                    ))?,
                    other => {
                        tracing::error!(
                            "Failed to download image from S3 cache due to service error: {other:?}",
                        );
                        Err(S3CacheError::S3ErrorGet(
                            "Failed to download image from S3 cache due to service error",
                            Box::new(other),
                        ))?
                    }
                },
                err @ aws_sdk_s3::error::SdkError::TimeoutError(_) => {
                    tracing::warn!(
                        "Failed to download image from S3 cache due to timeout error: {err:?}",
                    );
                    Err(S3CacheError::S3ErrorGet(
                        "Failed to download image from S3 cache due to timeout error",
                        Box::new(err),
                    ))?
                }
                // ... you can add more match arms if you want to handle other specific errors
                err => {
                    let code = err.code();
                    tracing::error!(
                        "Failed to download image from S3 cache code {code:?} due to error: {err:?}"
                    );
                    Err(S3CacheError::S3ErrorGet(
                        "Failed to download image from S3 cache",
                        Box::new(err),
                    ))?
                }
            }
        }
    }
}

#[tracing::instrument(name = "s3_cache_save", parent= None, skip(path, metadata, content, cache, client), fields(cache_name = %cache.bucket_name))]
async fn save<'a>(
    path: String,
    metadata: CacheMetadata,
    content: Bytes,
    cache: &S3Cache,
    client: aws_sdk_s3::Client,
) -> Result<()> {
    let meta = serde_json::to_string(&metadata)?;
    let mut output: Vec<u8> = Vec::with_capacity(content.len() + meta.as_bytes().len() + 4);

    // write meta.len as u32 in native endian
    output.write_all(&(meta.len() as u32).to_ne_bytes())?;
    output.write_all(meta.as_bytes())?;
    output.write_all(content.as_ref())?;

    let mut object = client.put_object();

    if cache.path_style_host {
        object = object.bucket(&cache.bucket_name).key(path);
    } else {
        object = object.key(format!("{}/{}", &cache.bucket_name, path));
    };

    let project_id = cache.project_id.clone();

    let add_headers =
        move |req: &mut ::aws_smithy_runtime_api::client::orchestrator::HttpRequest| {
            req.headers_mut().insert(
                HeaderName::from_static("x-amz-project-id"),
                HeaderValue::from_str(&project_id).expect("invalid project id"),
            );
        };

    match object
        .body(ByteStream::from(output))
        .bucket(&cache.bucket_name)
        .customize()
        .mutate_request(add_headers)
        .send()
        .in_current_span()
        .await
    {
        Ok(_) => Ok(()),
        Err(SdkError::ServiceError(err)) if err.raw().status().as_u16() == 429 => {
            // GCS returns 429 when we exceed the rate limit
            // Maximum rate of writes to the same object name: 1 write per second
            // https://cloud.google.com/storage/quotas
            // It's ok to ignore it since other instance must have already written the same object
            // If not - we will get it next time
            Ok(())
        }
        Err(err) => Err(S3CacheError::S3ErrorSave(
            "Failed to upload image to S3 cache".into(),
            Box::new(err),
        ))?,
    }
}

#[tracing::instrument(name = "s3_cache", skip(context, cache_name, args), fields(cache_name = %cache_name))]
async fn filter_result(context: Context, cache_name: String, args: &Args) -> Result<CacheEntry> {
    let filter = match args.first().as_ref() {
        Some(&FilterArg::Img(filter)) => filter,
        _ => return Err(NotAnImage().into()),
    };

    let params = format!("{:?}", args[0]);
    let lookup_image_path = arg_type!(cache, args, 2, Option(String));
    let cache = get_cache_config(&cache_name)?;
    let client = get_s3_client(&cache, &cache_name)?;

    let is_img = lookup_image_path.is_some();
    let path = lookup_image_path.unwrap_or_else(|| get_path(&params));

    if let Ok(entry) = get_cache_entry(context.clone(), &path, is_img, &cache, client.clone()).await
    {
        CACHE_HIT_COUNTER
            .with_label_values(&["hit", &cache_name, &filter.name, "s3"])
            .inc();
        if let Some(header_name) = context.log_filters_header {
            context
                .response_headers()?
                .entry(header_name.clone())
                .and_modify(|value| {
                    value.push_str("_hit(");
                    value.push_str(cache_name.as_str());
                    value.push(')');
                });
        }
        Ok(entry)
    } else {
        CACHE_HIT_COUNTER
            .with_label_values(&["miss", &cache_name, &filter.name, "s3"])
            .inc();
        let (metadata, content) = {
            if let Some(header_name) = context.log_filters_header {
                context
                    .response_headers()?
                    .entry(header_name.clone())
                    .and_modify(|value| {
                        value.push_str("_miss(");
                        value.push_str(cache_name.as_str());
                        value.push(')');
                    });
            }
            let img = match exec_filter(context.clone(), filter).in_current_span().await {
                Ok(img) => img,
                Err(err) => return Err(err),
            };

            (
                CacheMetadata {
                    content_type: img.content_type()?,
                    dpi: img.dpi().ok(),
                },
                img.content()?,
            )
        };

        let metadata_clone = metadata.clone();
        let content_clone = content.clone();
        let save_entry = async move {
            save(path.clone(), metadata_clone, content_clone, &cache, client)
                .map_err(|e| {
                    error!("failed to save cache entry: {e:?}");
                })
                .in_current_span()
                .await
        };
        tokio::spawn(save_entry);
        Ok(CacheEntry {
            metadata,
            buffer: content,
            locker: context.cpu_core_guard.clone(),
        })
    }
}

pub fn filter(context: Context, args: &Args) -> Future<'_> {
    let cache_name = arg_type!(cache, args, 1, String);

    async move {
        let cache_entry = filter_result(context, cache_name, args).await?;

        Ok(Box::new(cache_entry).into())
    }
    .in_current_span()
    .boxed()
}
