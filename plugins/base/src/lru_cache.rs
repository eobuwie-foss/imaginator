use anyhow::{Context, Result};
use bincode::{deserialize_from, serialize_into};
use core::marker::PhantomData;
use hex;
use jwalk::WalkDir;
use serde::Deserialize;
use serde::Serialize;
use serde::{de::Visitor, Deserializer, Serializer};
use sha1::{Digest, Sha1};
use std::collections::{HashMap, HashSet};
use std::convert::TryInto;
use std::fmt;
use std::fs::{create_dir_all, remove_file, File};
use std::hash::Hash;
use std::io::Write;
use std::iter::FromIterator;
use std::path::PathBuf;
use std::time::Instant;
use systemd;
use thiserror::Error;
use tokio::task::block_in_place;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Error)]
#[error("Data is too big to insert into the cache. {size} bytes > {capacity} bytes of capacity")]
struct DataTooBigError {
    pub size: usize,
    pub capacity: usize,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Error)]
#[error("Data is empty")]
struct DataEmpty {}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Error)]
#[error("The file is not in cache")]
struct NotInCache {}

type CachePath = [u8; 20];

#[derive(Debug, Clone, Serialize, Deserialize, Default)]
struct LruEntry<K, V> {
    pub key: K,
    pub value: V,
    pub prev: Option<usize>,
    pub next: Option<usize>,
}

#[derive(Debug, Clone, Default, Serialize)]
struct Lru<K: Clone + Eq + Hash + Default> {
    #[serde(serialize_with = "serialize_as_bytes")]
    data: Vec<LruEntry<K, usize>>,
    #[serde(serialize_with = "serialize_as_bytes")]
    dead_cells: Vec<usize>,
    #[serde(skip)]
    keys: HashMap<K, usize>,
    head: Option<usize>,
    tail: Option<usize>,
    #[serde(skip)]
    size: usize,
}

impl<'de, K> Deserialize<'de> for Lru<K>
where
    K: Clone + Eq + Hash + Default + Serialize + Deserialize<'de>,
{
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        #[derive(Deserialize)]
        struct LruHelper<K: Clone + Eq + Hash + Default> {
            #[serde(deserialize_with = "deserialize_from_bytes")]
            data: Vec<LruEntry<K, usize>>,
            #[serde(deserialize_with = "deserialize_from_bytes")]
            dead_cells: Vec<usize>,
            head: Option<usize>,
            tail: Option<usize>,
        }

        let helper: LruHelper<K> = LruHelper::deserialize(deserializer)?;

        let mut keys = HashMap::new();
        let mut size = 0usize;
        let dead_cells: HashSet<usize> = HashSet::from_iter(helper.dead_cells.clone());
        for (i, entry) in helper.data.iter().enumerate() {
            if !dead_cells.contains(&i) {
                keys.insert(entry.key.clone(), i);
                size += entry.value;
            }
        }

        Ok(Lru {
            data: helper.data,
            dead_cells: helper.dead_cells,
            keys,
            head: helper.head,
            tail: helper.tail,
            size,
        })
    }
}

// `is_empty` would not be used anywhere so it would trigger dead code
// doc for lint: https://rust-lang.github.io/rust-clippy/master/index.html#/len_without_is_empty
#[allow(clippy::len_without_is_empty)]
#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct LruCache {
    #[serde(skip)]
    root: PathBuf,
    capacity: usize,
    items: Lru<CachePath>,
}

fn serialize_as_bytes<T, S: Serializer>(input: &[T], s: S) -> Result<S::Ok, S::Error> {
    let data_buf: *const u8 = input.as_ptr().cast();
    let data = std::ptr::slice_from_raw_parts(data_buf, std::mem::size_of_val(input));
    s.serialize_bytes(unsafe { &*data })
}

fn deserialize_from_bytes<'de, D: Deserializer<'de>, T: Clone>(de: D) -> Result<Vec<T>, D::Error> {
    de.deserialize_bytes(VecVisitor::<T>(PhantomData))
}

struct VecVisitor<T: Clone>(PhantomData<T>);
impl<'de, T: Clone> Visitor<'de> for VecVisitor<T> {
    type Value = Vec<T>;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        write!(formatter, "a slice of bytes")
    }

    fn visit_bytes<E>(self, value: &[u8]) -> Result<Self::Value, E> {
        if value.is_empty() {
            return Ok(Self::Value::new());
        }
        let buf = value.as_ptr().cast::<T>();
        let data = std::ptr::slice_from_raw_parts(buf, value.len() / std::mem::size_of::<T>());
        if data.is_null() {
            return Ok(Self::Value::new());
        }
        // this should be safe as long as the input data is trusted. and our own cache is assumed
        // to be.
        Ok(unsafe { &*data }.to_vec())
    }

    fn visit_borrowed_bytes<E>(self, value: &'de [u8]) -> Result<Self::Value, E> {
        let buf = value.as_ptr().cast::<T>();
        let data = std::ptr::slice_from_raw_parts(buf, value.len() / std::mem::size_of::<T>());
        // this should be safe as long as the input data is trusted. and our own cache is assumed
        // to be.
        Ok(unsafe { &*data }.to_vec())
    }
}

impl<K: Clone + Eq + Hash + Default> Lru<K> {
    pub fn new() -> Self {
        Lru::default()
    }

    fn remove_item(&mut self, index: usize) {
        if self.tail == Some(index) {
            if let Some(next) = self.data[index].next {
                self.tail = Some(next);
            }
        }

        if let Some(prev) = self.data[index].prev {
            self.data[prev].next = self.data[index].next;
        }
        if let Some(next) = self.data[index].next {
            self.data[next].prev = self.data[index].prev;
        }
    }

    fn bump_item(&mut self, index: usize) {
        if self.head != Some(index) {
            self.remove_item(index);

            if let Some(head) = self.head {
                self.data[head].next = Some(index);
                self.data[index].prev = Some(head);
                self.data[index].next = None;
            }
            self.head = Some(index);
        }
    }

    pub fn insert(&mut self, key: K, value: usize) {
        let value = LruEntry {
            key: key.clone(),
            value,
            prev: self.head,
            next: None,
        };
        self.size += value.value;
        if let Some(&index) = self.keys.get(&key) {
            self.bump_item(index);

            self.size -= self.data[index].value;
            self.data[index] = value;
        } else if let Some(new_index) = self.dead_cells.pop() {
            if let Some(head) = self.head {
                self.data[head].next = Some(new_index);
            }
            self.head = Some(new_index);
            self.data[new_index] = value;
        } else {
            let new_index = self.data.len();
            if let Some(head) = self.head {
                self.data[head].next = Some(new_index);
            }
            self.head = Some(new_index);
            self.data.push(value);
        }

        if self.tail.is_none() {
            self.tail = self.head;
        }

        self.keys.insert(key, self.head.unwrap());
    }

    pub fn pop(&mut self) -> Option<(K, usize)> {
        if let Some(tail) = self.tail {
            let entry = std::mem::take(&mut self.data[tail]);
            self.keys.remove(&entry.key);
            self.dead_cells.push(tail);
            if let Some(next) = entry.next {
                self.data[next].prev = None;
                self.tail = Some(next);
            } else {
                self.tail = None;
                // If there's no next element, the list becomes empty.
                self.head = None;
            }
            self.size -= entry.value;
            Some((entry.key, entry.value))
        } else {
            None
        }
    }

    pub fn get(&mut self, key: K) -> Option<&usize> {
        if let Some(&index) = self.keys.get(&key) {
            self.bump_item(index);

            Some(&self.data[index].value)
        } else {
            None
        }
    }

    pub fn len(&self) -> usize {
        self.data.len() - self.dead_cells.len()
    }
}

#[test]
fn test_adding_capacity_minus_one() {
    let temp_dir = tempfile::tempdir().unwrap();

    let mut lru_cache = LruCache::new(temp_dir.path(), 10).unwrap();

    lru_cache.insert_bytes("hello0".to_string(), b"0").unwrap();
    lru_cache
        .insert_bytes("hello1".to_string(), b"012345678")
        .unwrap();

    assert_eq!(lru_cache.len(), 2);
    assert_eq!(lru_cache.size(), 10);

    // update the value of hello1 in place
    lru_cache.insert_bytes("hello0".to_string(), b"0").unwrap();
    assert_eq!(lru_cache.len(), 2);
    assert_eq!(lru_cache.size(), 10);

    lru_cache
        .insert_bytes("hello2".to_string(), b"0123456789")
        .unwrap();

    assert_eq!(lru_cache.len(), 1);
    assert_eq!(lru_cache.size(), 10);
}

#[test]
fn test_lru_one_item() {
    let mut lru: Lru<&'static str> = Lru::new();
    lru.insert("entry1", 2137);
    assert_eq!(lru.get("entry1"), Some(&2137));
    assert_eq!(lru.pop(), Some(("entry1", 2137)));
    assert_eq!(lru.get("entry1"), None);
}

#[test]
fn test_lru() {
    let mut lru: Lru<&'static str> = Lru::new();
    lru.insert("entry1", 2137);
    lru.insert("entry2", 23);
    lru.insert("entry3", 42);
    assert_eq!(lru.get("entry2"), Some(&23));
    assert_eq!(lru.pop(), Some(("entry1", 2137)));
    assert_eq!(lru.pop(), Some(("entry3", 42)));
    assert_eq!(lru.pop(), Some(("entry2", 23)));
    assert_eq!(lru.pop(), None);
}

#[test]
fn test_lru_moving_tail() {
    let mut lru: Lru<&'static str> = Lru::new();
    lru.insert("entry1", 2137);
    lru.insert("entry2", 23);
    lru.insert("entry3", 42);
    assert_eq!(lru.get("entry1"), Some(&2137));
    assert_eq!(lru.get("entry2"), Some(&23));
    assert_eq!(lru.pop(), Some(("entry3", 42)));
    assert_eq!(lru.pop(), Some(("entry1", 2137)));
    assert_eq!(lru.pop(), Some(("entry2", 23)));
    assert_eq!(lru.pop(), None);
}

fn hash_entry_name(data: &str) -> CachePath {
    let mut hasher = Sha1::new();
    hasher.update(data);
    hasher.finalize().as_slice().try_into().unwrap()
}

fn path_from_cache_entry(data: CachePath) -> String {
    format!(
        "{:02x}/{:02x}/{:02x}/{}",
        data[0],
        data[1],
        data[2],
        hex::encode(&data[3..])
    )
}

impl LruCache {
    pub fn size(&self) -> usize {
        self.items.size
    }
    pub fn capacity(&self) -> usize {
        self.capacity
    }
    pub fn len(&self) -> usize {
        // print all elements
        self.items.len()
    }

    pub fn new<P: Into<PathBuf>>(root: P, capacity: usize) -> Result<LruCache> {
        let root = root.into();

        if let Some(imported) = LruCache::import(&root, capacity)? {
            tracing::debug!("imported cache from {}", root.display());
            return Ok(imported);
        }

        let mut cache = LruCache {
            root,
            capacity,
            items: Lru::new(),
        };

        if cache.root.exists() {
            let path = cache.root.clone();
            cache.load(path, &mut Instant::now())?;
            cache.export()?;
        } else {
            create_dir_all(&cache.root)?;
        }
        Ok(cache)
    }

    fn load(&mut self, path: PathBuf, last_systemd_ping: &mut Instant) -> Result<()> {
        let start = Instant::now();
        let root_path_length = self.root.to_str().unwrap().len() + 1;
        let mut entry_name = [0u8; 20];
        let mut entry_counter = 0;

        for entry in WalkDir::new(&path) {
            let entry = entry.context(format!("Error reading entry in {path:?}"))?;
            let entry_path = entry.path();

            if path == self.root {
                entry_counter += 1;
                let time_since_systemd_ping = Instant::now().duration_since(*last_systemd_ping);

                if time_since_systemd_ping.as_secs() >= 10 {
                    tracing::info!(
                        "Loading {}, {} entries processed so far",
                        self.root.to_str().unwrap(),
                        entry_counter
                    );

                    // Extend our startup time by 10 seconds every 10 seconds
                    systemd::daemon::notify(
                        false,
                        [(
                            systemd::daemon::STATE_EXTEND_TIMEOUT_USEC,
                            format!("{}", time_since_systemd_ping.as_micros()),
                        )]
                        .iter(),
                    )
                    .unwrap();
                    *last_systemd_ping = Instant::now();
                }
            }

            let metadata = entry.metadata()?;
            if metadata.is_dir() {
                continue; // Skip directories as `jwalk` handles nested traversal automatically
            } else {
                // We do not support non-utf8 paths, hence the unwrap below.
                let entry_name_string =
                    entry_path.to_str().unwrap()[root_path_length..].replace('/', "");
                hex::decode_to_slice(&entry_name_string, &mut entry_name)
                    .context(format!(
                        "decode_to_slice {entry_name_string} when loading cache from {path:?}"
                    ))
                    .unwrap();

                self.items.insert(entry_name, metadata.len() as usize);
            }
        }

        tracing::info!(
            "Loaded {} entries from {} in {:?}",
            entry_counter,
            self.root.display(),
            start.elapsed()
        );

        Ok(())
    }

    pub fn import<P: Into<PathBuf>, S: Into<Option<usize>>>(
        root: P,
        capacity: S,
    ) -> Result<Option<LruCache>> {
        let root = root.into();
        let path = root.join("index");

        if !PathBuf::from(&path).exists() {
            tracing::warn!("Cache index not found at {:?}", path);
            return Ok(None);
        }
        let mut cache: LruCache =
            deserialize_from(File::open(&path).with_context(|| format!("{path:?}"))?)?;

        cache.root = root;
        if let Some(capacity) = capacity.into() {
            cache.capacity = capacity;
        }
        let dead_cells: HashSet<usize> = HashSet::from_iter(cache.items.dead_cells.clone());
        for (i, entry) in cache.items.data.iter().enumerate() {
            if !dead_cells.contains(&i) {
                cache.items.keys.insert(entry.key, i);
            }
        }
        Ok(Some(cache))
    }

    pub fn export(&self) -> Result<()> {
        let path = self.root.join("index");
        let file = File::create(&path).with_context(|| format!("{path:?}"))?;
        serialize_into(file, self)?;
        Ok(())
    }

    pub fn resolve_path(&mut self, name: &str) -> Result<PathBuf> {
        let name = hash_entry_name(name);
        if self.items.get(name).is_some() {
            Ok(self.root.join(path_from_cache_entry(name)))
        } else {
            Err(NotInCache {})?
        }
    }

    pub fn insert_bytes(&mut self, name: String, value: &[u8]) -> Result<()> {
        if value.len() > self.capacity {
            return Err(DataTooBigError {
                size: value.len(),
                capacity: self.capacity,
            })?;
        }

        if value.is_empty() {
            return Err(DataEmpty {})?;
        }

        while self.items.size + value.len() > self.capacity {
            if let Some((name, _size)) = self.items.pop() {
                let mut path = self.root.clone();
                path.push(path_from_cache_entry(name));
                match remove_file(&path) {
                    Ok(_) => (),
                    Err(e) => match e.kind() {
                        std::io::ErrorKind::NotFound => (),
                        _ => Err(e).with_context(|| format!("{path:?}"))?,
                    },
                }
            } else {
                // change the log level to avoid generating too much noise in sentry
                // there is a issue to fix this bug and the warn log message will be available in
                // the logs anyway
                tracing::warn!("We couldn't remove anything from cache {:?}, even though we still have {} bytes in size. The cache has {} items. Stop imaginator, remove the index, then start it again. This is most likely a bug.", self.root, self.items.size, self.items.len());
                break;
            }
        }

        let mut path = self.root.clone();
        path.push(path_from_cache_entry(hash_entry_name(&name)));
        create_dir_all(path.parent().unwrap())?;
        let mut file = File::create(&path).with_context(|| format!("{path:?}"))?;

        block_in_place(move || file.write_all(value).with_context(|| format!("{path:?}")))?;
        self.items.insert(hash_entry_name(&name), value.len());

        Ok(())
    }
}

#[test]
fn test_lru_cache_eviction() {
    let temp_dir = tempfile::tempdir().unwrap();

    let mut lru_cache = LruCache::new(temp_dir.path(), 100).unwrap();

    // Insert items to fill up the cache
    for i in 0..10 {
        let key = format!("key{}", i);
        let value = rand::random::<[u8; 10]>();
        lru_cache.insert_bytes(key, &value).unwrap();
    }

    assert_eq!(lru_cache.len(), 10);
    assert!(lru_cache.size() <= lru_cache.capacity());

    // Insert more items to trigger eviction
    for i in 10..20 {
        let key = format!("key{}", i);
        let value = vec![0u8; 10]; // Each item is 10 bytes
        lru_cache.insert_bytes(key, &value).unwrap();
    }

    assert_eq!(lru_cache.len(), 10);
    assert!(lru_cache.size() <= lru_cache.capacity());

    // Ensure that the first few items have been evicted
    for i in 0..10 {
        let key = format!("key{}", i);
        assert!(lru_cache.resolve_path(&key).is_err());
    }

    // Ensure that the last few items are still in the cache
    for i in 10..20 {
        let key = format!("key{}", i);
        assert!(lru_cache.resolve_path(&key).is_ok());
    }
}
