use crate::cfg::Config;
use crate::cfg::JSON_LEN_BUFFER_SIZE;
use crate::lru_cache::LruCache;
use anyhow::{Context as _, Error, Result};
use bytes::Bytes;
use futures::future::ready;
use futures::FutureExt;
use imaginator::cfg::config_ref;
use imaginator::filter::{
    exec_filter, Args, Context, Filter, FilterArg, FilterResult, Future, NotAnImage,
};
use imaginator::img::Image;
use imaginator::metrics::CACHE_HIT_COUNTER;
use imaginator::HardwareLocker;
use serde_json;
use std::collections::HashMap;
use std::fmt;
use std::fmt::Debug;
use std::fmt::Formatter;
use std::sync::{Mutex, MutexGuard, Once};
use thiserror::Error;
use tokio::io::AsyncReadExt;
use tokio::io::AsyncWriteExt;
use tracing::debug;
use tracing::{warn, Instrument};

#[derive(Debug, Clone, Eq, PartialEq, Error)]
#[error("No such cache: {0}")]
struct NoSuchCache(String);

#[derive(Debug, Clone, Eq, PartialEq, Error)]
#[error("This entry is not present in the cache.")]
struct NotInCache();

static INIT_CACHE: Once = Once::new();
static mut FILE_CACHE: Option<HashMap<String, Mutex<LruCache>>> = None;

pub fn cache(name: &str) -> Result<MutexGuard<'static, LruCache>> {
    INIT_CACHE.call_once(|| {
        let mut map = HashMap::with_capacity(1_000);
        for (name, cache) in &config_ref::<Config>().caches {
            tracing::debug!("Initializing cache {name} {:?}", cache);
            map.insert(
                name.clone(),
                Mutex::new(
                    LruCache::new(&cache.dir, cache.size)
                        .with_context(|| format!("{:?}", cache.dir))
                        .expect("LRU cache at path should be valid"),
                ),
            );
        }
        unsafe {
            FILE_CACHE = Some(map);
        }
    });
    unsafe {
        let cache = FILE_CACHE
            .as_ref()
            .expect("// This should never fail, because we initialize the FILE_CACHE above");
        Ok(cache
            .get(name)
            .ok_or_else(|| NoSuchCache(name.to_owned()))?
            .lock()
            .expect("// If the mutex is poisoned, there's no sensible thing we can do anyway."))
    }
}

#[derive(Serialize, Deserialize, Debug)]
struct CacheMetadata {
    content_type: String,
    dpi: Option<(f64, f64)>,
}

struct CacheEntry {
    metadata: CacheMetadata,
    buffer: Bytes,
    locker: HardwareLocker,
}

impl Debug for CacheEntry {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        f.debug_struct("CacheEntry")
            .field("metadata", &self.metadata)
            .field("buffer", &self.buffer.len())
            .field("locker", &self.locker)
            .finish()
    }
}

impl FilterResult for CacheEntry {
    fn content_type(&self) -> Result<String> {
        Ok(self.metadata.content_type.clone())
    }

    fn dpi(&self) -> Result<(f64, f64)> {
        Ok(self.metadata.dpi.ok_or(NotAnImage())?)
    }

    fn content(&self) -> Result<Bytes> {
        Ok(self.buffer.clone())
    }

    fn image(self: Box<Self>) -> Result<Image> {
        Ok(Image::new(
            Some(&*self.buffer),
            self.metadata.dpi.map(|dpi| dpi.0),
            self.locker.clone(),
        )?)
    }
}

#[derive(Error, Debug)]
pub enum CacheSaveError {
    #[error("Failed to serialize metadata: {0}")]
    SerializationError(serde_json::Error, Box<dyn FilterResult>),

    #[error("Failed to get content type: {0}")]
    InvalidContentType(anyhow::Error, Box<dyn FilterResult>),

    #[error("Invalid content: {0}")]
    InvalidContent(anyhow::Error, Box<dyn FilterResult>),

    #[error("Unknown cache: {0}")]
    UnknownCache(String, Box<dyn FilterResult>),

    #[error("IO error: {0}")]
    Io(std::io::Error, Box<dyn FilterResult>),

    #[error("Failed to find cache: {0}")]
    CacheObtain(anyhow::Error, Box<dyn FilterResult>),

    #[error("Failed to insert into cache: {0:?}")]
    InsertFailed(anyhow::Error, Box<dyn FilterResult>),
}

impl CacheSaveError {
    pub fn get_filter_result(self) -> Box<dyn FilterResult> {
        match self {
            CacheSaveError::SerializationError(_, result)
            | CacheSaveError::InvalidContentType(_, result)
            | CacheSaveError::InvalidContent(_, result)
            | CacheSaveError::UnknownCache(_, result)
            | CacheSaveError::Io(_, result)
            | CacheSaveError::CacheObtain(_, result)
            | CacheSaveError::InsertFailed(_, result) => result,
        }
    }
}

#[tracing::instrument(skip(result))]
async fn save(
    cache_name: &str,
    path: String,
    result: Box<dyn FilterResult>,
) -> Result<Box<dyn FilterResult>, CacheSaveError> {
    let Some(specific_cache) = config_ref::<Config>().caches.get(cache_name) else {
        return Err(CacheSaveError::UnknownCache(cache_name.to_string(), result));
    };

    if specific_cache.read_only {
        return Ok(result);
    }

    let metadata = match result.content_type() {
        Ok(content_type) => CacheMetadata {
            content_type,
            dpi: result.dpi().ok(),
        },
        Err(e) => return Err(CacheSaveError::InvalidContentType(e, result)),
    };

    let meta = match serde_json::to_string(&metadata) {
        Ok(meta) => meta,
        Err(e) => return Err(CacheSaveError::SerializationError(e, result)),
    };

    let content = match result.content() {
        Ok(content) => content,
        Err(e) => return Err(CacheSaveError::InvalidContent(e, result)),
    };

    let mut output: Vec<u8> = Vec::with_capacity(JSON_LEN_BUFFER_SIZE + meta.len() + content.len());

    // write meta.len in native endian
    if let Err(e) = output.write_all(&(meta.len() as u32).to_ne_bytes()).await {
        return Err(CacheSaveError::Io(e, result));
    }

    if let Err(e) = output.write_all(meta.as_bytes()).await {
        return Err(CacheSaveError::Io(e, result));
    };
    if let Err(e) = output.write_all(content.as_ref()).await {
        return Err(CacheSaveError::Io(e, result));
    };

    let mut cache = match cache(cache_name) {
        Ok(cache) => cache,
        Err(e) => return Err(CacheSaveError::CacheObtain(e, result)),
    };

    if let Err(e) = cache.insert_bytes(path, output.as_slice()) {
        return Err(CacheSaveError::InsertFailed(e, result));
    }

    Ok(result)
}

#[tracing::instrument(name = "lru_read", skip(ctx))]
async fn read_cache_entry(cache_name: &str, params: &str, ctx: Context) -> Result<CacheEntry> {
    let path = cache(cache_name)?.resolve_path(params).map_err(|e| {
        debug!("cache entry for {cache_name} {params} does not exist: {e}");
        NotInCache()
    })?;
    let mut reader = tokio::fs::File::open(&path).await?;
    let file_size = reader.metadata().await?.len() as usize;

    let mut size_buf = [0u8; JSON_LEN_BUFFER_SIZE];
    reader.read_exact(&mut size_buf).await?;
    let json_buf_len = u32::from_ne_bytes(size_buf) as usize;

    let file_small = file_size < JSON_LEN_BUFFER_SIZE;

    if file_small {
        return Err(anyhow::anyhow!(
            "File {path:?} in cache {cache_name} is too small: {file_size} bytes"
        ));
    }

    if json_buf_len > file_size {
        return Err(anyhow::anyhow!(
                "Metadata length {json_buf_len} is bigger than file size {file_size} in cache {cache_name}"
        ));
    }

    let mut json_buf = vec![0; json_buf_len];
    reader.read_exact(&mut json_buf).await?;
    let content_type: CacheMetadata = serde_json::from_slice(&json_buf)?;

    let image_buffer_size = file_size - json_buf_len - JSON_LEN_BUFFER_SIZE;
    if image_buffer_size == 0 {
        return Err(anyhow::anyhow!(
            "File {path:?} in cache {cache_name} is too small: {file_size} bytes"
        ));
    }

    let mut buf = vec![0; image_buffer_size];
    reader.read_exact(&mut buf).await?;

    Ok(CacheEntry {
        metadata: content_type,
        buffer: Bytes::from(buf),
        locker: ctx.cpu_core_guard.clone(),
    })
}

#[tracing::instrument(name = "lru_cache", skip(context))]
fn filter_result(context: Context, cache_name: String, args: &Args) -> Future<'_> {
    let arg = args.first();
    let filter = match arg {
        Some(FilterArg::Img(filter)) => filter,
        _ => return ready(Err(NotAnImage().into())).boxed(),
    };

    let params = format!("{:?}", args[0]);

    async move {
        let entry = match read_cache_entry(&cache_name, &params, context.clone()).await {
            Ok(entry) => {
                CACHE_HIT_COUNTER
                    .with_label_values(&["hit", &cache_name, &filter.name, "lru"])
                    .inc();
                entry
            }
            err => {
                tracing::debug!("Cache miss for {cache_name} {args:?}: {err:?}");
                return create_cache_entry(context, filter, params, cache_name).await;
            }
        };

        if let Some(header_name) = context.log_filters_header {
            context
                .response_headers()?
                .entry(header_name.clone())
                .and_modify(|value| {
                    value.push_str("_hit(");
                    value.push_str(cache_name.as_str());
                    value.push(')');
                });
        }
        Ok(Box::new(entry).into())
    }
    .in_current_span()
    .boxed()
}

#[tracing::instrument(name = "lru_save", skip(context, filter))]
async fn create_cache_entry(
    context: Context,
    filter: &Filter,
    params: String,
    cache_name: String,
) -> Result<Box<dyn FilterResult>, Error> {
    CACHE_HIT_COUNTER
        .with_label_values(&["miss", &cache_name, &filter.name, "lru"])
        .inc();

    if let Some(header_name) = context.log_filters_header {
        context
            .response_headers()?
            .entry(header_name.clone())
            .and_modify(|value| {
                value.push_str("_miss(");
                value.push_str(cache_name.as_str());
                value.push(')');
            });
    }
    let img = exec_filter(context, filter).in_current_span().await?;
    save(cache_name.as_str(), params.clone(), img)
        .in_current_span()
        .await
        .or_else(|e| {
            warn!("Failed to save cache entry: {e}");
            Ok(e.get_filter_result())
        })
}

pub fn filter(context: Context, args: &Args) -> Future<'_> {
    let cache_name = arg_type!(cache, args, 1, String);
    filter_result(context, cache_name, args)
}
