#[macro_use]
extern crate imaginator_common as imaginator;
#[macro_use]
extern crate serde_derive;
use aws_config::BehaviorVersion;
use url::{Position, Url};

use aws_config::timeout::TimeoutConfig;
use aws_sdk_s3::config::{Credentials, Region, SharedHttpClient};
use cfg::S3Cache;
use tracing_futures::Instrument;

use anyhow::{anyhow, Result};
use imaginator::cfg::config_ref;
use imaginator::filter::{Args, Context, Future};
use imaginator::img::{
    cleanup_magick, AlphaChannel, ColorProfile, Colorspace, CompositeOperator, CompressionType,
    Filter as FilterType, Gravity, ImageFormat, ResolutionUnit,
};
use imaginator::prelude::*;
use std::collections::HashMap;
use thiserror::Error;

pub mod cfg;
pub use cfg::Config;

pub mod cache;
pub mod lru_cache;

pub mod download;
pub mod s3_cache;

use aws_smithy_runtime::client::http::hyper_014::HyperClientBuilder;

fn init_caches() -> Result<()> {
    // TODO: https://ccc-group.atlassian.net/browse/CORE-1543
    let accept_invalid_tls_certs = config_ref::<Config>().accept_invalid_tls_certs;
    unsafe {
        CLIENT = Some(
            reqwest::ClientBuilder::new()
                .danger_accept_invalid_certs(accept_invalid_tls_certs)
                // .timeout(std::time::Duration::from_secs(10))
                // .pool_idle_timeout(std::time::Duration::from_secs(10))
                // .connect_timeout(std::time::Duration::from_secs(10))
                .build()
                .expect("HTTP client must be valid and build at the boot"),
        );
    };
    let caches = &config_ref::<Config>().caches;
    for key in caches.keys() {
        let _mutex_gard = cache::cache(key)?;
    }

    let s3_init = async {
        let s3_caches = &config_ref::<Config>().s3_caches;

        let mut s3_clients = HashMap::new();
        for (name, cache) in s3_caches {
            let hash = cache.hash();
            match s3_clients.get(&hash) {
                Some(_) => {
                    tracing::debug!("S3 client for cache {} already exists", name);
                }
                None => {
                    tracing::info!("Creating S3 client for cache {}", name);
                    let client = build_client(cache)
                        .await
                        .map_err(|e| {
                            anyhow!("failed to build client from config {:?}: {}", cache, e)
                        })
                        .expect("S3 client must be valid and build at the boot");
                    s3_clients.insert(hash, client);
                }
            }
        }
        s3_clients
    };

    unsafe {
        S3_CLIENTS = Some(futures::executor::block_on(s3_init));
    };

    Ok(())
}

fn save_caches() -> Result<()> {
    cleanup_magick();
    let caches = &config_ref::<Config>().caches;
    for (name, cache) in caches {
        if !cache.read_only {
            tracing::info!("Saving cache {}", name);
            cache::cache(name)?.export()?;
        } else {
            tracing::warn!("Skipping saving cache {} because it's read-only", name);
        }
    }

    Ok(())
}

static mut S3_CLIENTS: Option<HashMap<u64, aws_sdk_s3::Client>> = None;

pub fn get_s3_client(cache: &S3Cache, cache_name: &str) -> Result<aws_sdk_s3::Client> {
    unsafe {
        S3_CLIENTS
            .as_ref()
            .expect("S3 clients must be build at the boot")
            .get(&cache.hash())
            .cloned()
            .ok_or_else(|| anyhow!("S3 client for cache {cache_name} not found"))
    }
}

fn create_my_credential_provider(access_key: &str, secret_key: &str) -> Credentials {
    Credentials::new(access_key, secret_key, None, None, "example")
}

async fn build_client(cache: &S3Cache) -> anyhow::Result<aws_sdk_s3::Client> {
    // This is a workaround for issues with GCS and HTTP2
    let enable_http1_only = std::env::var("ENABLE_HTTP1_ONLY")
        .map(|val| {
            val.parse::<bool>().unwrap_or_else(|e| {
                tracing::error!(
                    "failed to parse ENABLE_HTTP1_ONLY, got '{val}'; defaulting to 'false': {e}"
                );
                false
            })
        })
        .unwrap_or(false);

    let tls_connector = hyper_rustls::HttpsConnectorBuilder::new()
        .with_webpki_roots()
        .https_or_http();

    let tls_connector = if enable_http1_only {
        tracing::info!("Forcing HTTP1 transport");
        tls_connector.enable_http1().build()
    } else {
        tracing::info!("Using HTTP1/HTTP2 transport");
        tls_connector.enable_http1().enable_http2().build()
    };

    let hyper_client: SharedHttpClient = HyperClientBuilder::new().build(tls_connector);

    let mut config = aws_config::defaults(BehaviorVersion::latest())
        .region(Region::new(cache.region.clone()))
        .credentials_provider(create_my_credential_provider(
            &cache.access_key,
            &cache.secret_key,
        ))
        .http_client(hyper_client)
        .endpoint_url(cache.endpoint.clone());

    // aws_sdk library does not support virtual host style expilicitly
    if cache.path_style_host {
        config = config.endpoint_url(cache.endpoint.clone());
    } else {
        // virutal host style http://your-bucket-name.s3.amazonaws.com/your-object-key
        let url = Url::parse(&cache.endpoint)?;
        let endpoint = format!(
            "{scheme}://{bucket_name}.{s3_endpoint}",
            scheme = url.scheme(),
            bucket_name = cache.bucket_name,
            s3_endpoint = &url[Position::BeforeHost..]
        );
        config = config.endpoint_url(endpoint);
    }

    if let Some(duration) = cache.timeout {
        config = config.timeout_config(TimeoutConfig::builder().operation_timeout(duration).build())
    };

    let config = config.load().await;
    Ok(aws_sdk_s3::Client::new(&config))
}

pub static mut CLIENT: Option<reqwest::Client> = None;

pub fn http_client() -> reqwest::Client {
    unsafe {
        CLIENT
            .as_ref()
            .expect("HTTP client must be build at the boot")
            .clone()
    }
}

pub fn plugin() -> PluginInformation {
    let mut map: FilterMap = HashMap::with_capacity(100);
    map.insert("download", &download::filter);
    map.insert("resize", &resize);
    map.insert("fit-in", &fit_in);
    map.insert("brightness_contrast", &brightness_contrast);
    map.insert("compose", &compose);
    map.insert("color_rect", &color_rect);
    map.insert("trim", &trim);
    map.insert("crop", &crop);
    map.insert("extend", &extend);
    map.insert("extend-from-center", &extend_from_center);
    map.insert("format", &format);
    map.insert("repeat", &repeat);
    map.insert("colorspace", &colorspace);
    map.insert("profile", &profile);
    map.insert("compression", &compression);
    map.insert("resample", &resample);
    map.insert("canvas", &canvas);
    map.insert("cm", &cm);
    map.insert("sepia", &sepia);
    map.insert("flip", &flip);
    map.insert("flop", &flop);
    map.insert("cache", &cache::filter);
    map.insert("dpi", &dpi);
    map.insert("alpha", &alpha);
    map.insert("gravity", &gravity);
    map.insert("bg", &background);
    map.insert("quality", &quality);
    map.insert("s3_cache", &s3_cache::filter);
    PluginInformation::new(map)
        .with_init(&init_caches)
        .with_exit(&save_caches)
}

/// Generate color rectangle
///
/// This can be useful in composition operations.
pub fn color_rect(context: Context, args: &Args) -> Future<'_> {
    let color = arg_type!(color_rect, args, 0, String);
    let width = arg_type!(color_rect, args, 1, isize) as _;
    let height = arg_type!(color_rect, args, 2, isize) as _;
    let format = if args.len() > 3 {
        Some(arg_type!(color_rect, args, 3, String))
    } else {
        None
    };

    async move {
        let mut image =
            imaginator::img::Image::new(None::<&[u8]>, None, context.cpu_core_guard.clone())?;
        image.empty_image(&color, width, height, format.as_deref())?;

        Ok(Box::new(image).into())
    }
    .boxed()
}

image_filter!(brightness_contrast(img: Image, context: &Context, brightness: f32, contrast: f32) {
    img.set_brightness_contrast(brightness as _, contrast as _)?;
});

image_filter!(fit_in(img: Image, context: &Context, mut w: isize, mut h: isize) {
    let cfg = &config_ref::<Config>().image;
    if let Some(max_w) = cfg.max_width {
        if w > max_w {
            w = max_w;
        }
    }

    if let Some(max_h) = cfg.max_height {
        if h > max_h {
            h = max_h;
        }
    }
    let ratio: f64 = (img.width() as f64)/(img.height() as f64);
    if w == 0 {
        w = ((h as f64) * ratio) as isize;
    }
    if h == 0 {
        h = ((w as f64) / ratio) as isize;
    }
    img.fit_in(w as usize, h as usize)?;
});

image_filter!(resize(img: Image, context: &Context, mut w: isize, mut h: isize, filter: Option<FilterType>) {
    let filter = filter.unwrap_or_default();
    let cfg = &config_ref::<Config>().image;
    if let Some(max_w) = cfg.max_width {
        if w > max_w {
            w = max_w;
        }
    }

    if let Some(max_h) = cfg.max_height {
        if h > max_h {
            h = max_h;
        }
    }
    let ratio: f64 = (img.width() as f64)/(img.height() as f64);
    if w == 0 {
        w = ((h as f64) * ratio) as isize;
    }
    if h == 0 {
        h = ((w as f64) / ratio) as isize;
    }
    img.resize(w as usize, h as usize, &filter)?;
});

#[derive(Debug, Error)]
pub enum DimensionsExceeded {
    #[error("Cannot resample image to horizontal resolution {dpi}, because it would exceed the maximum width of {max} pixels.")]
    Horizontal { dpi: f64, max: isize },
    #[error("Cannot resample image to vertical resolution {dpi}, because it would exceed the maximum height of {max} pixels.")]
    Vertical { dpi: f64, max: isize },
}

image_filter!(resample(img: Image, context: &Context, x_dpi: f32, y_dpi: f32, filter: Option<FilterType>) {
    let x_dpi = x_dpi as f64;
    let y_dpi = y_dpi as f64;
    let filter = filter.unwrap_or_default();
    let (orig_x_dpi, orig_y_dpi) = img.resolution()?;
    let cfg = &config_ref::<Config>().image;
    if let Some(max_w) = cfg.max_width {
        let w = img.width() as f64 * x_dpi / orig_x_dpi;
        if w as isize > max_w {
            Err(DimensionsExceeded::Horizontal { dpi: x_dpi, max: max_w })?
        }
    }

    if let Some(max_h) = cfg.max_height {
        let h = img.height() as f64 * y_dpi / orig_y_dpi;
        if h as isize > max_h {
            Err(DimensionsExceeded::Vertical { dpi: y_dpi, max: max_h })?
        }
    }

    img.resample(x_dpi, y_dpi, &filter)?;
});

image_filter!(trim(img: Image, context: &Context, ) {
    img.trim(15.0)?;
});

image_filter!(crop(img: Image, context: &Context, x: isize, y: isize, w: isize, h: isize) {
    img.crop(x, y, w as usize, h as usize)?;
});

image_filter!(extend(img: Image, context: &Context, x: isize, y: isize, mut w: isize, mut h: isize) {
    w -= x;
    h -= y;
    img.extend(x, y, w as usize, h as usize)?;
});

image_filter!(extend_from_center(img: Image, context: &Context, width: isize, height: isize) {
    let x = (img.width() as isize - width) / 2;
    let y = (img.height() as isize - height) / 2;
    let w = width;
    let h = height;
    img.extend(x, y, w as usize, h as usize)?;
});

image_filter!(format(img: Image, context: &Context, format: ImageFormat) {
    img.set_format(&format)?;
});

pub fn compose(context: Context, args: &Args) -> Future<'_> {
    let dst = arg_type!(compose, args, 0, context.clone(), Image);
    let src = arg_type!(compose, args, 1, context, Image);
    Box::pin(async move {
        let src = src.await?;
        let dst = dst.await?;

        let op = arg_type!(compose, args, 2, dst, CompositeOperator);
        let x = arg_type!(compose, args, 3, dst, isize);
        let y = arg_type!(compose, args, 4, dst, isize);

        Ok(dst.compose(&op, &src, x, y).map(|_| dst.into())?)
    })
}

image_filter!(colorspace(img: Image, context: &Context, colorspace: Colorspace) {
    img.set_colorspace(&colorspace)?;
});

image_filter!(alpha(img: Image, context: &Context, alpha_channel: AlphaChannel) {
    img.set_alpha_channel(&alpha_channel)?;
});

image_filter!(quality(img: Image, context: &Context, q: isize) {
    img.set_quality(q as usize)?;
});

image_filter!(compression(img: Image, context: &Context, compression: CompressionType) {
    img.set_compression(&compression)?;
});

#[derive(Debug, Error)]
#[error("profile: The image cannot be converted, becuase the image is in {image:?} colorspace instead of {src:?}.")]
pub struct ColorspaceConversionError {
    image: Colorspace,
    src: Colorspace,
}

image_filter!(profile(img: Image, context: &Context, source: ColorProfile, dest: ColorProfile) {
    let image_colorspace = img.colorspace();
    let source_colorspace: Colorspace = (&source).into();
    if image_colorspace != source_colorspace {
        Err(ColorspaceConversionError { image: image_colorspace, src: source_colorspace })?
    }
    img.transform_color_profile(&source, &dest)?;
});

image_filter!(flip(img: Image, context: &Context,) {
    img.flip()?;
});

image_filter!(flop(img: Image, context: &Context,) {
    img.flop()?;
});

image_filter!(cm(img: Image, context: &Context, x: f32, y: f32) {
    let x = x * 0.3937008;
    let y = y * 0.3937008;
    let x_dpi = img.width() as f32 / x;
    let y_dpi = img.height() as f32 / y;

    img.set_resolution_unit(&ResolutionUnit::PixelsPerInch)?;
    img.set_resolution(x_dpi as f64, y_dpi as f64)?;
});

image_filter!(dpi(img: Image, context: &Context, horizontal: f32, vertical: f32) {
    img.set_resolution_unit(&ResolutionUnit::PixelsPerInch)?;
    img.set_resolution(horizontal as f64, vertical as f64)?;
});

image_filter!(sepia(img: Image, context: &Context, threshold: f32) {
    img.sepia(threshold as f64)?;
});

image_filter!(gravity(img: Image, context: &Context, gravity: Gravity) {
    img.set_gravity(&gravity)?;
});

image_filter!(repeat(img: Image, context: &Context, count_x: isize, count_y: isize, offset_x: isize, offset_y: isize) {
    for x in 1..count_x {
        img.compose(&CompositeOperator::Over, &img, x * offset_x, 0)?;
    }
    for y in 1..count_y {
        img.compose(&CompositeOperator::Over, &img, 0, y * offset_y)?;
    }
});

image_filter!(canvas(img: Image, context: &Context, border_x: f32, border_y: f32) {
    // Remove the 1px border from the image in case it contains artifacts from some
    // earlier processing.
    let border_x = border_x as f64;
    let border_y = border_y as f64;
    img.extend(1, 1, img.width()-2, img.height()-2)?;

    let mut flipped = img.clone();
    flipped.flip()?;

    let mut flopped = img.clone();
    flopped.flop()?;

    let mut flipflopped = img.clone();
    flipflopped.flip()?;
    flipflopped.flop()?;

    let (x_dpi, y_dpi) = img.resolution()?;
    let (w, h) = (img.width(), img.height());
    let x_border = (x_dpi * border_x * 0.3937008) as isize; // convert the border to inches
    let y_border = (y_dpi * border_y * 0.3937008) as isize; // convert the border to inches
    img.extend(-x_border, -y_border, w + (x_border as usize)*2, h + (y_border as usize)*2)?;
    img.compose(&CompositeOperator::Over, &flopped, x_border - w as isize, y_border)?;
    img.compose(&CompositeOperator::Over, &flopped, x_border + w as isize, y_border)?;
    img.compose(&CompositeOperator::Over, &flipped, x_border, y_border - h as isize)?;
    img.compose(&CompositeOperator::Over, &flipped, x_border, y_border + h as isize)?;
    img.compose(&CompositeOperator::Over, &flipflopped, x_border - w as isize, y_border + h as isize)?;
    img.compose(&CompositeOperator::Over, &flipflopped, x_border + w as isize, y_border - h as isize)?;
    img.compose(&CompositeOperator::Over, &flipflopped, x_border - w as isize, y_border - h as isize)?;
    img.compose(&CompositeOperator::Over, &flipflopped, x_border + w as isize, y_border + h as isize)?;
});

image_filter!(background(img: Image, context: &Context, color: String) {
    img.set_background_color(&color)?;
});

#[cfg(test)]
mod tests {
    use std::sync::{Arc, Mutex};

    use assert2::{check, let_assert};
    use image::io::Reader as ImageReader;
    use image::{GenericImageView, Pixel, Rgba};
    use imaginator::filter::{Context, ContextData, FilterArg, SizeUnit};
    use imaginator::img;
    use imaginator::{HardwareLocker, PluginInformation};
    use std::io::Cursor;

    use crate::brightness_contrast;

    use super::{color_rect, plugin};

    static PLUGIN_INFO: once_cell::sync::Lazy<PluginInformation> =
        once_cell::sync::Lazy::new(plugin);

    fn context() -> Context {
        let data = ContextData::new(&PLUGIN_INFO.filters, &None, locker());

        Context::new(data)
    }

    fn rgba_to_hex<T>(color: &Rgba<T>) -> String
    where
        T: std::fmt::UpperHex,
    {
        format!(
            "#{:02X}{:02X}{:02X}{:02X}",
            color.0[0], color.0[1], color.0[2], color.0[3]
        )
    }

    fn locker() -> HardwareLocker {
        HardwareLocker::new(None, Arc::new(Mutex::new(0)))
    }

    #[track_caller]
    fn image(width: usize, height: usize, color: Rgba<u8>) -> img::Image {
        let_assert!(Ok(mut img) = img::Image::new(None::<&[u8]>, None, locker()));
        let_assert!(Ok(_) = img.empty_image(&rgba_to_hex(&color), width, height, None));
        img
    }

    #[track_caller]
    fn rustimage_from_buf<T: AsRef<[u8]>>(bytes: T) -> image::DynamicImage {
        let_assert!(Ok(img) = ImageReader::new(Cursor::new(bytes)).with_guessed_format());
        let_assert!(Ok(img) = img.decode());
        img
    }

    #[track_caller]
    fn rustimage_from_img(image: &img::Image) -> image::DynamicImage {
        let bytes = image
            .encode(img::ImageFormat::PNG)
            .expect("failed to retrieve image buffer");
        rustimage_from_buf(bytes)
    }

    // we require multithreaded runtime for HardwareLocker to work
    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn test_color_rect() {
        let width = 20_u32;
        let height = 30_u32;
        let color = Rgba::from([255, 0, 0, 0]);

        let context = context();
        let args = vec![
            FilterArg::String(rgba_to_hex(&color)),
            FilterArg::Int(width as _, SizeUnit::None),
            FilterArg::Int(height as _, SizeUnit::None),
        ];
        let_assert!(Ok(result) = color_rect(context, &args).await);

        let_assert!(Ok(ctype) = result.content_type());
        check!(ctype == "image/png");

        let_assert!(Ok(bytes) = result.content());
        let img = rustimage_from_buf(bytes);

        check!(img.width() == width);
        check!(img.height() == height);

        check!(img.get_pixel(5, 10) == color);
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 1)]
    async fn test_brightness_contrast() {
        let width = 10_u32;
        let height = 10_u32;
        let color = Rgba::from([0, 0, 0, 0]);

        let brightness = 2.0;
        let contrast = 1.0;

        let image = image(width as _, height as _, color);

        let luma_pixel = {
            let img = rustimage_from_img(&image);
            img.get_pixel(5, 5).to_luma()
        };

        let context = context();
        let args = vec![
            FilterArg::ResolvedImg(image),
            FilterArg::Float(brightness, SizeUnit::None),
            FilterArg::Float(contrast, SizeUnit::None),
        ];
        let_assert!(Ok(result) = brightness_contrast(context, &args).await);

        let_assert!(Ok(ctype) = result.content_type());
        check!(ctype == "image/png");

        let_assert!(Ok(bytes) = result.content());
        let filtered_img = rustimage_from_buf(bytes);

        check!(filtered_img.width() == width);
        check!(filtered_img.height() == height);

        // new pixel should be brighter than the original
        check!(filtered_img.get_pixel(5, 5).to_luma().0 > luma_pixel.0);
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 5)]
    async fn empty_leak_test() {
        let context = context();

        let pid = std::process::id();
        let heaptrack = match std::env::var("HEAPTRACK") {
            Ok(val) => {
                if val == "1" {
                    println!("running heaptrack for pid: {}", pid);
                    Some(
                        tokio::process::Command::new("heaptrack")
                            .arg("-o")
                            .arg("/tmp/heaptrack")
                            .arg("-p")
                            .arg(pid.to_string())
                            .spawn()
                            .expect("failed to spawn heaptrack"),
                    )
                } else {
                    None
                }
            }
            Err(_) => None,
        };

        let mut handles = vec![];
        // 10 actors
        for _ in 0..std::env::var("REQ_COUNT")
            .unwrap_or("1".to_string())
            .parse::<usize>()
            .unwrap()
        {
            //tries
            for _ in 0..200 {
                let context_clone = context.clone();
                let handle = tokio::spawn(async move {
                    let _img2 = color_rect(
                        context_clone,
                        &vec![
                            FilterArg::String("#f6f6f6".to_string()),
                            FilterArg::Int(1200, SizeUnit::None),
                            FilterArg::Int(1200, SizeUnit::None),
                        ],
                    )
                    .await
                    .unwrap()
                    .content()
                    .unwrap();
                });
                handles.push(handle);
            }
        }

        for handle in handles {
            handle.await.unwrap();
        }

        if let Some(mut heaptrack) = heaptrack {
            tokio::time::sleep(tokio::time::Duration::from_secs(1)).await;
            // send SIGINT to heaptrack
            heaptrack.kill().await.unwrap();
            println!("to run heaptrack gui run: heaptrack_gui /tmp/heaptrack.zst");
        }
    }

    #[tokio::test(flavor = "multi_thread", worker_threads = 10)]
    async fn composite_filter() {
        let mut handles = vec![];
        for _ in 0..1000 {
            let join_handle = tokio::spawn(async {
                let width = 560_u32;
                let height = 746_u32;

                let color = Rgba::from([0, 0, 0, 0]);
                let image1 = image(width as _, height as _, color);

                let luma_pixel = {
                    let img = rustimage_from_img(&image1);
                    img.get_pixel(5, 5).to_luma()
                };

                let color2 = Rgba::from([5, 5, 5, 5]);
                let image2 = image(width as _, height as _, color2);

                let context = context();
                let args = vec![
                    FilterArg::ResolvedImg(image1),
                    FilterArg::ResolvedImg(image2),
                    FilterArg::String("darken".to_string()),
                    FilterArg::Int(0, SizeUnit::None),
                    FilterArg::Int(0, SizeUnit::None),
                ];
                let_assert!(Ok(result) = super::compose(context.clone(), &args).await);
                let_assert!(Ok(img) = result.image());

                // brighthen
                let brightness = 1.02;
                let contrast = 1.0;

                let image_brightened = {
                    let args = vec![
                        FilterArg::ResolvedImg(img),
                        FilterArg::Float(brightness, SizeUnit::None),
                        FilterArg::Float(contrast, SizeUnit::None),
                    ];
                    let_assert!(
                        Ok(result) = super::brightness_contrast(context.clone(), &args).await
                    );
                    result
                };
                let_assert!(Ok(img) = image_brightened.image());

                let quality = 85;
                let image_quality = {
                    let args = vec![
                        FilterArg::ResolvedImg(img),
                        FilterArg::Int(quality, SizeUnit::None),
                    ];
                    let_assert!(Ok(result) = super::quality(context, &args).await);
                    result
                };

                let_assert!(Ok(bytes) = image_quality.content());
                let img = rustimage_from_buf(bytes);

                check!(img.width() == width);
                check!(img.height() == height);

                // new pixel should be brighter than the original
                check!(img.get_pixel(5, 5).to_luma().0 > luma_pixel.0);
            });
            handles.push(join_handle);
        }

        for handle in handles {
            handle.await.unwrap();
        }
    }
}
