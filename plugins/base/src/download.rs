use crate::{http_client, Config};
use anyhow::Result;
use bytes::Bytes;
use futures::{future::BoxFuture, FutureExt};
use hyper::StatusCode;
use imaginator::cfg::config_ref;
use imaginator::filter::{Args, Context, ErrorResponse, Future};
use imaginator::img::UnknownImageFormat;
use imaginator::prelude::*;
use imaginator::{img, HardwareLocker};
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Clone, Error)]
#[error("Url {url} returned {status_code}.")]
pub struct DownloadError {
    pub url: String,
    pub status_code: StatusCode,
}

#[derive(PartialEq, Eq, Debug, Clone, Error)]
#[error("Invalid url {url}.")]
pub struct InvalidUrl {
    pub url: String,
}

#[derive(PartialEq, Eq, Debug, Clone, Error)]
#[error("Url {url} failed with {status_code} and message {message}")]
pub struct RequestError {
    pub url: String,
    pub status_code: StatusCode,
    pub message: String,
}

impl RequestError {
    pub fn with_url_message(url: impl Into<String>, message: impl Into<String>) -> Self {
        Self {
            url: url.into(),
            message: message.into(),
            status_code: StatusCode::BAD_REQUEST,
        }
    }
}

#[derive(Debug, Clone)]
pub struct DownloadResult {
    dpi: Option<f64>,
    content_type: Option<String>,
    buffer: Bytes,
    url_arg: String,
    locker: HardwareLocker,
}

impl PartialEq for DownloadResult {
    fn eq(&self, o: &Self) -> bool {
        self.dpi == o.dpi
            && self.content_type == o.content_type
            && self.buffer == o.buffer
            && self.url_arg == o.url_arg
    }
}

impl FilterResult for DownloadError {
    fn content_type(&self) -> Result<String> {
        Ok("text/plain".to_owned())
    }

    fn status_code(&self) -> StatusCode {
        self.status_code
    }

    fn content(&self) -> Result<Bytes> {
        Ok(Bytes::from(format!("{}", self)))
    }
}

impl FilterResult for InvalidUrl {
    fn content_type(&self) -> Result<String> {
        Ok("text/plain".to_owned())
    }

    fn status_code(&self) -> StatusCode {
        StatusCode::BAD_REQUEST
    }

    fn content(&self) -> Result<Bytes> {
        Ok(Bytes::from(format!("{}", self)))
    }
}

impl FilterResult for RequestError {
    fn content_type(&self) -> Result<String> {
        Ok("text/plain".to_owned())
    }

    fn status_code(&self) -> StatusCode {
        self.status_code
    }

    fn content(&self) -> Result<Bytes> {
        Ok(Bytes::copy_from_slice(self.message.as_bytes()))
    }
}

impl FilterResult for DownloadResult {
    fn content_type(&self) -> Result<String> {
        let mut image = img::Image::new(None::<[u8; 0]>, self.dpi, self.locker.clone())?;
        if image.ping(&*self.buffer).is_err() {
            return Ok(self
                .content_type
                .clone()
                .unwrap_or("application/octet-stream".to_owned()));
        }
        let image_format = image.format()?;
        if let Some(ref formats) = config_ref::<Config>().image.supported_formats {
            if !formats.contains(&image_format) {
                return Err(UnknownImageFormat::new(
                    image_format.into(),
                    self.url_arg.clone(),
                ))?;
            }
        }
        Ok(image.format()?.into())
    }

    fn content(&self) -> Result<Bytes> {
        Ok(self.buffer.clone())
    }

    fn image(self: Box<Self>) -> Result<img::Image> {
        let image = img::Image::new(Some(&*self.buffer), self.dpi, self.locker.clone())?;
        let image_format = image.format()?;
        if let Some(ref formats) = config_ref::<Config>().image.supported_formats {
            if !formats.contains(&image_format) {
                return Err(UnknownImageFormat::new(
                    image_format.into(),
                    self.url_arg.clone(),
                ))?;
            }
        }
        Ok(image)
    }
}

pub fn decode_url(url: &str) -> Result<String> {
    let mut split = url.splitn(2, ':');
    let config = config_ref::<Config>();
    let domains = &config.domains;
    let domain = split.next().ok_or_else(|| {
        ErrorResponse(Box::new(RequestError::with_url_message(
            url,
            "Missing domain alias",
        )))
    })?;
    let resource = split.next().ok_or_else(|| {
        ErrorResponse(Box::new(RequestError::with_url_message(
            url,
            "Missing resource path",
        )))
    })?;

    if let Some(domain) = domains.get(domain) {
        let mut url = domain.clone();
        url.push_str(resource);
        Ok(url)
    } else {
        Err(ErrorResponse(Box::new(InvalidUrl {
            url: url.to_owned(),
        })))?
    }
}

pub fn download_url<'a>(url_arg: &str) -> BoxFuture<'a, Result<(Option<String>, Bytes)>> {
    let url_arg = url_arg.to_owned();

    async move {
        let url = decode_url(&url_arg)?;
        let request = http_client().get(&url).send();
        let response = request.await?;
        if response.status() != StatusCode::OK {
            Err(ErrorResponse(Box::new(DownloadError {
                url: url_arg,
                status_code: response.status(),
            })))?
        }
        let content_type = response
            .headers()
            .get("content-type")
            .and_then(|v| v.to_str().ok())
            .map(|v| v.to_owned());
        Ok((content_type, response.bytes().await?))
    }
    .boxed()
}

pub fn filter(context: Context, args: &Args) -> Future<'_> {
    let url_arg = arg_type!(download, args, 0, String);
    let dpi = if args.len() > 1 {
        Some(arg_type!(download, args, 1, isize) as f64)
    } else {
        None
    };
    let response = download_url(&url_arg);
    async move {
        if let Some(header_name) = context.log_filters_header {
            context
                .response_headers()?
                .entry(header_name.clone())
                .and_modify(|value| {
                    value.push('(');
                    value.push_str(
                        url_arg
                            .split_once(':')
                            .or_else(|| url_arg.split_once(','))
                            .map(|arg| arg.0)
                            .unwrap_or_else(|| &url_arg),
                    );
                    value.push(')');
                });
        }
        let (content_type, img) = response.await?;
        Ok(Box::new(DownloadResult {
            dpi,
            content_type,
            buffer: img,
            url_arg,
            locker: context.cpu_core_guard.clone(),
        })
        .into())
    }
    .boxed()
}
