use ::imaginator::img::ImageFormat;
use anyhow::anyhow;
use imaginator::systemd_creds::{ReadCredential, ReadCredentialDir, ReadCredentialError};
use serde_humanize_rs;
use std::collections::HashMap;
use std::convert::TryFrom;
use std::fmt::Debug;
use std::hash::{Hash, Hasher};
use std::time::Duration;

/// Size of buffer length to read entire JSON metadata of cache file
/// In other words this is number of bytes to read which will be cast to integer with total bytes of JSON metadata
pub const JSON_LEN_BUFFER_SIZE: usize = std::mem::size_of::<u32>();

#[derive(Serialize, Deserialize, Debug, Clone, Default)]
pub struct ImageConfig {
    pub max_width: Option<isize>,
    pub max_height: Option<isize>,
    pub supported_formats: Option<Vec<ImageFormat>>,
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Cache {
    pub dir: String,
    #[serde(deserialize_with = "serde_humanize_rs::deserialize")]
    pub size: usize,
    #[serde(default)]
    pub read_only: bool,
}

const fn default_path_style_host() -> bool {
    true
}

#[derive(Serialize, Deserialize, Clone, Hash, Eq, PartialEq)]
pub struct S3Cache {
    pub bucket_name: String,
    pub region: String,
    pub endpoint: String,
    pub access_key: String,
    pub secret_key: String,
    pub project_id: String,
    #[serde(deserialize_with = "serde_humanize_rs::deserialize")]
    pub timeout: Option<Duration>,
    #[serde(default = "default_path_style_host")]
    pub path_style_host: bool,
}

impl Debug for S3Cache {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("S3Cache")
            .field("bucket_name", &self.bucket_name)
            .field("region", &self.region)
            .field("endpoint", &self.endpoint)
            .field("access_key", &"********")
            .field("secret_key", &"********")
            .field("project_id", &self.project_id)
            .field("timeout", &self.timeout)
            .field("path_style_host", &self.path_style_host)
            .finish()
    }
}

impl S3Cache {
    /// Calculates a hash that will be used to cache s3 client instances.
    /// Configuration allows for multiple s3 caches, so we need to be able to
    /// differentiate between them when building the client.
    pub fn hash(&self) -> u64 {
        let mut hasher = std::collections::hash_map::DefaultHasher::new();
        self.bucket_name.hash(&mut hasher);
        self.region.hash(&mut hasher);
        self.endpoint.hash(&mut hasher);
        self.access_key.hash(&mut hasher);
        self.secret_key.hash(&mut hasher);
        self.path_style_host.hash(&mut hasher);
        hasher.finish()
    }
}

#[derive(Serialize, Deserialize, Debug, Clone)]
#[serde(try_from = "inner::ConfigWrapped")]
pub struct Config {
    #[serde(default)]
    pub image: ImageConfig,
    pub domains: HashMap<String, String>,
    pub caches: HashMap<String, Cache>,
    pub accept_invalid_tls_certs: bool,
    pub s3_caches: HashMap<String, S3Cache>,
}

mod inner {
    use super::*;

    #[derive(Serialize, Deserialize, Debug, Clone)]
    pub(super) struct ConfigWrapped {
        #[serde(default)]
        pub image: ImageConfig,
        pub domains: HashMap<String, String>,
        pub caches: HashMap<String, Cache>,
        pub accept_invalid_tls_certs: bool,
        pub s3_caches: HashMap<String, S3Cache>,
    }

    impl TryFrom<ConfigWrapped> for Config {
        type Error = anyhow::Error;

        fn try_from(mut value: ConfigWrapped) -> Result<Self, Self::Error> {
            value
                .load_credentials(ReadCredentialDir)
                .map_err(|e| anyhow!("Error loading credentials: {}", e))?;
            Ok(Config {
                image: value.image,
                domains: value.domains,
                caches: value.caches,
                accept_invalid_tls_certs: value.accept_invalid_tls_certs,
                s3_caches: value.s3_caches,
            })
        }
    }

    impl ConfigWrapped {
        pub fn load_credentials(
            &mut self,
            creds: impl ReadCredential,
        ) -> Result<(), anyhow::Error> {
            let global_access_key = match creds.read_credential("global_s3_access_key") {
                Ok(pass) => Some(pass),
                Err(ReadCredentialError::CredentialDirectoryNotSet)
                | Err(ReadCredentialError::CredentialDoesNotExist(_)) => None,
                Err(e) => return Err(anyhow!("Error reading global_s3_access_key: {}", e)),
            };

            let global_secret_key = match creds.read_credential("global_s3_secret_key") {
                Ok(pass) => Some(pass),
                Err(ReadCredentialError::CredentialDirectoryNotSet)
                | Err(ReadCredentialError::CredentialDoesNotExist(_)) => None,
                Err(e) => return Err(anyhow!("Error reading global_s3_secret_key: {}", e)),
            };

            if global_access_key.is_some() ^ global_secret_key.is_some() {
                return Err(anyhow!(
                    "global_s3_access_key and global_s3_secret_key must be both set or both unset"
                ));
            }

            for (name, s3_cache) in &mut self.s3_caches {
                let mut specific_access_key = match creds
                    .read_credential(&format!("s3_access_key_{}", name))
                {
                    Ok(pass) => Some(pass),
                    Err(ReadCredentialError::CredentialDirectoryNotSet)
                    | Err(ReadCredentialError::CredentialDoesNotExist(_)) => None,
                    Err(e) => return Err(anyhow!("Error reading s3_access_key_{}: {}", name, e)),
                };
                let mut specific_secret_key = match creds
                    .read_credential(&format!("s3_secret_key_{}", name))
                {
                    Ok(pass) => Some(pass),
                    Err(ReadCredentialError::CredentialDirectoryNotSet)
                    | Err(ReadCredentialError::CredentialDoesNotExist(_)) => None,
                    Err(e) => return Err(anyhow!("Error reading s3_secret_key_{}: {}", name, e)),
                };

                if specific_access_key.is_some() ^ specific_secret_key.is_some() {
                    return Err(anyhow!(
                        "s3_access_key_{} and s3_secret_key_{} must be both set or both unset",
                        name,
                        name
                    ));
                }

                if specific_access_key.is_some() {
                    s3_cache.access_key = specific_access_key.take().unwrap();
                } else if global_access_key.is_some() {
                    s3_cache.access_key = global_access_key.clone().unwrap();
                }

                if specific_secret_key.is_some() {
                    s3_cache.secret_key = specific_secret_key.take().unwrap();
                } else if global_secret_key.is_some() {
                    s3_cache.secret_key = global_secret_key.clone().unwrap();
                }
            }

            Ok(())
        }
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use super::inner::*;
    use super::*;

    #[test]
    fn check_initialization_empty_caches_no_passwords() {
        let mut config = ConfigWrapped {
            image: ImageConfig::default(),
            domains: HashMap::new(),
            caches: HashMap::new(),
            accept_invalid_tls_certs: false,
            s3_caches: HashMap::new(),
        };

        struct EmptyCreds;

        impl ReadCredential for EmptyCreds {
            fn read_credential(&self, _name: &str) -> Result<String, ReadCredentialError> {
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        assert!(config.load_credentials(EmptyCreds).is_ok());

        struct ErrorOnGlobalCreds;

        impl ReadCredential for ErrorOnGlobalCreds {
            fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError> {
                if name == "global_s3_access_key" {
                    return Err(ReadCredentialError::CredentialEmpty(
                        "global_s3_access_key".to_string(),
                    ));
                }
                if name == "global_s3_secret_key" {
                    return Err(ReadCredentialError::CredentialEmpty(
                        "global_s3_secret_key".to_string(),
                    ));
                }
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        assert!(config.load_credentials(ErrorOnGlobalCreds).is_err());
    }

    #[test]
    fn test_only_one_global_key_set() {
        let mut config = ConfigWrapped {
            image: ImageConfig::default(),
            domains: HashMap::new(),
            caches: HashMap::new(),
            accept_invalid_tls_certs: false,
            s3_caches: HashMap::new(),
        };

        struct OnlyAccessKey;

        impl ReadCredential for OnlyAccessKey {
            fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError> {
                if name == "global_s3_access_key" {
                    return Ok("access_key".to_string());
                }
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        assert!(config.load_credentials(OnlyAccessKey).is_err());
    }

    #[test]
    fn global_secret_fails_due_to_io() {
        let mut config = ConfigWrapped {
            image: ImageConfig::default(),
            domains: HashMap::new(),
            caches: HashMap::new(),
            accept_invalid_tls_certs: false,
            s3_caches: HashMap::new(),
        };

        struct SecretFails;

        impl ReadCredential for SecretFails {
            fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError> {
                if name == "global_s3_access_key" {
                    return Ok("access_key".to_string());
                }
                if name == "global_s3_secret_key" {
                    return Err(ReadCredentialError::ReadCredentials(
                        PathBuf::from(""),
                        std::io::Error::new(std::io::ErrorKind::Other, "test"),
                    ));
                }
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        assert!(config.load_credentials(SecretFails).is_err());
    }

    #[test]
    fn test_specific_key_missing() {
        let mut config = ConfigWrapped {
            image: ImageConfig::default(),
            domains: HashMap::new(),
            caches: HashMap::new(),
            accept_invalid_tls_certs: false,
            s3_caches: HashMap::from([(
                "specific_cache".to_string(),
                S3Cache {
                    bucket_name: "specific_bucket".to_string(),
                    region: "region".to_string(),
                    endpoint: "https://url".to_string(),
                    access_key: "ak".to_string(),
                    secret_key: "sk".to_string(),
                    project_id: "id".to_string(),
                    timeout: None,
                    path_style_host: false,
                },
            )]),
        };

        struct SpecificKeyMissing;

        impl ReadCredential for SpecificKeyMissing {
            fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError> {
                if name == "s3_access_key_specific_cache" {
                    return Ok("access_key".to_string());
                }
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        assert!(config.load_credentials(SpecificKeyMissing).is_err());
    }

    #[test]
    fn test_global_and_specific_default_work() {
        let config_source = ConfigWrapped {
            image: ImageConfig::default(),
            domains: HashMap::new(),
            caches: HashMap::new(),
            accept_invalid_tls_certs: false,
            s3_caches: HashMap::from([
                (
                    "specific_cache".to_string(),
                    S3Cache {
                        bucket_name: "specific_bucket".to_string(),
                        region: "region".to_string(),
                        endpoint: "https://url".to_string(),
                        access_key: "ak".to_string(),
                        secret_key: "sk".to_string(),
                        project_id: "id".to_string(),
                        timeout: None,
                        path_style_host: false,
                    },
                ),
                (
                    "global_cache".to_string(),
                    S3Cache {
                        bucket_name: "global_bucket".to_string(),
                        region: "region".to_string(),
                        endpoint: "https://url".to_string(),
                        access_key: "ak_global".to_string(),
                        secret_key: "sk_global".to_string(),
                        project_id: "id".to_string(),
                        timeout: None,
                        path_style_host: false,
                    },
                ),
            ]),
        };

        struct EmptyCreds;

        impl ReadCredential for EmptyCreds {
            fn read_credential(&self, _name: &str) -> Result<String, ReadCredentialError> {
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        let mut config = config_source.clone();
        assert!(config.load_credentials(EmptyCreds).is_ok());

        struct GlobalCreds;

        impl ReadCredential for GlobalCreds {
            fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError> {
                if name == "global_s3_access_key" {
                    return Ok("AK".to_string());
                }
                if name == "global_s3_secret_key" {
                    return Ok("SK".to_string());
                }
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        let mut config = config_source.clone();

        assert!(config.load_credentials(GlobalCreds).is_ok());

        config.s3_caches.iter().for_each(|(_, cache)| {
            assert_eq!(cache.access_key, "AK".to_string());
            assert_eq!(cache.secret_key, "SK".to_string());
        });

        struct SpecificCreds;

        impl ReadCredential for SpecificCreds {
            fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError> {
                if name == "s3_access_key_specific_cache" {
                    return Ok("AK_specific".to_string());
                }
                if name == "s3_secret_key_specific_cache" {
                    return Ok("SK_specific".to_string());
                }
                Err(ReadCredentialError::CredentialDoesNotExist(PathBuf::from(
                    "",
                )))
            }
        }

        let mut config = config_source.clone();
        assert!(config.load_credentials(SpecificCreds).is_ok());

        let specific = config.s3_caches.get("specific_cache").unwrap();
        assert_eq!(specific.access_key, "AK_specific".to_string());
        assert_eq!(specific.secret_key, "SK_specific".to_string());

        // global has been left untouched
        let global = config.s3_caches.get("global_cache").unwrap();
        assert_eq!(global.access_key, "ak_global".to_string());
        assert_eq!(global.secret_key, "sk_global".to_string());
    }
}
