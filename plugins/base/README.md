# Plugins

## Using filters.

Filters are designed to be used as a composable pipeline that each operation process and performs a single operation on an image.

Each pipeline shall start with `download` operation.

Result of the whole pipeline is result from last operation.

### Composing

To create a pipeline compose operations to perform a single operation. So, for example

```
flip(extend(download(http://my.img/test.png),0,0,100px,100px))
```

This will create a pipeline that will

1. Download an image from `http://my.img/test.png`. Returns there with error code if any error occurs (e.g. bad url).
2. Extends the image with zero offset to 100px square.
3. Flips the image.

So as an result you should obtain flipped square (100x100px) of an original image.

## List of available operations on image.

### Downloading image

This step must be your first operation invoked. Simply downloads image and loads it for further processing. This operation has following signature:

```
download(source, dpi?)
```

where `source` parameter should be downloadable URL and `dpi` is optional dpi resolution for this image. Please note that original dpi of the image is lost and won't be loaded even if you do not provide `dpi` parameter. This may effect other image processing operations.

### Caching

For caching you have two options available: local disk and S3 protocol. They both can be used simultaneously. They cache result of all operations that were invoked before them.
If cache was not loaded all operations will be performed.

For configuring cache filters please go to [root's README.md](../../README.md) of this repository.

### Image manipulation operations

Below is present list of all available functions that manipulate image. They must be called only after some image is loaded via `download` operation.

- `resize(image, width, height, filter_type?)` - check `FilterType` for available list of options.
- `fit-in(image, width, height)`
- `trim(image)` - always trim using fuzz of 15.0
- `crop(image, x, y, width, height)`
- `extend(image, x, y, width, height)`
- `extend-from-center(image, x, y, width, height)`
- `format(image, format)` - check `ImageFormat` for list of available formats.
- `repeat(image, count_x, count_y, offset_x, offset_y)`
- `colorspace(image, colorspace)` - check `Colorspace` for available list of options.
- `profile(image, source_color_profile, destination_color_profile)` - check `ColorProfile` for available list of options.
- `compression(image, compression_type)` - check `ComperssionType` for available list of options.
- `resample(image, x_dpi, y_dpi, filter_type?)`
- `canvas(image, border_x, border_y)`
- `cm(image, x, y)`
- `sepia(image, threshold)`
- `flip(image)`
- `flop(image)`
- `dpi(image, horizontal, vertical)`
- `alpha(image, alpha_channel` - check `AlphaChannel` for available list of options.
- `gravity(image, gravity)`
- `bg(image, color)`
- `quality(image, quality)`

In order to add a new image manipulate operation register it in plugin heatmap in [lib.rs](./src/lib.rs) file.

Please note that different units are available. List of available units can be found in the repository root [README.md](../../README.md))

#### Developing image manipulation operation.

Each image filter should be passed to `image_filter!` macro, and each function should have one of following signatures:
- `image_filter!(fn custom_filter(img: Image) {})`
- `image_filter!(fn custom_filter(img: Image, my_arg: u32, ...) {})`
- `image_filter!(fn custom_filter(img: Image, context: &Context))`
- `image_filter!(fn custom_filter(img: Image, context: &Context, my_arg: u32, ...) {})`

### Composing images

To compose two images use `compose` operation. This operation has following signature:

```
compose(destination, source, composite_operator, x, y)
```

For list of available options on `composite_operator` parameter check `CompositeOperator`.


## Developing new filters guidelines.

### Defining error handling and validation.

Errors shall be constructed via `thiserror` package and defined as enum at the top of the file.

Each validation rule broken shall return meaningful message to the user.

### Filter result

Result of each pipeline should return `FilterResult`. Take note that even [DownloadError](./src/download.rs) implements it, so you can freely implement it.

Filter result will be used as and result of the filter even if it will fail it may still return error implementing `FilterResult` for customized output.

### Filter signature

Each filter that is not an `image_filter` shall be constructed with the following signature:

     pub fn filter<'a>(context: Context, args: &'a Args) -> Future<'a> {...}

### Parsing args

For parsing your filter arguments use [arg_type!](../../common/src/filter.rs) macro. Stick to normal code conventions when defining your filter, so
firstly should be required parameters, then default parameters and optional parameters.

### Performance

In regard to performance, non-blocking operations should be used as much as possible. Also take note that you can have your filter ran within request lifecycle:

```rust
pub fn filter<'a>(context: Context, args: &'a Args) -> Future<'a> {
    // This will be executed before whole pipeline processing   
    async move {
        // this will be executed in whole pipeline processing
    }
    .boxed()
}
```