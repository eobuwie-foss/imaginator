extern crate imaginator_common as imaginator;
use imaginator::ImaginatorExtension;
use std::collections::HashMap;

use imaginator::prelude::*;
use serde_derive::{Deserialize, Serialize};

pub trait Check: serde::Serialize {}

#[derive(Serialize)]
#[serde(rename_all = "camelCase")]
pub struct HealthResponse {
    status: &'static str,
    version: &'static str,
    release_id: &'static str,
    nodes: &'static [&'static str],
    description: &'static str,
    checks: &'static [Box<dyn erased_serde::Serialize>],
}

#[derive(Serialize, Deserialize, Debug, Clone)]
pub struct Config {}

pub fn plugin() -> PluginInformation {
    PluginInformation::new(HashMap::with_capacity(1))
        .with_middleware(&health_checks)
        .with_post_init(&post_init)
}

static POST_INIT_CALLED: std::sync::atomic::AtomicBool = std::sync::atomic::AtomicBool::new(false);

fn post_init() {
    POST_INIT_CALLED.store(true, std::sync::atomic::Ordering::SeqCst);
}

fn health_checks(req: &Request) -> BoxFuture<'_, anyhow::Result<Option<Response>>> {
    async move {
        match req.uri().path() {
            "/_system/liveness" => liveness(req),
            "/_system/health" => readiness(req),
            "/_system/readiness" => readiness(req),
            _ => Ok(None),
        }
    }
    .boxed()
}

fn liveness(_req: &Request) -> anyhow::Result<Option<Response>> {
    Ok(Some(
        hyper::Response::builder()
            .status(200)
            .body("".into())
            .unwrap(),
    ))
}

fn readiness(req: &Request) -> anyhow::Result<Option<Response>> {
    let ext: &ImaginatorExtension = req.extensions().get().unwrap();
    let post_init_called = POST_INIT_CALLED.load(std::sync::atomic::Ordering::SeqCst);

    let resp = serde_json::to_string(&HealthResponse {
        status: if post_init_called {
            "ok"
        } else {
            "initializing"
        },
        version: ext.major(),
        release_id: ext.full_version(),
        nodes: &[],
        description: "Health check for image processing service",
        checks: &[],
    })
    .unwrap();

    Ok(Some(
        hyper::Response::builder()
            .status(if post_init_called { 200 } else { 503 })
            .body(resp.into())
            .unwrap(),
    ))
}
