# this is copied from
# https://gitlab.com/eobuwie/checkout/checkout-bff/-/blob/master/semver.py
import sys
import re

from datetime import datetime

latest_tag = sys.argv[1]
commit_title = sys.argv[2]
branch_name = sys.argv[3]
commit_sha = sys.argv[4]

version = ""

# @TODO we need to remember about changing this branch name to master when we will fix issues with this repository and stabilize application
# https://ccc-group.atlassian.net/browse/CORE-2034
if branch_name == "master":
    tag_split = latest_tag.split(".")

    if "feature" in commit_title:
        tag_split[1] = str(int(tag_split[1]) + 1)
        tag_split[2] = "0"
    if "major" in commit_title:
        tag_split[0] = str(int(tag_split[0]) + 1)
        tag_split[1] = "0"
        tag_split[2] = "0"
    else: # hotfix
        tag_split[2] = str(int(tag_split[2]) + 1)

    version = ".".join(tag_split)
else:
    regex = r"\b([A-Z]{3,}-\d{3,})\b"
    task_id = re.search(regex, branch_name)
    
    if task_id is None:
        current_date = datetime.now()
        date = current_date.strftime("%Y%m%d")
        version = date + "-" + commit_sha
    else:
        version = task_id.group(1) + "-" + commit_sha

print("export APP_VERSION='{}'".format(version))

