//! Based on [ecs-logger](https://github.com/ciffelia/ecs-logger) crate
use chrono::{DateTime, Utc};
use sentry_tracing::EventFilter;
use serde::{Deserialize, Serialize};
use std::fmt::Write;
use std::path::Path;
use tracing::field::Visit;
use tracing::Subscriber;
use tracing_core::span::{Attributes, Id};

use tracing_subscriber::{
    fmt::{format, FmtContext, FormatEvent, FormatFields},
    layer::{Context, Layer, SubscriberExt},
    registry::LookupSpan,
    util::SubscriberInitExt,
    EnvFilter,
};

use otel_tracing::{otel_layer, TracingGuard};

pub mod otel_tracing;

/// Represents Elastic Common Schema version.
const ECS_VERSION: &str = "1.2.0";

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub enabled: bool,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct Log {
    pub logger: &'static str,
    pub level: String,
}

impl Log {
    fn new<'a>(record: &'a tracing_core::Event<'a>) -> Self {
        Self {
            logger: "ECS Logger",
            level: record.metadata().level().to_string(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct Service<'env> {
    pub name: &'static str,
    pub version: &'static str,
    pub environment: &'env str,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize)]
pub struct BodyMockup {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bytes: Option<usize>,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize)]
pub struct HttpRequest {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub method: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub referer: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub bytes: Option<usize>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub user_agent: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_length: Option<String>,
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<String>,

    pub body: BodyMockup,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize)]
pub struct HttpResponse {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub status_code: Option<u16>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub mime_type: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub latency: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub content_length: Option<String>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct Http {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub version: Option<String>,
    pub request: HttpRequest,
    pub response: HttpResponse,
}

#[derive(Default, Debug, Clone, PartialEq, Eq, Serialize)]
pub struct Url {
    #[serde(skip_serializing_if = "Option::is_none")]
    pub path: Option<String>,

    #[serde(skip_serializing_if = "Option::is_none")]
    pub query: Option<String>,
}

/// Information about the source code which logged the message.
///
/// <https://www.elastic.co/guide/en/ecs/current/ecs-log.html>
#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct LogOrigin<'a> {
    /// Representation of the source code which logged the message.
    ///
    /// Mapped to `log.origin.file` field.
    pub file: LogOriginFile<'a>,

    /// Rust-specific information about the source code which logged the
    /// message.
    ///
    /// Mapped to `log.origin.rust` field.
    pub rust: LogOriginRust<'a>,
}

/// Representation of the source code which logged the message.
///
/// <https://www.elastic.co/guide/en/ecs/current/ecs-log.html>
#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct LogOriginFile<'a> {
    /// The line number of the source code which logged the message.
    ///
    /// Mapped to `log.origin.file.line` field.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub line: Option<u32>,

    /// The filename of the source code which logged the message.
    ///
    /// Mapped to `log.origin.file.name` field.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub name: Option<&'a str>,
}

/// Rust-specific information about the source code which logged the message.
#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct LogOriginRust<'a> {
    /// The name of the log target.
    ///
    /// Mapped to `log.origin.rust.target` field.
    pub target: &'a str,

    /// The module path of the source code which logged the message.
    ///
    /// Mapped to `log.origin.rust.module_path` field.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub module_path: Option<&'a str>,

    /// The file path of the source code which logged the message.
    ///
    /// Mapped to `log.origin.rust.file_path` field.
    #[serde(skip_serializing_if = "Option::is_none")]
    pub file_path: Option<&'a str>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct Labels {
    #[serde(rename = "rust.target")]
    pub target: String,
    #[serde(rename = "rust.module_path")]
    pub module_path: String,
    #[serde(rename = "rust.file_path")]
    pub file: String,
    #[serde(rename = "file.line")]
    pub line: String,
    #[serde(rename = "file.name")]
    pub file_path: String,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub request_id: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub correlation_id: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub correlation_origin: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub causation_id: Option<String>,
    #[serde(default, skip_serializing_if = "Option::is_none")]
    pub causation_origin: Option<String>,
}

/// Representation of an event compatible with ECS logging.
///
/// The event follows [ECS Logging spec](https://github.com/elastic/ecs-logging/tree/master/spec).
#[derive(Debug, Clone, PartialEq, Eq, Serialize)]
pub struct Event<'env> {
    pub log: Log,
    pub service: Service<'env>,
    pub labels: Labels,
    pub tags: Vec<String>,
    pub http: Option<Http>,

    /// Url data representation
    /// Mapped to `url.*` fields
    pub url: Option<Url>,

    /// Date and time when the message is logged.
    ///
    /// Mapped to `@timestamp` field.
    #[serde(rename = "@timestamp")]
    pub timestamp: DateTime<Utc>,

    /// The message body.
    ///
    /// Mapped to `message` field.
    pub message: String,

    /// ECS version this event conforms to.
    ///
    /// Mapped to `ecs.version` field.
    #[serde(rename = "ecs.version")]
    pub ecs_version: &'static str,
}

pub struct StringVisitor<'a> {
    string: &'a mut String,
}

impl<'a> Visit for StringVisitor<'a> {
    fn record_debug(&mut self, _field: &tracing::field::Field, value: &dyn std::fmt::Debug) {
        write!(self.string, "{:?} ", value).unwrap();
    }
}

pub const CORRELATION_ID: &str = "correlation_id";
pub const CORRELATION_ORIGIN: &str = "correlation_origin";
pub const CAUSATION_ID: &str = "causation_id";
pub const CAUSATION_ORIGIN: &str = "causation_origin";
pub const REQUEST_ID: &str = "request_id";

const CORRELATION_FIELDS_LENGTH: usize = 5;
static CORRELATION_FIELDS: [&str; CORRELATION_FIELDS_LENGTH] = [
    CORRELATION_ID,
    CORRELATION_ORIGIN,
    CAUSATION_ID,
    CAUSATION_ORIGIN,
    REQUEST_ID,
];

#[derive(Default)]
struct CorrelationContextLayer {}

impl CorrelationContextLayer {
    pub fn new() -> Self {
        Self::default()
    }
}

#[derive(Debug, Clone)]
pub struct CorrelationExtension {
    inner: [Option<String>; CORRELATION_FIELDS_LENGTH],
}

impl CorrelationExtension {
    fn new() -> Self {
        Self {
            inner: [None, None, None, None, None],
        }
    }

    fn get(&self, field: &str) -> Option<String> {
        let index = CORRELATION_FIELDS.iter().position(|&i| i == field);
        index.and_then(|i| self.inner[i].clone())
    }
}

impl Visit for CorrelationExtension {
    fn record_debug(&mut self, field: &tracing::field::Field, value: &dyn std::fmt::Debug) {
        let index = CORRELATION_FIELDS.iter().position(|&i| i == field.name());
        if let Some(index) = index {
            let mut val = String::new();
            write!(val, "{:?}", value).unwrap();
            self.inner[index] = Some(val);
        }
    }

    fn record_str(&mut self, field: &tracing::field::Field, value: &str) {
        let index = CORRELATION_FIELDS.iter().position(|&i| i == field.name());
        if let Some(index) = index {
            self.inner[index] = Some(value.to_string());
        }
    }
}

impl<S> Layer<S> for CorrelationContextLayer
where
    S: tracing::Subscriber + for<'lookup> tracing_subscriber::registry::LookupSpan<'lookup>,
{
    fn on_new_span(&self, attrs: &Attributes<'_>, id: &Id, ctx: Context<'_, S>) {
        let span = ctx.span(id).expect("Span not found, this is a bug");
        let mut ext = span.extensions_mut();
        let values = attrs.values();
        let mut correlation_extension = CorrelationExtension::new();

        values.record(&mut correlation_extension);
        ext.insert(correlation_extension);
    }
}

impl<'a, 'env> Event<'env> {
    pub fn new(
        timestamp: DateTime<Utc>,
        event: &'a tracing_core::Event<'a>,
        env: &'env str,
        app_name: &'static str,
        correlation: &CorrelationExtension,
        http_extension: &HttpContextExtension,
    ) -> Self {
        let meta = event.metadata();
        let file_path = meta.file().map(Path::new);
        let mut message = String::new();
        event.record(&mut StringVisitor {
            string: &mut message,
        });

        Self {
            timestamp,
            log: Log::new(event),
            service: Service {
                name: app_name,
                version: env!("CARGO_PKG_VERSION"),
                environment: env,
            },
            message,
            ecs_version: ECS_VERSION,
            labels: Labels {
                target: meta.target().into(),
                module_path: meta.module_path().unwrap_or_default().into(),
                file: meta.file().unwrap_or_default().into(),
                line: meta.line().unwrap_or_default().to_string(),
                file_path: file_path
                    .and_then(|p| p.file_name())
                    .and_then(|os_str| os_str.to_str())
                    .unwrap_or_default()
                    .into(),
                request_id: correlation.get("request_id"),
                correlation_id: correlation.get("correlation_id"),
                correlation_origin: correlation.get("correlation_origin"),
                causation_id: correlation.get("causation_id"),
                causation_origin: correlation.get("causation_origin"),
            },
            tags: vec![],
            http: Some(http_extension.http.clone()),
            url: Some(http_extension.url.clone()),
        }
    }
}

#[derive(Clone)]
pub struct HttpContextExtension {
    pub http: Http,
    pub url: Url,
}

// są też nazwy otelowe, ale nie chce robić reexportu.
pub const HTTP_ROUTE: &str = "http.route";

pub const HTTP_RESPONSE_MIME_TYPE: &str = "http.response.mime_type";
pub const HTTP_RESPONSE_STATUS_CODE: &str = "http.response.status_code";
pub const HTTP_RESPONSE_LATENCY: &str = "http.response,latency";
pub const HTTP_RESPONSE_CONTENT_LENGTH: &str = "http.response.content_length";

pub const HTTP_REQUEST_METHOD: &str = "http.request.method";
pub const HTTP_REQUEST_CONTENT_LENGTH: &str = "http.request.content_length";

pub const URL_PATH: &str = "url.path";
pub const URL_QUERY: &str = "url.query";

pub const USER_AGENT: &str = "user_agent.original";

impl HttpContextExtension {
    fn new() -> Self {
        Self {
            http: Http {
                version: None,
                request: HttpRequest::default(),
                response: HttpResponse::default(),
            },
            url: Url::default(),
        }
    }

    fn record_internal(&mut self, field: &tracing_core::Field, value: &str) {
        if field.name() == HTTP_RESPONSE_MIME_TYPE {
            self.http.response.mime_type = Some(value.to_owned());
        }

        if field.name() == HTTP_REQUEST_METHOD {
            self.http.request.method = Some(value.to_owned());
        }

        if field.name() == HTTP_RESPONSE_STATUS_CODE {
            self.http.response.status_code = Some(value.parse().unwrap_or(0));
        }

        if field.name() == HTTP_RESPONSE_LATENCY {
            self.http.response.latency = Some(value.to_string());
        }

        if field.name() == HTTP_RESPONSE_CONTENT_LENGTH {
            self.http.response.content_length = Some(value.to_string());
        }

        if field.name() == HTTP_REQUEST_CONTENT_LENGTH {
            self.http.request.content_length = Some(value.to_string());
        }

        // matched route - not full route
        if field.name() == HTTP_ROUTE {
            self.http.request.path = Some(value.to_string());
        }

        if field.name() == URL_PATH {
            self.url.path = Some(value.to_string());
        }

        if field.name() == URL_QUERY {
            self.url.query = Some(value.to_string());
        }

        if field.name() == USER_AGENT {
            self.http.request.user_agent = Some(value.to_string());
        }
    }
}

impl Visit for HttpContextExtension {
    fn record_str(&mut self, field: &tracing_core::Field, value: &str) {
        self.record_internal(field, value);
    }

    fn record_debug(&mut self, field: &tracing_core::Field, value: &dyn std::fmt::Debug) {
        let mut val = String::new();
        write!(val, "{:?}", value).unwrap();
        self.record_internal(field, &val);
    }
}

struct HttpContextLayer {}

impl HttpContextLayer {
    fn new() -> Self {
        Self {}
    }
}

impl<S> Layer<S> for HttpContextLayer
where
    S: tracing::Subscriber + for<'lookup> tracing_subscriber::registry::LookupSpan<'lookup>,
{
    fn on_new_span(
        &self,
        attrs: &tracing_core::span::Attributes<'_>,
        id: &tracing_core::span::Id,
        ctx: tracing_subscriber::layer::Context<'_, S>,
    ) {
        let span = ctx.span(id).expect("Span must exist!");
        // find root span
        if let Some(span) = span.scope().from_root().next() {
            let mut ext = span.extensions_mut();

            match ext.get_mut::<HttpContextExtension>() {
                Some(http_extension) => {
                    let values = attrs.values();
                    values.record(http_extension);
                }
                None => {
                    let mut http_extension = HttpContextExtension::new();
                    let values = attrs.values();
                    values.record(&mut http_extension);
                    ext.insert(http_extension);
                }
            }
        }
    }

    fn on_record(
        &self,
        id: &tracing_core::span::Id,
        values: &tracing_core::span::Record<'_>,
        ctx: tracing_subscriber::layer::Context<'_, S>,
    ) {
        let span = ctx.span(id).expect("Span must exist, on record!");
        // find root span
        if let Some(span) = span.scope().from_root().next() {
            let mut ext = span.extensions_mut();

            match ext.get_mut::<HttpContextExtension>() {
                Some(http_extension) => values.record(http_extension),
                None => tracing::error!("No http context extension"),
            }
        }
    }
}

struct EcsFormatter {
    env: String,
    app_name: &'static str,
}

impl<S, N> FormatEvent<S, N> for EcsFormatter
where
    S: Subscriber + for<'a> LookupSpan<'a>,
    N: for<'a> FormatFields<'a> + 'static,
{
    fn format_event(
        &self,
        ctx: &FmtContext<'_, S, N>,
        mut writer: format::Writer<'_>,
        event: &tracing_core::Event<'_>,
    ) -> std::fmt::Result {
        let mut correlation_extension = CorrelationExtension::new();
        let mut http_extension = HttpContextExtension::new();
        if let Some(scope) = ctx.event_scope() {
            for span in scope.from_root() {
                let ext = span.extensions();
                if ext.get::<CorrelationExtension>().is_some() {
                    correlation_extension = ext.get::<CorrelationExtension>().unwrap().clone();
                }
                if ext.get::<HttpContextExtension>().is_some() {
                    http_extension = ext.get::<HttpContextExtension>().unwrap().clone();
                }
            }
        };

        let event = Event::new(
            Utc::now(),
            event,
            &self.env,
            self.app_name,
            &correlation_extension,
            &http_extension,
        );

        writeln!(writer, "{}", serde_json::to_string(&event).unwrap())
    }
}

// Initialize tracing-subscriber and return OtelGuard for opentelemetry-related termination processing
pub fn init() -> TracingGuard {
    let env = std::env::var("ENV").unwrap_or_else(|_| "prod".into());
    let format_layer = if std::env::var("LOGGER_TYPE") == Ok("ECS".to_owned()) {
        tracing_subscriber::fmt::layer::<_>()
            .event_format(formatter("imaginator"))
            .boxed()
    } else {
        tracing_subscriber::fmt::layer().pretty().boxed()
    };

    let correlation_context_layer = CorrelationContextLayer::new();

    let http_context_layer = HttpContextLayer::new();

    #[cfg(feature = "sentry")]
    let sentry_layer = sentry_tracing::layer().event_filter(|md| match md.level() {
        &tracing::Level::ERROR => EventFilter::Event,
        _ => EventFilter::Ignore,
    });

    let env_filter = tracing_subscriber::EnvFilter::from_default_env();

    if cfg!(feature = "sentry") {
        tracing_subscriber::registry()
            .with(env_filter)
            .with(format_layer)
            .with(correlation_context_layer)
            .with(http_context_layer)
            .with(sentry_layer)
            .with(otel_layer(env))
            .init();
    } else {
        tracing_subscriber::registry()
            .with(env_filter)
            .with(format_layer)
            .with(correlation_context_layer)
            .with(http_context_layer)
            .with(otel_layer(env))
            .init();
    }

    TracingGuard {}
}

pub fn init_old() {
    if std::env::var("LOGGER_TYPE") == Ok("ECS".to_string()) {
        ecs_init("imaginator");
    } else if cfg!(feature = "sentry") {
        let correlation_context_layer = CorrelationContextLayer::new();
        let http_context_layer = HttpContextLayer::new();
        tracing_subscriber::registry()
            .with(tracing_subscriber::fmt::layer().with_filter(EnvFilter::from_default_env()))
            .with(correlation_context_layer)
            .with(http_context_layer)
            .with(sentry_tracing::layer().event_filter(|md| match md.level() {
                &tracing::Level::ERROR => sentry_tracing::EventFilter::Event,
                _ => sentry_tracing::EventFilter::Ignore,
            }))
            .init();
    } else {
        tracing_subscriber::registry()
            .with(tracing_subscriber::fmt::layer().with_filter(EnvFilter::from_default_env()))
            .init();
    }
}

fn ecs_init(app_name: &'static str) {
    let correlation_context_layer = CorrelationContextLayer::new();
    let http_context_layer = HttpContextLayer::new();
    let event_filter = EnvFilter::from_default_env();
    if cfg!(feature = "sentry") {
        tracing_subscriber::fmt()
            .event_format(formatter(app_name))
            .with_env_filter(event_filter)
            .finish()
            .with(correlation_context_layer)
            .with(http_context_layer)
            .with(sentry_tracing::layer().event_filter(|md| match md.level() {
                &tracing::Level::ERROR => sentry_tracing::EventFilter::Event,
                _ => sentry_tracing::EventFilter::Ignore,
            }))
            .init();
    } else {
        tracing_subscriber::fmt::fmt()
            .event_format(formatter(app_name))
            .with_env_filter(event_filter)
            .finish()
            .with(correlation_context_layer)
            .with(http_context_layer)
            .init();
    }
}

fn formatter(app_name: &'static str) -> EcsFormatter {
    EcsFormatter {
        env: std::env::var("ENV").unwrap_or_else(|_| "prod".into()),
        app_name,
    }
}

#[cfg(test)]
mod tests {
    use crate::formatter;
    use std::sync::Mutex;
    use tracing::info;
    use tracing_subscriber::layer::SubscriberExt;
    use tracing_subscriber::util::SubscriberInitExt;
    use tracing_test::internal::MockWriter;

    static mut BUFFER: Option<Mutex<Vec<u8>>> = None;

    fn buffer() -> &'static Mutex<Vec<u8>> {
        unsafe { BUFFER.get_or_insert_with(|| Mutex::new(Vec::with_capacity(1_024))) }
    }

    #[test]
    fn format_msg() {
        let buffer = buffer();
        let writer: MockWriter = MockWriter::new(buffer);
        let layer = tracing_subscriber::fmt::layer()
            .event_format(formatter("test msg format"))
            .with_writer(writer);
        tracing_subscriber::registry().with(layer).init();

        info!("Message");

        let b = buffer.lock().unwrap();
        let res = std::str::from_utf8(&b).unwrap();
        assert!(res.contains("Message"));
        assert!(res.contains("\"logger\":\"ECS Logger\""));
        assert!(res.contains("\"name\":\"test msg format\""));
    }
}
