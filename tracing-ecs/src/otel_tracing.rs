use opentelemetry::propagation::TextMapCompositePropagator;
use opentelemetry::{global, KeyValue};
use opentelemetry_otlp::WithExportConfig;
use opentelemetry_sdk::propagation::{BaggagePropagator, TraceContextPropagator};
use opentelemetry_sdk::trace::{BatchConfig, RandomIdGenerator, Sampler, Tracer};
use opentelemetry_sdk::{runtime, Resource};

use opentelemetry_semantic_conventions::{
    resource::{DEPLOYMENT_ENVIRONMENT, SERVICE_NAME, SERVICE_VERSION},
    SCHEMA_URL,
};

use tracing_core::Subscriber;
use tracing_opentelemetry::OpenTelemetryLayer;
use tracing_subscriber::prelude::*;

/// Returns a tracing layer that will use the OpenTelemetry SDK to export spans
/// to the configured OTLP endpoint.
/// There are 3 environment variables that can be used to configure the layer:
/// - `OTLP_ENABLED`: if set to `true`, the layer will be enabled. Defaults to `false`.
/// - `OTLP_ENDPOINT`: the endpoint to which spans will be exported. Required if `OTLP_ENABLED` is set to `true`.
/// - `SAMPLE_RATE`: the rate at which spans will be sampled. Defaults to 1.0.
pub fn otel_layer<
    T: Subscriber + Send + Sync + for<'span> tracing_subscriber::registry::LookupSpan<'span>,
>(
    env: String,
) -> Box<dyn tracing_subscriber::Layer<T> + std::marker::Send + Sync> {
    if std::env::var("OTLP_ENABLED") == Ok("true".to_owned()) {
        OpenTelemetryLayer::new(init_tracer_otlp(env)).boxed()
    } else {
        tracing_opentelemetry::layer().boxed()
    }
}

fn init_tracer_otlp(env: String) -> Tracer {
    let propagator = TextMapCompositePropagator::new(vec![
        Box::new(BaggagePropagator::new()),
        Box::new(TraceContextPropagator::new()),
    ]);
    global::set_text_map_propagator(propagator);

    let otlp_endpoint =
        std::env::var("OTLP_ENDPOINT")
        .expect("OTLP_ENDPOINT environment variable is not set it is required when OTLP_ENABLED is set to true");
    let sample_rate = std::env::var("SAMPLE_RATE")
        .unwrap_or_else(|_| "1.0".into())
        .parse::<f64>()
        .unwrap_or(1.0);

    opentelemetry_otlp::new_pipeline()
        .tracing()
        .with_trace_config(
            opentelemetry_sdk::trace::Config::default()
                // Customize sampling strategy
                .with_sampler(Sampler::ParentBased(Box::new(Sampler::TraceIdRatioBased(
                    sample_rate,
                ))))
                .with_id_generator(RandomIdGenerator::default())
                .with_resource(resource(env)),
        )
        .with_batch_config(BatchConfig::default())
        .with_exporter(
            opentelemetry_otlp::new_exporter()
                .http()
                .with_endpoint(otlp_endpoint),
        )
        .install_batch(runtime::Tokio)
        .unwrap()
}

pub struct TracingGuard {}
impl Drop for TracingGuard {
    fn drop(&mut self) {
        opentelemetry::global::shutdown_tracer_provider();
    }
}

fn resource(env: String) -> Resource {
    Resource::from_schema_url(
        [
            KeyValue::new(SERVICE_NAME, "imaginator"),
            KeyValue::new(SERVICE_VERSION, env!("CARGO_PKG_VERSION")),
            KeyValue::new(DEPLOYMENT_ENVIRONMENT, env),
        ],
        SCHEMA_URL,
    )
}
