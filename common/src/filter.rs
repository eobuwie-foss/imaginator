use crate::img::{self, Image};
use crate::metrics::FILTER_DURATION;
use crate::{url, CensoredError, CpuNotAvailable, HardwareLocker};
use anyhow::Result;
use bytes::Bytes;
use core::pin::Pin;
use enum_as_inner::EnumAsInner;
use futures::{future::BoxFuture, Future as FutureTrait, FutureExt};
use hyper;
use std::collections::HashMap;
use std::fmt::Display;
use std::fmt::{self, Debug};
use std::str::FromStr;
use std::sync::{Arc, Mutex, MutexGuard};
use thiserror::Error;
use tracing::Instrument;

pub type BoxedFuture<T> = Pin<Box<dyn FutureTrait<Output = T>>>;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("No such filter: {0}")]
pub struct UnknownFilter(pub String);

impl CensoredError for UnknownFilter {
    fn censored_error(&self) -> &'static str {
        "Unknown Filter"
    }
}

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum SizeUnit {
    None,
    Px,
    Width,
    Height,
    HorizontalCentimeters,
    VerticalCentimeters,
    HorizontalInches,
    VerticalInches,
}

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Fractional px values are not supported")]
pub struct FractionalPixelError();

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Poisoned mutex: {0}")]
pub struct PoisonedMutex(&'static str);

#[derive(Debug, Clone, EnumAsInner)]
pub enum FilterArg {
    Int(isize, SizeUnit),
    Float(f32, SizeUnit),
    String(String),
    Img(Filter),
    ResolvedImg(img::Image),
    Add(Box<FilterArg>, Box<FilterArg>),
    Sub(Box<FilterArg>, Box<FilterArg>),
    Mul(Box<FilterArg>, Box<FilterArg>),
    Div(Box<FilterArg>, Box<FilterArg>),
}

#[derive(Debug, Clone, PartialEq)]
pub struct Filter {
    pub name: String,
    pub args: Vec<FilterArg>,
}

impl PartialEq for FilterArg {
    fn eq(&self, other: &Self) -> bool {
        use FilterArg::*;
        match (self, other) {
            (Int(a, au), Int(b, bu)) => a == b && au == bu,
            (Float(a, au), Float(b, bu)) => (a - b).abs() < f32::EPSILON && au == bu,
            (String(a), String(b)) => a == b,
            (Img(a), Img(b)) => a == b,
            (Sub(a, b), Sub(c, d)) => a == c && b == d,
            _ => false,
        }
    }
}

impl Display for FilterArg {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            FilterArg::Int(ref v, ref u) => write!(f, "{}{:?}", v, u),
            FilterArg::Float(ref v, ref u) => write!(f, "{}{:?}", v, u),
            FilterArg::String(ref v) => write!(f, "{}", v),
            FilterArg::Img(ref v) => write!(f, "{}", v),
            FilterArg::ResolvedImg(_) => unimplemented!(),
            FilterArg::Add(ref a, ref b) => write!(f, "{}+{}", a, b),
            FilterArg::Sub(ref a, ref b) => write!(f, "{}-{}", a, b),
            FilterArg::Mul(ref a, ref b) => write!(f, "{}*{}", a, b),
            FilterArg::Div(ref a, ref b) => write!(f, "{}/{}", a, b),
        }
    }
}

impl Display for Filter {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}(", self.name)?;
        write!(
            f,
            "{}",
            self.args
                .iter()
                .map(|arg| format!("{}", arg))
                .collect::<Vec<_>>()
                .join(",")
        )?;
        write!(f, ")")
    }
}

#[derive(Debug, Error)]
#[error("ErrorResponse")] // This shouldn't actually be used.
pub struct ErrorResponse(pub Box<dyn FilterResult + Sync + Send>);

#[derive(Debug, Error)]
#[error("This filter does not return an image!")]
pub struct NotAnImage();

#[derive(Debug, Error)]
#[error("Argument {arg} to `{filter_name}` must be of type {type_name}")]
pub struct InvalidArgumentType {
    arg: FilterArg,
    filter_name: String,
    type_name: String,
}

#[derive(Debug, Error)]
#[error("Required argument {arg_num} to `{filter_name}` is missing")]
pub struct MissingArgument {
    arg_num: usize,
    filter_name: String,
}

#[derive(Debug, Error)]
#[error("Argument {arg} has an unit that can't be used here.")]
pub struct InvalidUnit {
    arg: FilterArg,
}

pub trait FilterResult: fmt::Debug + Send + Sync {
    fn content_type(&self) -> Result<String>;
    fn dpi(&self) -> Result<(f64, f64)> {
        Err(NotAnImage())?
    }
    fn status_code(&self) -> hyper::StatusCode {
        hyper::StatusCode::OK
    }
    fn content(&self) -> Result<Bytes>;
    fn image(self: Box<Self>) -> Result<img::Image> {
        Err(NotAnImage())?
    }
}

impl FilterResult for img::Image {
    fn content_type(&self) -> Result<String> {
        Ok((&self.format()?).into())
    }

    fn dpi(&self) -> Result<(f64, f64)> {
        Ok(self.resolution()?)
    }

    fn content(&self) -> Result<Bytes> {
        Ok(Bytes::from(self.encode(self.format()?)?))
    }

    fn image(self: Box<Self>) -> Result<img::Image> {
        Ok(*self)
    }
}

impl FilterResult for ErrorResponse {
    fn content_type(&self) -> Result<String> {
        self.0.content_type()
    }
    fn status_code(&self) -> hyper::StatusCode {
        self.0.status_code()
    }
    fn content(&self) -> Result<Bytes> {
        self.0.content()
    }
    fn image(self: Box<Self>) -> Result<img::Image> {
        self.0.image()
    }
}

impl<T: FilterResult + 'static> From<Box<T>> for Box<dyn FilterResult> {
    fn from(obj: Box<T>) -> Self {
        obj
    }
}

pub fn as_future_image(f: Box<FutureImage>) -> Box<FutureImage> {
    f
}

pub type FutureImage<'a> = BoxFuture<'a, Result<Box<img::Image>>>;
pub type Future<'a> = BoxFuture<'a, Result<Box<dyn FilterResult>>>;
pub type Args = Vec<FilterArg>;
pub type FilterMap = HashMap<&'static str, &'static (dyn Fn(Context, &Args) -> Future + Sync)>;

pub struct ContextData {
    pub filters: &'static FilterMap,
    pub log_filters_header: &'static Option<String>,
    pub cpu_core_guard: HardwareLocker,
    pub _response_headers: Mutex<HashMap<String, String>>,
}

pub type Context = Arc<ContextData>;

impl std::fmt::Debug for ContextData {
    fn fmt(&self, fmt: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        fmt.debug_struct("ContextData").finish()
    }
}

impl ContextData {
    pub fn new(
        filters: &'static FilterMap,
        log_filters_header: &'static Option<String>,
        cpu_core_guard: HardwareLocker,
    ) -> Self {
        ContextData {
            filters,
            log_filters_header,
            cpu_core_guard,
            _response_headers: Mutex::new(HashMap::with_capacity(100)),
        }
    }

    pub fn response_headers(&self) -> Result<MutexGuard<HashMap<String, String>>> {
        self._response_headers
            .lock()
            .map_err(|_| PoisonedMutex("response_headers").into())
    }

    pub fn request_blocking(&self) -> Result<(), CpuNotAvailable> {
        tracing::debug!("ctx request blocking");
        self.cpu_core_guard.clone().request_blocking()
    }
}

pub fn parse_size<T: Into<f32>>(val: T, unit: &SizeUnit, img: &Image) -> Result<f32> {
    let val = val.into();
    Ok(match *unit {
        SizeUnit::None | SizeUnit::Px => val,
        SizeUnit::Width => val * img.width() as f32,
        SizeUnit::Height => val * img.height() as f32,
        SizeUnit::HorizontalCentimeters => val * 0.3937008 * img.resolution()?.0 as f32,
        SizeUnit::VerticalCentimeters => val * 0.3937008 * img.resolution()?.1 as f32,
        SizeUnit::HorizontalInches => val * img.resolution()?.0 as f32,
        SizeUnit::VerticalInches => val * img.resolution()?.1 as f32,
    })
}

fn parse_int_expr(length: &FilterArg, name: &str, img: Option<&Image>) -> Result<isize> {
    match length {
        FilterArg::Int(ref val, ref unit) => match *unit {
            SizeUnit::None | SizeUnit::Px => Ok(*val),
            _ => match img {
                Some(img) => Ok(parse_size(*val as f32, unit, img)? as isize),
                None => Err(InvalidUnit {
                    arg: length.clone(),
                })?,
            },
        },
        FilterArg::Float(ref val, ref unit) => match *unit {
            SizeUnit::None | SizeUnit::Px => Err(FractionalPixelError())?,
            _ => match img {
                Some(img) => Ok(parse_size(*val, unit, img)? as isize),
                None => Err(InvalidUnit {
                    arg: length.clone(),
                })?,
            },
        },
        FilterArg::Add(ref a, ref b) => {
            Ok(parse_int_expr(a, name, img)? + parse_int_expr(b, name, img)?)
        }
        FilterArg::Sub(ref a, ref b) => {
            Ok(parse_int_expr(a, name, img)? - parse_int_expr(b, name, img)?)
        }
        FilterArg::Mul(ref a, ref b) => {
            Ok(parse_int_expr(a, name, img)? * parse_int_expr(b, name, img)?)
        }
        FilterArg::Div(ref a, ref b) => {
            Ok(parse_int_expr(a, name, img)? / parse_int_expr(b, name, img)?)
        }
        _ => Err(InvalidArgumentType {
            arg: length.clone(),
            filter_name: name.to_owned(),
            type_name: "integer".to_owned(),
        })?,
    }
}

fn parse_float_expr(length: &FilterArg, name: &str, img: Option<&Image>) -> Result<f32> {
    match length {
        FilterArg::Int(ref val, ref unit) => match img {
            Some(img) => Ok(parse_size(*val as f32, unit, img)?),
            None => Err(InvalidUnit {
                arg: length.clone(),
            })?,
        },
        FilterArg::Float(ref val, ref unit) => match img {
            Some(img) => Ok(parse_size(*val, unit, img)?),
            None => Err(InvalidUnit {
                arg: length.clone(),
            })?,
        },
        FilterArg::Add(ref a, ref b) => {
            Ok(parse_float_expr(a, name, img)? + parse_float_expr(b, name, img)?)
        }
        FilterArg::Sub(ref a, ref b) => {
            Ok(parse_float_expr(a, name, img)? - parse_float_expr(b, name, img)?)
        }
        FilterArg::Mul(ref a, ref b) => {
            Ok(parse_float_expr(a, name, img)? * parse_float_expr(b, name, img)?)
        }
        FilterArg::Div(ref a, ref b) => {
            Ok(parse_float_expr(a, name, img)? / parse_float_expr(b, name, img)?)
        }
        _ => Err(InvalidArgumentType {
            arg: length.clone(),
            filter_name: name.to_owned(),
            type_name: "float".to_owned(),
        })?,
    }
}

pub trait ArgType: Sized {
    /// This trait exists to be used by the arg_type! macro.
    ///
    /// It exists, because this macro has to return a future, and therefore,
    /// depends on the futures crate. It could just `use futures;`, but then
    /// this code would be just pasted at call size, requiring the caller to
    /// `extern crate futures;`. This is undesirable.
    /// If we extract this dependency to something defined here, like a function,
    /// this problem is solved.
    fn arg_type(name: &str, args: &Args, i: usize) -> Result<Self>;
}

pub trait ArgTypeImg: Sized + FromStr {
    /// This trait is similar to AugType, but requires an additional parameter and returns
    /// an immediate result instead of a future.
    /// And it has a default implementation for enum support.
    fn arg_type_img(name: &str, typename: &str, args: &Args, i: usize, _img: &Image) -> Result<Self>
    where
        <Self as FromStr>::Err: std::error::Error + Send + Sync + 'static,
    {
        match args.get(i) {
            Some(FilterArg::String(val)) => Ok(val.parse::<Self>()?),
            Some(other) => Err(InvalidArgumentType {
                arg: other.clone(),
                filter_name: name.to_owned(),
                type_name: typename.to_owned(),
            })?,
            None => Err(MissingArgument {
                arg_num: i + 1,
                filter_name: name.to_owned(),
            })?,
        }
    }
}

pub trait ArgTypeContext<'a>: Sized {
    /// This trait is similar to AugType, but requires a Context.
    fn arg_type_context(name: &str, args: &'a Args, i: usize, context: Context) -> Self;
}

impl ArgType for String {
    fn arg_type(name: &str, args: &Args, i: usize) -> Result<Self> {
        match args.get(i) {
            Some(FilterArg::String(s)) => Ok(s.clone()),
            Some(other) => Err(InvalidArgumentType {
                arg: other.clone(),
                filter_name: name.to_owned(),
                type_name: "string".to_owned(),
            })?,
            None => Err(MissingArgument {
                arg_num: i + 1,
                filter_name: name.to_owned(),
            })?,
        }
    }
}

impl ArgType for isize {
    fn arg_type(name: &str, args: &Args, i: usize) -> Result<Self> {
        match args.get(i) {
            Some(val) => parse_int_expr(val, name, None),
            None => Err(MissingArgument {
                arg_num: i + 1,
                filter_name: name.to_owned(),
            })?,
        }
    }
}

impl ArgTypeImg for String {
    fn arg_type_img(name: &str, _: &str, args: &Args, i: usize, _img: &Image) -> Result<Self> {
        match args.get(i) {
            Some(FilterArg::String(s)) => Ok(s.clone()),
            Some(other) => Err(InvalidArgumentType {
                arg: other.clone(),
                filter_name: name.to_owned(),
                type_name: "string".to_owned(),
            })?,
            None => Err(MissingArgument {
                arg_num: i + 1,
                filter_name: name.to_owned(),
            })?,
        }
    }
}

impl ArgTypeImg for isize {
    fn arg_type_img(name: &str, _: &str, args: &Args, i: usize, img: &Image) -> Result<Self> {
        match args.get(i) {
            Some(val) => parse_int_expr(val, name, Some(img)),
            None => Err(MissingArgument {
                arg_num: i + 1,
                filter_name: name.to_owned(),
            })?,
        }
    }
}

impl ArgTypeImg for f32 {
    fn arg_type_img(name: &str, _: &str, args: &Args, i: usize, img: &Image) -> Result<Self> {
        match args.get(i) {
            Some(val) => parse_float_expr(val, name, Some(img)),
            None => Err(MissingArgument {
                arg_num: i + 1,
                filter_name: name.to_owned(),
            })?,
        }
    }
}

impl ArgType for Filter {
    fn arg_type(name: &str, args: &Args, i: usize) -> Result<Self> {
        match args.get(i) {
            Some(FilterArg::Img(val)) => Ok(val.clone()),
            Some(other) => Err(InvalidArgumentType {
                arg: other.clone(),
                filter_name: name.to_owned(),
                type_name: "float".to_owned(),
            })?,
            None => Err(MissingArgument {
                arg_num: i + 1,
                filter_name: name.to_owned(),
            })?,
        }
    }
}

impl<'a> ArgTypeContext<'a> for FutureImage<'a> {
    fn arg_type_context(name: &str, args: &'a Args, i: usize, context: Context) -> Self {
        match args.get(i) {
            Some(FilterArg::Img(val)) => async move {
                let img = exec_filter(context, val).in_current_span().await?;
                Ok(Box::new(img.image()?))
            }
            .boxed(),
            Some(FilterArg::ResolvedImg(val)) => async move { Ok(Box::new(val.clone())) }.boxed(),
            Some(other) => {
                let name = name.to_owned();
                let other = other.clone();
                async move {
                    Err(InvalidArgumentType {
                        arg: other,
                        filter_name: name,
                        type_name: "image".to_owned(),
                    })?
                }
                .boxed()
            }
            None => {
                let name = name.to_owned();
                async move {
                    Err(MissingArgument {
                        arg_num: i + 1,
                        filter_name: name,
                    })?
                }
                .boxed()
            }
        }
    }
}

impl ArgTypeImg for img::CompositeOperator {}
impl ArgTypeImg for img::Colorspace {}
impl ArgTypeImg for img::ColorProfile {}
impl ArgTypeImg for img::CompressionType {}
impl ArgTypeImg for img::Filter {}
impl ArgTypeImg for img::ResolutionUnit {}
impl ArgTypeImg for img::ImageFormat {}
impl ArgTypeImg for img::AlphaChannel {}
impl ArgTypeImg for img::Gravity {}

#[macro_export]
macro_rules! arg_type {
    ($name:ident, $args:expr, $i:expr, Option(String)) => {{
        use $crate::filter;
        match <String as filter::ArgType>::arg_type(stringify!($name), &$args, $i) {
            Ok(v) => Some(v),
            Err(_) => None,
        }
    }};
    ($name:ident, $args:expr, $i:expr, String) => {{
        use $crate::filter;
        match <String as filter::ArgType>::arg_type(stringify!($name), &$args, $i) {
            Ok(v) => v,
            Err(e) => return Box::pin(async { Err(e) }),
        }
    }};
    ($name:ident, $args:expr, $i:expr, isize) => {{
        use $crate::filter;
        match <isize as filter::ArgType>::arg_type(stringify!($name), &$args, $i) {
            Ok(v) => v,
            Err(e) => return Box::pin(async { Err(e) }),
        }
    }};
    ($name:ident, $args:expr, $i:expr, $img:expr, isize) => {{
        use $crate::filter;
        match <isize as filter::ArgTypeImg>::arg_type_img(stringify!($name), "", &$args, $i, &$img)
        {
            Ok(v) => v,
            Err(e) => return Err(e),
        }
    }};
    ($name:ident, $args:expr, $i:expr, $img:expr, f32) => {{
        use $crate::filter;
        match <f32 as filter::ArgTypeImg>::arg_type_img(stringify!($name), "", &$args, $i, &$img) {
            Ok(v) => v,
            Err(e) => return Err(e),
        }
    }};
    ($name:ident, $args:expr, $i:expr, Filter) => {{
        use $crate::filter;
        match <Filter as filter::ArgType>::arg_type(stringify!($name), &$args, $i) {
            Ok(v) => v,
            Err(e) => return e,
        }
    }};
    ($name:ident, $args:expr, $i:expr, $context:expr, Image) => {{
        use $crate::filter;
        <filter::FutureImage as filter::ArgTypeContext>::arg_type_context(
            stringify!($name),
            &$args,
            $i,
            $context.clone(),
        )
    }};
    ($name:ident, $args:expr, $i:expr, $img:expr, $type:ty) => {{
        use $crate::filter;
        match <$type as filter::ArgTypeImg>::arg_type_img(
            stringify!($name),
            stringify!($type),
            &$args,
            $i,
            &$img,
        ) {
            Ok(v) => v,
            Err(e) => return Err(e),
        }
    }};
}

#[macro_export]
macro_rules! image_filter_args {
    // mut arg: Type
    ($name: ident, $args:ident, $img:ident, $i:expr, mut $arg_name:ident : $arg_type:tt) => {
        let mut $arg_name = arg_type!($name, $args, $i, $img, $arg_type);
    };
    // arg: Type
    ($name: ident, $args:ident, $img:ident, $i:expr, $arg_name:ident : $arg_type:tt) => {
        let $arg_name = arg_type!($name, $args, $i, $img, $arg_type);
    };
    // mut arg: Option<Type>
    ($name: ident, $args:ident, $img:ident, $i:expr, mut $arg_name:ident : Option<$arg_type:tt>) => {
        let mut $arg_name = if $args.len() > $i {
            Some(arg_type!($name, $args, $i, $img, $arg_type))
        } else { None };
    };
    // arg: Option<Type>
    ($name: ident, $args:ident, $img:ident, $i:expr, $arg_name:ident : Option<$arg_type:tt>) => {
        let $arg_name = if $args.len() > $i {
            Some(arg_type!($name, $args, $i, $img, $arg_type))
        } else { None };
    };
    // mut arg, args
    ($name: ident, $args:ident, $img:ident, $i:expr, mut $arg_name:ident : $arg_type:tt, $( $rest:tt )+) => {
        image_filter_args!($name, $args, $img, $i, mut $arg_name: $arg_type);
        image_filter_args!($name, $args, $img, $i + 1, $($rest)+);
    };
    // arg, args
    ($name: ident, $args:ident, $img:ident, $i:expr, $arg_name:ident : $arg_type:tt, $( $rest:tt )+) => {
        image_filter_args!($name, $args, $img, $i, $arg_name: $arg_type);
        image_filter_args!($name, $args, $img, $i + 1, $($rest)+);
    };
    // empty list of args
    ($name: ident, $args:ident, $img:ident, $i:expr, ) => {};
}

#[macro_export]
macro_rules! image_filter {
    ($name:ident ($img:ident : Image ) $body:block) => {
        image_filter!($name($img: Image, ) $body);
    };

    ($name:ident ($img:ident : Image, $ctx:ident : &Context, $( $args:tt )* ) $body:block) => {
        pub fn $name<'a>(mut $ctx: $crate::filter::Context, args: &'a $crate::filter::Args) -> $crate::filter::Future<'a> {
            let img = arg_type!($name, args, 0, $ctx, Image);

            #[allow(unused_variables)]
            let args = args.clone();
            #[allow(unused_mut)] // Because Image doesn't need to be mutable right now.
            Box::pin(async move {
                let mut $img = img.await?;
                image_filter_args!($name, args, $img, 1, $($args)*);
                $body
                Ok($img.into())
            }
            .in_current_span())
        }
    };

    ($name:ident ($img:ident : Image, $( $args:tt )* ) $body:block) => {
        image_filter!($name($img: Image, ctx: &Context, $($args)*) $body);
    }
}

#[macro_export]
macro_rules! boxed_async {
    ($($vis:vis async fn $name:ident ($($arg_name:ident : $arg_ty:ty),*) -> $result:ty $body:block)*) => {$(
        $vis fn $name<'a>($($arg_name: $arg_ty),*) -> BoxFuture<'a, $result> {
            async move {
                $body
            }.boxed()
        }
    )*}
}

fn inject_img(img: Box<img::Image>, filter: &mut Filter) {
    if !filter.args.is_empty() {
        if let FilterArg::Img(ref mut inner) = filter.args[0] {
            if inner.name != "__img__" {
                inject_img(img, inner);
                return;
            }
        } else {
            return;
        }
    }
    filter.args[0] = FilterArg::ResolvedImg(*img);
}

fn log_filter(context: &Context, filter: &Filter) -> Result<()> {
    let header_name = if let Some(ref name) = context.log_filters_header {
        name
    } else {
        return Ok(());
    };

    let mut route = String::new();
    let headers = &mut context.response_headers()?;
    if let Some(val) = headers.get(header_name) {
        route.push_str(val);
    }
    if !route.is_empty() {
        route.push(',')
    }
    route.push_str(filter.name.as_str());
    headers.insert(header_name.to_owned(), route);
    Ok(())
}

pub fn exec_from_partial_url<'a>(context: Context, img: Box<img::Image>, url: &str) -> Future<'a> {
    let subst_url = format!("__img__():{}", url);
    async move {
        let mut filter = url::parse(&subst_url)?;
        inject_img(img, &mut filter);
        exec_filter(context, &filter).in_current_span().await
    }
    .in_current_span()
    .boxed()
}

#[tracing::instrument(skip(context), fields(filter_name = %filter.name))]
pub fn exec_filter(context: Context, filter: &Filter) -> Future<'_> {
    async move {
        log_filter(&context, filter)?;

        let filter_runner = context
            .filters
            .get(filter.name.as_str())
            .ok_or_else(|| UnknownFilter(filter.name.clone()))?;

        let now = std::time::Instant::now();
        let result = filter_runner(context, &filter.args).await;
        let duration = now.elapsed().as_secs_f64();
        FILTER_DURATION
            .with_label_values(&[&filter.name])
            .observe(duration);

        result
    }
    .in_current_span()
    .boxed()
}
