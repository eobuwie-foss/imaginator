use std::path::PathBuf;

use thiserror::Error;

#[derive(Error, Debug)]
pub enum ReadCredentialError {
    #[error("CREDENTIALS_DIRECTORY is not set")]
    CredentialDirectoryNotSet,
    #[error("credential directory {0:?} does not exist")]
    CredentialDirectoryDoesNotExist(PathBuf),
    #[error("credential directory {0:?} is not a directory")]
    NotADirectory(PathBuf),
    #[error("failed to read credential from {0:?}: {1}")]
    ReadCredentials(PathBuf, std::io::Error),
    #[error("credential file does not exist: {0:?}")]
    CredentialDoesNotExist(PathBuf),
    #[error("credential file is empty: {0:?}")]
    CredentialEmpty(String),
}

pub trait ReadCredential {
    /// Tries to read credential from specified directory.
    ///
    /// This is intended to be used with:
    ///     - [systemd-creds](https://www.freedesktop.org/software/systemd/man/systemd-creds.html)
    ///     - [k8s secrets](https://kubernetes.io/docs/concepts/configuration/secret/) mounted as files
    ///
    /// # Errors
    /// Returns `Err` if:
    ///    - `CREDENTIALS_DIRECTORY` is not set
    ///    - credential file does not exist
    ///    - credential file is empty
    ///    - credential file cannot be read (e.g. permission denied)
    fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError>;
}

pub struct ReadCredentialDir;

impl ReadCredential for ReadCredentialDir {
    fn read_credential(&self, name: &str) -> Result<String, ReadCredentialError> {
        let credentials_dir = std::env::var("CREDENTIALS_DIRECTORY");

        let Ok(creds_dir) = credentials_dir.map(PathBuf::from) else {
            tracing::warn!(
                "CREDENTIALS_DIRECTORY is not set while looking for {} - not running under systemd or k8s with mounts?",
                name
            );
            return Err(ReadCredentialError::CredentialDirectoryNotSet);
        };

        if !creds_dir.exists() {
            return Err(ReadCredentialError::CredentialDirectoryDoesNotExist(
                creds_dir,
            ));
        }

        if !creds_dir.is_dir() {
            return Err(ReadCredentialError::NotADirectory(creds_dir));
        }

        let pass_file = creds_dir.join(name);
        if !pass_file.exists() {
            return Err(ReadCredentialError::CredentialDoesNotExist(pass_file));
        }

        let pass = std::fs::read_to_string(pass_file)
            .map_err(|e| ReadCredentialError::ReadCredentials(creds_dir.join(name), e))?
            .trim_end_matches('\n')
            .to_string();

        if pass.is_empty() {
            return Err(ReadCredentialError::CredentialEmpty(name.to_string()));
        };

        Ok(pass)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn all_env_tests() {
        credential_dir_does_not_exist();
        io_error_for_reading_file();
        password_missing();
        credential_directory_not_set();
        test_empty_pass();
        valid_password();
        credential_dir_is_a_file_not_a_dir();
        test_pass_with_new_line();
    }

    fn test_empty_pass() {
        let temp_dir = tempfile::tempdir().unwrap();
        let pass_file = temp_dir.path().join("pass");
        std::fs::write(pass_file, "").unwrap();
        std::env::set_var("CREDENTIALS_DIRECTORY", temp_dir.path());
        let pass = ReadCredentialDir.read_credential("pass");
        assert!(pass.is_err())
    }

    fn test_pass_with_new_line() {
        let temp_dir = tempfile::tempdir().unwrap();
        let pass_file = temp_dir.path().join("pass");
        let pass = "password\n";
        std::fs::write(pass_file, pass).unwrap();
        std::env::set_var("CREDENTIALS_DIRECTORY", temp_dir.path());
        let pass = ReadCredentialDir.read_credential("pass");
        assert_eq!(pass.unwrap(), "password")
    }

    fn valid_password() {
        let temp_dir = tempfile::tempdir().unwrap();
        let pass_file = temp_dir.path().join("pass");
        std::fs::write(pass_file, "password").unwrap();
        std::env::set_var("CREDENTIALS_DIRECTORY", temp_dir.path());
        let pass = ReadCredentialDir.read_credential("pass");
        assert_eq!(pass.unwrap(), "password")
    }

    fn credential_dir_does_not_exist() {
        let temp_dir = tempfile::tempdir().unwrap();
        std::env::set_var(
            "CREDENTIALS_DIRECTORY",
            temp_dir.path().join("does-not-exist"),
        );
        let pass = ReadCredentialDir.read_credential("pass");
        assert!(pass.is_err());
        match pass.unwrap_err() {
            ReadCredentialError::CredentialDirectoryDoesNotExist(path) => {
                assert_eq!(path, temp_dir.path().join("does-not-exist"));
            }
            e => panic!("unexpected error: {}", e),
        }
    }

    fn io_error_for_reading_file() {
        let temp_dir = tempfile::tempdir().unwrap();
        let pass_file = temp_dir.path().join("pass");
        let invalid_utf8 = [0x80];
        std::fs::write(pass_file, invalid_utf8).unwrap();
        std::env::set_var("CREDENTIALS_DIRECTORY", temp_dir.path());
        let pass = ReadCredentialDir.read_credential("pass");
        assert!(pass.is_err());
        match pass.unwrap_err() {
            ReadCredentialError::ReadCredentials(path, io) => {
                assert_eq!(path, temp_dir.path().join("pass"));
                assert_eq!(io.kind(), std::io::ErrorKind::InvalidData);
            }
            _ => panic!("unexpected error"),
        }
    }

    fn password_missing() {
        let temp_dir = tempfile::tempdir().unwrap();
        std::env::set_var("CREDENTIALS_DIRECTORY", temp_dir.path());
        let pass = ReadCredentialDir.read_credential("pass");
        assert!(pass.is_err());
        match pass.unwrap_err() {
            ReadCredentialError::CredentialDoesNotExist(path) => {
                assert_eq!(path, temp_dir.path().join("pass"));
            }
            _ => panic!("unexpected error"),
        }
    }

    fn credential_directory_not_set() {
        std::env::remove_var("CREDENTIALS_DIRECTORY");
        let pass = ReadCredentialDir.read_credential("pass");
        assert!(pass.is_err());
        match pass.unwrap_err() {
            ReadCredentialError::CredentialDirectoryNotSet => {}
            _ => panic!("unexpected error"),
        }
    }

    fn credential_dir_is_a_file_not_a_dir() {
        let temp_file = tempfile::NamedTempFile::new().unwrap();
        std::env::set_var("CREDENTIALS_DIRECTORY", temp_file.path());
        let pass = ReadCredentialDir.read_credential("pass");
        assert!(pass.is_err());
        match pass.unwrap_err() {
            ReadCredentialError::NotADirectory(path) => {
                assert_eq!(path, temp_file.path());
            }
            _ => panic!("unexpected error"),
        }
    }
}
