use std::sync::{Arc, Mutex, RwLock};

use futures::future::BoxFuture;

pub mod cfg;
pub mod filter;
pub mod img;
pub mod metrics;
pub mod prelude;
pub mod systemd_creds;
pub mod url;

pub use filter::UnknownFilter;
pub use img::UnknownImageFormat;
pub use prelude::{Request, Response};
use tokio::task::block_in_place;

#[derive(Debug, Clone, Copy, thiserror::Error)]
#[cfg_attr(test, derive(PartialEq))]
#[error("No CPU cores currently available")]
pub struct CpuNotAvailable;

impl CensoredError for CpuNotAvailable {
    fn censored_error(&self) -> &'static str {
        "Too many requests"
    }
}

/// This structure inner state is created once per http request and shared between all instances
/// It is responsible for ensuring number of long operation is equal or lower to configured number
///
/// For example:
/// * If there's no limit this structure does nothing
/// * If limit is set to 3 and there are 3 pending long task and new request with long task should be started locker will return Error
/// * If limit is set and not reached it will return Ok
///
/// Cloning this structure does not allow to increase used cpu counter since internals are shared.
///
/// Example flow:
///
/// Server received `cache(trim(crop(resize(download(..)))))`
///
/// Those imaginator filters will be called:
///
/// * download
/// * resize
/// * crop
/// * trim
/// * cache (blocking write to cache)
///
/// When resize call [HardwareLocke::request_blocking] `bool` flag will be set to prevent changing `used_cpu` by `crop`, `trim` and `cache`. Then `used_cpu` will be increased.
/// At the end of the request HardwareLocker is dropped and `used_cpu` will be decreased if
/// `request_blocking` was used.
///
/// # Example
///
/// ```rust
/// # use imaginator_common::*;
/// # use std::sync::{Mutex,Arc};
/// # #[derive(Debug,thiserror::Error)] enum E { #[error(transparent)] C(#[from] imaginator_common::CpuNotAvailable) }
/// # type R = Result<(), E>;
///
/// fn run() {
///     let used = Arc::new(Mutex::new(0));
///     call_filters(Some(5), used, vec![Box::new(crop), Box::new(trim), Box::new(resize)]);
/// }
///
/// fn call_filters(
///     available: Option<usize>,
///     used: Arc<Mutex<usize>>,
///     filters: Vec<Box<dyn Fn(HardwareLocker) -> anyhow::Result<()>>>,
/// ) -> anyhow::Result<()> {
///     let locker = HardwareLocker::new(available, used);
///     for filter in filters {
///         filter(locker.clone());
///     }
///     Ok(())
/// } // drop locker and decrease used_cpu
///
/// fn crop(mut locker: HardwareLocker) -> anyhow::Result<()> {
///     locker.run_blocking(|| {
///         // cpu expensive work for example using ImageMagic wand to crop large image
///         Ok(()) as R
///     })?; // set locked to true and increase used_cpu
///     Ok(())
/// }
///
/// fn trim(mut locker: HardwareLocker) -> anyhow::Result<()> {
///     locker.run_blocking(|| {
///         // cpu expensive work for example using ImageMagic wand to trim large image
///         Ok(()) as R
///     })?; // Just return Ok(())
///     Ok(())
/// }
///
/// fn resize(mut locker: HardwareLocker) -> anyhow::Result<()> {
///     locker.run_blocking(|| {
///         // cpu expensive work for example using ImageMagic wand to resize large image
///         Ok(()) as R}
///     )?; // Just return Ok(())
///     Ok(())
/// }
/// ```
#[derive(Debug, Clone)]
pub struct HardwareLocker {
    /// Inner state which keeps track how many long heavy requests is running
    inner: Arc<HardwareLockerInner>,
    /// Configured information how many heavy requests can be handled in total
    available_cpu: Option<usize>,
}

#[derive(Debug)]
pub struct HardwareLockerInner {
    /// Prevents increase of `cpu_used` more than once
    locked: Arc<RwLock<bool>>,
    /// How many heavy requests is currently running
    used_cpu: Arc<Mutex<usize>>,
}

impl Drop for HardwareLockerInner {
    fn drop(&mut self) {
        if !*self.locked.read().unwrap() {
            return;
        }
        let mut n = self.used_cpu.lock().unwrap();
        (*n) = n.checked_sub(1).unwrap_or(*n);
    }
}

impl HardwareLocker {
    /// Creates instance of locker with shared inner state
    pub fn new(available_cpu: Option<usize>, used_cpu: Arc<Mutex<usize>>) -> Self {
        Self {
            available_cpu,
            inner: Arc::new(HardwareLockerInner {
                used_cpu,
                locked: Arc::new(RwLock::new(false)),
            }),
        }
    }

    /// Attempt to mark request it's expensive for async engine
    /// If request was already marked as expensive it'll just return Ok otherwise
    /// it will check if there's limit and if limit was reached. In this situation it'll return Err
    /// which should be handled by HTTP server. If limit wasn't reached then http request will be
    /// marked as expensive and used cpu counter will be increased. If there's no limit this
    /// function will just return Ok
    pub fn run_blocking<F, T, E>(&self, f: F) -> Result<T, E>
    where
        F: FnMut() -> Result<T, E>,
        E: From<CpuNotAvailable>,
    {
        self.request_blocking()?;
        block_in_place(f)
    }

    /// Attempt to mark request it's expensive for async engine
    fn request_blocking(&self) -> Result<(), CpuNotAvailable> {
        let max = match (*self.inner.locked.read().unwrap(), self.available_cpu) {
            (true, _) | (_, None) => return Ok(()),
            (_, Some(max)) => max,
        };
        {
            let mut used = self.inner.used_cpu.lock().unwrap();
            if *used == max {
                return Err(CpuNotAvailable);
            }
            *used += 1;
        }
        *self.inner.locked.write().unwrap() = true;
        Ok(())
    }
}

pub struct ImaginatorExtension {
    pub version: &'static str,
}

impl ImaginatorExtension {
    pub fn full_version(&self) -> &'static str {
        self.version
    }

    pub fn major(&self) -> &'static str {
        self.version.split_once('.').unwrap().0
    }
}

#[allow(clippy::type_complexity)]
pub struct PluginInformation {
    pub filters: filter::FilterMap,
    pub init: Option<&'static (dyn Fn() -> Result<(), anyhow::Error> + Sync)>,
    pub exit: Option<&'static (dyn Fn() -> Result<(), anyhow::Error> + Sync)>,
    pub middleware:
        Option<&'static (dyn for<'a> Fn(&'a Request) -> BoxFuture<'a, MiddlewareResult> + Sync)>,
    pub post_init: Option<&'static (dyn Fn() + Sync)>,
}

type MiddlewareResult = Result<Option<Response>, anyhow::Error>;

impl PluginInformation {
    pub fn new(filters: filter::FilterMap) -> Self {
        PluginInformation {
            filters,
            init: None,
            exit: None,
            middleware: None,
            post_init: None,
        }
    }

    pub fn with_init(
        mut self,
        init: &'static (dyn Fn() -> Result<(), anyhow::Error> + Sync),
    ) -> Self {
        self.init = Some(init);
        self
    }

    pub fn with_post_init(mut self, initialized: &'static (dyn Fn() + Sync)) -> Self {
        self.post_init = Some(initialized);
        self
    }

    pub fn with_exit(
        mut self,
        exit: &'static (dyn Fn() -> Result<(), anyhow::Error> + Sync),
    ) -> Self {
        self.exit = Some(exit);
        self
    }

    pub fn with_middleware(
        mut self,
        middleware: &'static (dyn for<'a> Fn(&'a Request) -> BoxFuture<'a, MiddlewareResult>
                      + Sync),
    ) -> Self {
        self.middleware = Some(middleware);
        self
    }
}

/// Error message that can be displayed to the user
///
/// This makes sure that we don't leak any sensitive information,
/// while preserving original error's message, which can be logged.
pub trait CensoredError: Send + Sync + std::error::Error {
    fn censored_error(&self) -> &'static str {
        std::any::type_name::<Self>()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn disabled() {
        let available = Arc::new(Mutex::new(0));

        let hl = HardwareLocker::new(None, available);
        let r1 = hl.request_blocking();
        let r2 = hl.request_blocking();
        let r3 = hl.request_blocking();

        assert_eq!(r1, Ok(()));
        assert_eq!(r2, Ok(()));
        assert_eq!(r3, Ok(()));
    }

    #[test]
    fn too_many_requests() {
        let available = Arc::new(Mutex::new(0));

        let hl1 = HardwareLocker::new(Some(1), available.clone());
        let r1 = hl1.request_blocking();
        let r2 = hl1.request_blocking();
        let r3 = hl1.request_blocking();

        assert_eq!(r1, Ok(()));
        assert_eq!(r2, Ok(()));
        assert_eq!(r3, Ok(()));

        let hl2 = HardwareLocker::new(Some(1), available);
        let r1 = hl2.request_blocking();
        let r2 = hl2.request_blocking();
        let r3 = hl2.request_blocking();

        assert_eq!(r1, Err(CpuNotAvailable));
        assert_eq!(r2, Err(CpuNotAvailable));
        assert_eq!(r3, Err(CpuNotAvailable));
    }

    #[test]
    fn clone_locker() {
        let available = Arc::new(Mutex::new(0));

        let hl1 = HardwareLocker::new(Some(1), available.clone());
        let r1 = hl1.request_blocking();
        let hl1c = hl1.clone();
        let _r1 = hl1c.request_blocking();
        let _r2 = hl1c.request_blocking();
        let _r3 = hl1c.request_blocking();
        let r2 = hl1.request_blocking();
        let r3 = hl1.request_blocking();

        assert_eq!(r1, Ok(()));
        assert_eq!(r2, Ok(()));
        assert_eq!(r3, Ok(()));

        let hl2 = HardwareLocker::new(Some(1), available);
        let r1 = hl2.request_blocking();
        let r2 = hl2.request_blocking();
        let r3 = hl2.request_blocking();

        assert_eq!(r1, Err(CpuNotAvailable));
        assert_eq!(r2, Err(CpuNotAvailable));
        assert_eq!(r3, Err(CpuNotAvailable));
    }

    #[test]
    fn many_valid_requests() {
        let available = Arc::new(Mutex::new(0));

        let _ = (0..10)
            .map(|_| {
                let hl = HardwareLocker::new(Some(20), available.clone());
                let r1 = hl.request_blocking();
                let r2 = hl.request_blocking();
                let r3 = hl.request_blocking();

                assert_eq!(r1, Ok(()));
                assert_eq!(r2, Ok(()));
                assert_eq!(r3, Ok(()));
                hl
            })
            .collect::<Vec<_>>();
    }

    #[test]
    fn valid_with_release() {
        let available = Arc::new(Mutex::new(0));

        let _hl1 = {
            let hl = HardwareLocker::new(Some(2), available.clone());
            let r1 = hl.request_blocking();
            let r2 = hl.request_blocking();
            let r3 = hl.request_blocking();

            assert_eq!(r1, Ok(()));
            assert_eq!(r2, Ok(()));
            assert_eq!(r3, Ok(()));
            hl
        };
        let hl2 = {
            let hl = HardwareLocker::new(Some(2), available.clone());
            let r1 = hl.request_blocking();
            let r2 = hl.request_blocking();
            let r3 = hl.request_blocking();

            assert_eq!(r1, Ok(()));
            assert_eq!(r2, Ok(()));
            assert_eq!(r3, Ok(()));
            hl
        };
        {
            let hl = HardwareLocker::new(Some(2), available.clone());
            let r1 = hl.request_blocking();
            assert_eq!(r1, Err(CpuNotAvailable));
        }
        drop(hl2);
        {
            let hl = HardwareLocker::new(Some(2), available.clone());
            let r1 = hl.request_blocking();
            assert_eq!(r1, Ok(()));
        }
        let _hl3 = {
            let hl = HardwareLocker::new(Some(2), available.clone());
            let r1 = hl.request_blocking();
            let r2 = hl.request_blocking();
            let r3 = hl.request_blocking();

            assert_eq!(r1, Ok(()));
            assert_eq!(r2, Ok(()));
            assert_eq!(r3, Ok(()));
            hl
        };
        {
            let hl = HardwareLocker::new(Some(2), available.clone());
            let r1 = hl.request_blocking();
            assert_eq!(r1, Err(CpuNotAvailable));
        }
    }

    #[test]
    fn problematic_test() {
        let available = Arc::new(Mutex::new(0));

        let base = HardwareLocker::new(Some(10), available.clone());
        let _lockers = (0..4000)
            .map(|_| {
                let hl = base.clone();
                let rsp = hl.request_blocking();
                // this should fail at some point
                assert_eq!(rsp, Ok(()));
                hl
            })
            .collect::<Vec<_>>();

        let rsp = base.request_blocking();
        assert_eq!(rsp, Ok(()));
    }

    #[test]
    fn check_drop() {
        let used = Arc::new(Mutex::new(10));
        let hl = HardwareLocker::new(Some(20), used.clone());
        hl.request_blocking().unwrap();

        assert_eq!(*used.lock().unwrap(), 11);

        let lock1 = hl.clone();
        let lock2 = hl.clone();
        let lock3 = hl.clone();

        drop(lock1);
        assert_eq!(*used.lock().unwrap(), 11);

        drop(lock2);
        assert_eq!(*used.lock().unwrap(), 11);

        drop(lock3);
        assert_eq!(*used.lock().unwrap(), 11);

        drop(hl);
        assert_eq!(*used.lock().unwrap(), 10);
    }
}
