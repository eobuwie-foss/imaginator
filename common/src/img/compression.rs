use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unknown compression type: {0}")]
pub struct UnknownCompression(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum CompressionType {
    Undefined,
    B44A,
    B44,
    BZip,
    DXT1,
    DXT3,
    DXT5,
    Fax,
    Group4,
    JBIG1,
    JBIG2,
    JPEG2000,
    JPEG,
    LosslessJPEG,
    LZMA,
    LZW,
    No,
    Piz,
    Pxr24,
    RLE,
    Zip,
    ZipS,
    Zstd,
    WebP,
    DWAA,
    DWAB,
    BC7,
    BC5,
    LERC,
}

impl FromStr for CompressionType {
    type Err = UnknownCompression;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => CompressionType::Undefined,
            "b44a" => CompressionType::B44A,
            "b44" => CompressionType::B44,
            "bzip" => CompressionType::BZip,
            "dxt1" => CompressionType::DXT1,
            "dxt3" => CompressionType::DXT3,
            "dxt5" => CompressionType::DXT5,
            "fax" => CompressionType::Fax,
            "group4" => CompressionType::Group4,
            "jbig1" => CompressionType::JBIG1,
            "jbig2" => CompressionType::JBIG2,
            "jpeg2000" => CompressionType::JPEG2000,
            "jpeg" => CompressionType::JPEG,
            "losslessjpeg" => CompressionType::LosslessJPEG,
            "lzma" => CompressionType::LZMA,
            "lzw" => CompressionType::LZW,
            "no" => CompressionType::No,
            "piz" => CompressionType::Piz,
            "pxr24" => CompressionType::Pxr24,
            "rle" => CompressionType::RLE,
            "zip" => CompressionType::Zip,
            "zips" => CompressionType::ZipS,
            "zstd" => CompressionType::Zstd,
            "webp" => CompressionType::WebP,
            "dwaa" => CompressionType::DWAA,
            "dwab" => CompressionType::DWAB,
            "bc7" => CompressionType::BC7,
            "bc5" => CompressionType::BC5,
            "lerc" => CompressionType::LERC,
            _ => return Err(UnknownCompression(input.to_owned())),
        })
    }
}

impl From<CompressionType> for magick_rust::CompressionType {
    fn from(from: CompressionType) -> magick_rust::CompressionType {
        match from {
            CompressionType::Undefined => magick_rust::CompressionType::Undefined,
            CompressionType::B44A => magick_rust::CompressionType::B44A,
            CompressionType::B44 => magick_rust::CompressionType::B44,
            CompressionType::BZip => magick_rust::CompressionType::BZip,
            CompressionType::DXT1 => magick_rust::CompressionType::DXT1,
            CompressionType::DXT3 => magick_rust::CompressionType::DXT3,
            CompressionType::DXT5 => magick_rust::CompressionType::DXT5,
            CompressionType::Fax => magick_rust::CompressionType::Fax,
            CompressionType::Group4 => magick_rust::CompressionType::Group4,
            CompressionType::JBIG1 => magick_rust::CompressionType::JBIG1,
            CompressionType::JBIG2 => magick_rust::CompressionType::JBIG2,
            CompressionType::JPEG2000 => magick_rust::CompressionType::JPEG2000,
            CompressionType::JPEG => magick_rust::CompressionType::JPEG,
            CompressionType::LosslessJPEG => magick_rust::CompressionType::LosslessJPEG,
            CompressionType::LZMA => magick_rust::CompressionType::LZMA,
            CompressionType::LZW => magick_rust::CompressionType::LZW,
            CompressionType::No => magick_rust::CompressionType::No,
            CompressionType::Piz => magick_rust::CompressionType::Piz,
            CompressionType::Pxr24 => magick_rust::CompressionType::Pxr24,
            CompressionType::RLE => magick_rust::CompressionType::RLE,
            CompressionType::Zip => magick_rust::CompressionType::Zip,
            CompressionType::ZipS => magick_rust::CompressionType::ZipS,
            CompressionType::Zstd => magick_rust::CompressionType::Zstd,
            CompressionType::WebP => magick_rust::CompressionType::WebP,
            CompressionType::DWAA => magick_rust::CompressionType::DWAA,
            CompressionType::DWAB => magick_rust::CompressionType::DWAB,
            CompressionType::BC7 => magick_rust::CompressionType::BC7,
            CompressionType::BC5 => magick_rust::CompressionType::BC5,
            CompressionType::LERC => magick_rust::CompressionType::LERC,
        }
    }
}

impl From<magick_rust::CompressionType> for CompressionType {
    fn from(from: magick_rust::CompressionType) -> Self {
        match from {
            magick_rust::CompressionType::Undefined => CompressionType::Undefined,
            magick_rust::CompressionType::B44A => CompressionType::B44A,
            magick_rust::CompressionType::B44 => CompressionType::B44,
            magick_rust::CompressionType::BZip => CompressionType::BZip,
            magick_rust::CompressionType::DXT1 => CompressionType::DXT1,
            magick_rust::CompressionType::DXT3 => CompressionType::DXT3,
            magick_rust::CompressionType::DXT5 => CompressionType::DXT5,
            magick_rust::CompressionType::Fax => CompressionType::Fax,
            magick_rust::CompressionType::Group4 => CompressionType::Group4,
            magick_rust::CompressionType::JBIG1 => CompressionType::JBIG1,
            magick_rust::CompressionType::JBIG2 => CompressionType::JBIG2,
            magick_rust::CompressionType::JPEG2000 => CompressionType::JPEG2000,
            magick_rust::CompressionType::JPEG => CompressionType::JPEG,
            magick_rust::CompressionType::LosslessJPEG => CompressionType::LosslessJPEG,
            magick_rust::CompressionType::LZMA => CompressionType::LZMA,
            magick_rust::CompressionType::LZW => CompressionType::LZW,
            magick_rust::CompressionType::No => CompressionType::No,
            magick_rust::CompressionType::Piz => CompressionType::Piz,
            magick_rust::CompressionType::Pxr24 => CompressionType::Pxr24,
            magick_rust::CompressionType::RLE => CompressionType::RLE,
            magick_rust::CompressionType::Zip => CompressionType::Zip,
            magick_rust::CompressionType::ZipS => CompressionType::ZipS,
            magick_rust::CompressionType::Zstd => CompressionType::Zstd,
            magick_rust::CompressionType::WebP => CompressionType::WebP,
            magick_rust::CompressionType::DWAA => CompressionType::DWAA,
            magick_rust::CompressionType::DWAB => CompressionType::DWAB,
            magick_rust::CompressionType::BC7 => CompressionType::BC7,
            magick_rust::CompressionType::BC5 => CompressionType::BC5,
            magick_rust::CompressionType::LERC => CompressionType::LERC,
        }
    }
}
