use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unknown resize filter: {0}")]
pub struct UnknownFilter(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq, Default)]
pub enum Filter {
    Undefined,
    Point,
    Box,
    Triangle,
    Hermite,
    Hann,
    Hamming,
    Blackman,
    Gaussian,
    Quadratic,
    Cubic,
    Catrom,
    Mitchell,
    Jinc,
    Sinc,
    SincFast,
    Kaiser,
    Welch,
    Parzen,
    Bohman,
    Bartlett,
    Lagrange,
    #[default]
    Lanczos,
    LanczosSharp,
    Lanczos2,
    Lanczos2Sharp,
    Robidoux,
    RobidouxSharp,
    Cosine,
    Spline,
    LanczosRadius,
    CubicSpline,
    Sentinel,
}

impl FromStr for Filter {
    type Err = UnknownFilter;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => Filter::Undefined,
            "point" => Filter::Point,
            "box" => Filter::Box,
            "triangle" => Filter::Triangle,
            "hermite" => Filter::Hermite,
            "hann" => Filter::Hann,
            "hamming" => Filter::Hamming,
            "blackman" => Filter::Blackman,
            "gaussian" => Filter::Gaussian,
            "quadratic" => Filter::Quadratic,
            "cubic" => Filter::Cubic,
            "catrom" => Filter::Catrom,
            "mitchell" => Filter::Mitchell,
            "jinc" => Filter::Jinc,
            "sinc" => Filter::Sinc,
            "sinc_fast" => Filter::SincFast,
            "kaiser" => Filter::Kaiser,
            "welch" => Filter::Welch,
            "parzen" => Filter::Parzen,
            "bohman" => Filter::Bohman,
            "bartlett" => Filter::Bartlett,
            "lagrange" => Filter::Lagrange,
            "lanczos" => Filter::Lanczos,
            "lanczos_sharp" => Filter::LanczosSharp,
            "lanczos2" => Filter::Lanczos2,
            "lanczos2_sharp" => Filter::Lanczos2Sharp,
            "robidoux" => Filter::Robidoux,
            "robidoux_sharp" => Filter::RobidouxSharp,
            "cosine" => Filter::Cosine,
            "spline" => Filter::Spline,
            "lanczos_radius" => Filter::LanczosRadius,
            "cubic_spline" => Filter::CubicSpline,
            "sentinel" => Filter::Sentinel,
            _ => return Err(UnknownFilter(input.to_owned())),
        })
    }
}

impl<'a> From<&'a Filter> for magick_rust::FilterType {
    fn from(from: &'a Filter) -> magick_rust::FilterType {
        match *from {
            Filter::Undefined => magick_rust::FilterType::Undefined,
            Filter::Point => magick_rust::FilterType::Point,
            Filter::Box => magick_rust::FilterType::Box,
            Filter::Triangle => magick_rust::FilterType::Triangle,
            Filter::Hermite => magick_rust::FilterType::Hermite,
            Filter::Hann => magick_rust::FilterType::Hann,
            Filter::Hamming => magick_rust::FilterType::Hamming,
            Filter::Blackman => magick_rust::FilterType::Blackman,
            Filter::Gaussian => magick_rust::FilterType::Gaussian,
            Filter::Quadratic => magick_rust::FilterType::Quadratic,
            Filter::Cubic => magick_rust::FilterType::Cubic,
            Filter::Catrom => magick_rust::FilterType::Catrom,
            Filter::Mitchell => magick_rust::FilterType::Mitchell,
            Filter::Jinc => magick_rust::FilterType::Jinc,
            Filter::Sinc => magick_rust::FilterType::Sinc,
            Filter::SincFast => magick_rust::FilterType::SincFast,
            Filter::Kaiser => magick_rust::FilterType::Kaiser,
            Filter::Welch => magick_rust::FilterType::Welch,
            Filter::Parzen => magick_rust::FilterType::Parzen,
            Filter::Bohman => magick_rust::FilterType::Bohman,
            Filter::Bartlett => magick_rust::FilterType::Bartlett,
            Filter::Lagrange => magick_rust::FilterType::Lagrange,
            Filter::Lanczos => magick_rust::FilterType::Lanczos,
            Filter::LanczosSharp => magick_rust::FilterType::LanczosSharp,
            Filter::Lanczos2 => magick_rust::FilterType::Lanczos2,
            Filter::Lanczos2Sharp => magick_rust::FilterType::Lanczos2Sharp,
            Filter::Robidoux => magick_rust::FilterType::Robidoux,
            Filter::RobidouxSharp => magick_rust::FilterType::RobidouxSharp,
            Filter::Cosine => magick_rust::FilterType::Cosine,
            Filter::Spline => magick_rust::FilterType::Spline,
            Filter::LanczosRadius => magick_rust::FilterType::LanczosRadius,
            Filter::CubicSpline => magick_rust::FilterType::CubicSpline,
            Filter::Sentinel => magick_rust::FilterType::Sentinel,
        }
    }
}

impl From<magick_rust::FilterType> for Filter {
    fn from(from: magick_rust::FilterType) -> Self {
        match from {
            magick_rust::FilterType::Undefined => Filter::Undefined,
            magick_rust::FilterType::Point => Filter::Point,
            magick_rust::FilterType::Box => Filter::Box,
            magick_rust::FilterType::Triangle => Filter::Triangle,
            magick_rust::FilterType::Hermite => Filter::Hermite,
            magick_rust::FilterType::Hann => Filter::Hann,
            magick_rust::FilterType::Hamming => Filter::Hamming,
            magick_rust::FilterType::Blackman => Filter::Blackman,
            magick_rust::FilterType::Gaussian => Filter::Gaussian,
            magick_rust::FilterType::Quadratic => Filter::Quadratic,
            magick_rust::FilterType::Cubic => Filter::Cubic,
            magick_rust::FilterType::Catrom => Filter::Catrom,
            magick_rust::FilterType::Mitchell => Filter::Mitchell,
            magick_rust::FilterType::Jinc => Filter::Jinc,
            magick_rust::FilterType::Sinc => Filter::Sinc,
            magick_rust::FilterType::SincFast => Filter::SincFast,
            magick_rust::FilterType::Kaiser => Filter::Kaiser,
            magick_rust::FilterType::Welch => Filter::Welch,
            magick_rust::FilterType::Parzen => Filter::Parzen,
            magick_rust::FilterType::Bohman => Filter::Bohman,
            magick_rust::FilterType::Bartlett => Filter::Bartlett,
            magick_rust::FilterType::Lagrange => Filter::Lagrange,
            magick_rust::FilterType::Lanczos => Filter::Lanczos,
            magick_rust::FilterType::LanczosSharp => Filter::LanczosSharp,
            magick_rust::FilterType::Lanczos2 => Filter::Lanczos2,
            magick_rust::FilterType::Lanczos2Sharp => Filter::Lanczos2Sharp,
            magick_rust::FilterType::Robidoux => Filter::Robidoux,
            magick_rust::FilterType::RobidouxSharp => Filter::RobidouxSharp,
            magick_rust::FilterType::Cosine => Filter::Cosine,
            magick_rust::FilterType::Spline => Filter::Spline,
            magick_rust::FilterType::LanczosRadius => Filter::LanczosRadius,
            magick_rust::FilterType::CubicSpline => Filter::CubicSpline,
            magick_rust::FilterType::Sentinel => Filter::Sentinel,
        }
    }
}
