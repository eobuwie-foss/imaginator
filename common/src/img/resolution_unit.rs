use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unknown resolution unit: {0}")]
pub struct UnknownResolutionUnit(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum ResolutionUnit {
    Undefined,
    PixelsPerInch,
    PixelsPerCentimeter,
}

impl FromStr for ResolutionUnit {
    type Err = UnknownResolutionUnit;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => ResolutionUnit::Undefined,
            "dpi" => ResolutionUnit::PixelsPerInch,
            "dpc" => ResolutionUnit::PixelsPerCentimeter,
            _ => return Err(UnknownResolutionUnit(input.to_owned())),
        })
    }
}

impl From<ResolutionUnit> for magick_rust::ResolutionType {
    fn from(from: ResolutionUnit) -> magick_rust::ResolutionType {
        match from {
            ResolutionUnit::Undefined => magick_rust::ResolutionType::Undefined,
            ResolutionUnit::PixelsPerInch => magick_rust::ResolutionType::PixelsPerInch,
            ResolutionUnit::PixelsPerCentimeter => magick_rust::ResolutionType::PixelsPerCentimeter,
        }
    }
}

impl From<magick_rust::ResolutionType> for ResolutionUnit {
    fn from(from: magick_rust::ResolutionType) -> Self {
        match from {
            magick_rust::ResolutionType::Undefined => ResolutionUnit::Undefined,
            magick_rust::ResolutionType::PixelsPerInch => ResolutionUnit::PixelsPerInch,
            magick_rust::ResolutionType::PixelsPerCentimeter => ResolutionUnit::PixelsPerCentimeter,
        }
    }
}
