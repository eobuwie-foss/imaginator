use crate::CensoredError;
use serde_derive::{Deserialize, Serialize};
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unsupported image {url:?} format: {format}")]
pub struct UnknownImageFormat {
    pub format: String,
    pub url: String,
}

impl UnknownImageFormat {
    pub fn new(format: String, url: String) -> Self {
        Self { format, url }
    }
}

impl CensoredError for UnknownImageFormat {
    fn censored_error(&self) -> &'static str {
        "Unknown Image Format"
    }
}

#[derive(PartialEq, Eq, Clone, Debug, Serialize, Deserialize)]
pub enum ImageFormat {
    Undefined,
    PNG,
    JPEG,
    TIFF,
    PDF,
    PS,
    SVG,
    WEBP,
    JP2,
}

impl ImageFormat {
    pub fn magick_str(&self) -> &str {
        match *self {
            ImageFormat::PNG => "PNG",
            ImageFormat::JPEG => "JPEG",
            ImageFormat::TIFF => "TIF",
            ImageFormat::PDF => "PDF",
            ImageFormat::PS => "PS",
            ImageFormat::SVG => "SVG",
            ImageFormat::WEBP => "WEBP",
            ImageFormat::JP2 => "JP2",
            ImageFormat::Undefined => "",
        }
    }
}

impl FromStr for ImageFormat {
    type Err = UnknownImageFormat;
    fn from_str(src: &str) -> Result<Self, Self::Err> {
        Ok(match src {
            "PNG" | "png" => ImageFormat::PNG,
            "JPEG" | "JPG" | "jpeg" | "jpg" => ImageFormat::JPEG,
            "TIFF" | "TIF" | "tiff" | "tif" => ImageFormat::TIFF,
            "PDF" | "pdf" => ImageFormat::PDF,
            "PS" | "ps" => ImageFormat::PS,
            "SVG" | "svg" => ImageFormat::SVG,
            "WEBP" | "webp" => ImageFormat::WEBP,
            "JP2" => ImageFormat::JP2,
            _ => return Err(UnknownImageFormat::new(src.to_owned(), src.to_owned())),
        })
    }
}

impl<'a> From<&'a ImageFormat> for String {
    fn from(src: &'a ImageFormat) -> Self {
        match *src {
            ImageFormat::JP2 => "image/jp2",
            ImageFormat::PNG => "image/png",
            ImageFormat::JPEG => "image/jpeg",
            ImageFormat::TIFF => "image/tiff",
            ImageFormat::PDF => "application/pdf",
            ImageFormat::PS => "application/postscript",
            ImageFormat::SVG => "image/svg+xml",
            ImageFormat::WEBP => "image/webp",
            ImageFormat::Undefined => "text/plain",
        }
        .to_owned()
    }
}

impl From<ImageFormat> for String {
    fn from(src: ImageFormat) -> Self {
        From::from(&src)
    }
}
