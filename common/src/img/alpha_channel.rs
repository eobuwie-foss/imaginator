use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unknown alpha channel option: {0}")]
pub struct UnknownAlphaChannelOption(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum AlphaChannel {
    Undefined,
    Activate,
    Associate,
    Background,
    Copy,
    Deactivate,
    Discrete,
    Disassociate,
    Extract,
    Off,
    On,
    Opaque,
    Remove,
    Set,
    Shape,
    Transparent,
}

impl FromStr for AlphaChannel {
    type Err = UnknownAlphaChannelOption;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => AlphaChannel::Undefined,
            "activate" => AlphaChannel::Activate,
            "associate" => AlphaChannel::Associate,
            "background" => AlphaChannel::Background,
            "copy" => AlphaChannel::Copy,
            "deactivate" => AlphaChannel::Deactivate,
            "discrete" => AlphaChannel::Discrete,
            "disassociate" => AlphaChannel::Disassociate,
            "extract" => AlphaChannel::Extract,
            "off" => AlphaChannel::Off,
            "on" => AlphaChannel::On,
            "opaque" => AlphaChannel::Opaque,
            "remove" => AlphaChannel::Remove,
            "set" => AlphaChannel::Set,
            "shape" => AlphaChannel::Shape,
            "transparent" => AlphaChannel::Transparent,
            _ => return Err(UnknownAlphaChannelOption(input.to_owned())),
        })
    }
}

impl From<AlphaChannel> for magick_rust::AlphaChannelOption {
    fn from(from: AlphaChannel) -> magick_rust::AlphaChannelOption {
        match from {
            AlphaChannel::Undefined => magick_rust::AlphaChannelOption::Undefined,
            AlphaChannel::Activate => magick_rust::AlphaChannelOption::Activate,
            AlphaChannel::Associate => magick_rust::AlphaChannelOption::Associate,
            AlphaChannel::Background => magick_rust::AlphaChannelOption::Background,
            AlphaChannel::Copy => magick_rust::AlphaChannelOption::Copy,
            AlphaChannel::Deactivate => magick_rust::AlphaChannelOption::Deactivate,
            AlphaChannel::Discrete => magick_rust::AlphaChannelOption::Discrete,
            AlphaChannel::Disassociate => magick_rust::AlphaChannelOption::Disassociate,
            AlphaChannel::Extract => magick_rust::AlphaChannelOption::Extract,
            AlphaChannel::Off => magick_rust::AlphaChannelOption::Off,
            AlphaChannel::On => magick_rust::AlphaChannelOption::On,
            AlphaChannel::Opaque => magick_rust::AlphaChannelOption::Opaque,
            AlphaChannel::Remove => magick_rust::AlphaChannelOption::Remove,
            AlphaChannel::Set => magick_rust::AlphaChannelOption::Set,
            AlphaChannel::Shape => magick_rust::AlphaChannelOption::Shape,
            AlphaChannel::Transparent => magick_rust::AlphaChannelOption::Transparent,
        }
    }
}

impl From<magick_rust::AlphaChannelOption> for AlphaChannel {
    fn from(from: magick_rust::AlphaChannelOption) -> Self {
        match from {
            magick_rust::AlphaChannelOption::Undefined => AlphaChannel::Undefined,
            magick_rust::AlphaChannelOption::Activate => AlphaChannel::Activate,
            magick_rust::AlphaChannelOption::Associate => AlphaChannel::Associate,
            magick_rust::AlphaChannelOption::Background => AlphaChannel::Background,
            magick_rust::AlphaChannelOption::Copy => AlphaChannel::Copy,
            magick_rust::AlphaChannelOption::Deactivate => AlphaChannel::Deactivate,
            magick_rust::AlphaChannelOption::Discrete => AlphaChannel::Discrete,
            magick_rust::AlphaChannelOption::Disassociate => AlphaChannel::Disassociate,
            magick_rust::AlphaChannelOption::Extract => AlphaChannel::Extract,
            magick_rust::AlphaChannelOption::Off => AlphaChannel::Off,
            magick_rust::AlphaChannelOption::On => AlphaChannel::On,
            magick_rust::AlphaChannelOption::Opaque => AlphaChannel::Opaque,
            magick_rust::AlphaChannelOption::Remove => AlphaChannel::Remove,
            magick_rust::AlphaChannelOption::Set => AlphaChannel::Set,
            magick_rust::AlphaChannelOption::Shape => AlphaChannel::Shape,
            magick_rust::AlphaChannelOption::Transparent => AlphaChannel::Transparent,
            _ => AlphaChannel::Undefined,
        }
    }
}
