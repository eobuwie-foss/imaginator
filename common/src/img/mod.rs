use magick_rust::*;
use std::sync::Once;
use thiserror::Error;
use tokio::task::block_in_place;

mod alpha_channel;
mod color_profile;
mod colorspace;
mod composite_op;
mod compression;
mod filter;
mod format;
mod gravity;
mod resolution_unit;
use crate::HardwareLocker;

pub use self::alpha_channel::AlphaChannel;
pub use self::color_profile::ColorProfile;
pub use self::colorspace::Colorspace;
pub use self::composite_op::CompositeOperator;
pub use self::compression::CompressionType;
pub use self::filter::Filter;
pub use self::format::{ImageFormat, UnknownImageFormat};
pub use self::gravity::Gravity;
pub use self::resolution_unit::ResolutionUnit;

static START: Once = Once::new();

#[derive(Debug, Error)]
pub enum ImageMagickError {
    #[error("ImageMagick error: {0}")]
    Native(#[from] MagickError),
    #[error(transparent)]
    Hardware(#[from] crate::CpuNotAvailable),
    #[error(transparent)]
    UnknownImageFormat(#[from] format::UnknownImageFormat),
}

#[derive(Clone, Debug)]
pub struct Image {
    wand: MagickWand,
    locker: HardwareLocker,
}

pub type ImgResult<T> = Result<T, ImageMagickError>;

// TODO: Investigate whether that's safe
unsafe impl Send for Image {}
unsafe impl Sync for Image {}

impl Image {
    pub fn new<T: AsRef<[u8]>, R: Into<Option<f64>>>(
        source: Option<T>,
        resolution: R,
        locker: HardwareLocker,
    ) -> Result<Self, ImageMagickError> {
        init_magick();
        block_in_place(move || {
            let instance = Image {
                wand: MagickWand::new(),
                locker,
            };
            if let Some(resolution) = resolution.into() {
                instance.wand.set_resolution(resolution, resolution)?;
            }
            if let Some(source) = source.as_ref() {
                instance.wand.read_image_blob(source)?;
            }
            Ok(instance)
        })
    }

    /// Generate new image, filled with given color.
    pub fn empty_image(
        &mut self,
        color: &str,
        width: usize,
        height: usize,
        format: Option<&str>,
    ) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| {
            let mut pw = PixelWand::new();
            pw.set_color(color)?;
            self.wand.new_image(width, height, &pw)?;
            self.wand.set_image_format(format.unwrap_or("png"))?;
            Ok(())
        })
    }

    /// Change brightness and contrast of the image
    pub fn set_brightness_contrast(&mut self, brightness: f64, contrast: f64) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| Ok(self.wand.brightness_contrast_image(brightness, contrast)?))
    }

    ///  Ping is similar to read except only enough of the image is read to determine the image columns, rows, and filesize.
    ///  The columns, rows and fileSize attributes are valid after invoking ping.
    ///  The image data is not valid after calling ping.
    pub fn ping(&mut self, source: impl AsRef<[u8]>) -> ImgResult<()> {
        let l = self.locker.clone();
        let s = &source;
        l.run_blocking(|| Ok(self.wand.ping_image_blob(s)?))
    }

    pub fn read(&mut self, source: &Vec<u8>) -> ImgResult<()> {
        self.locker
            .run_blocking(|| Ok(self.wand.read_image_blob(source)?))
    }

    pub fn encode(&self, format: ImageFormat) -> ImgResult<Vec<u8>> {
        self.locker
            .run_blocking(move || Ok(self.wand.write_image_blob(format.magick_str())?))
    }

    pub fn resize(&self, w: usize, h: usize, filter: &Filter) -> ImgResult<()> {
        self.locker
            .run_blocking(move || Ok(self.wand.resize_image(w, h, filter.into())?))
    }

    pub fn fit_in(&self, w: usize, h: usize) -> ImgResult<()> {
        self.locker.run_blocking(move || {
            self.wand.fit(w, h);
            Ok(())
        })
    }

    pub fn compose(
        &self,
        operator: &CompositeOperator,
        other: &Image,
        x: isize,
        y: isize,
    ) -> ImgResult<()> {
        self.locker.run_blocking(move || {
            Ok(self
                .wand
                .compose_images(&other.wand, (*operator).into(), true, x, y)?)
        })
    }

    pub fn crop(&self, x: isize, y: isize, width: usize, height: usize) -> ImgResult<()> {
        self.locker
            .run_blocking(move || Ok(self.wand.crop_image(width, height, x, y)?))
    }

    pub fn extend(&self, x: isize, y: isize, width: usize, height: usize) -> ImgResult<()> {
        self.locker
            .run_blocking(move || Ok(self.wand.extend_image(width, height, x, y)?))
    }

    pub fn trim(&self, fuzz: f64) -> ImgResult<()> {
        self.locker
            .run_blocking(move || Ok(self.wand.trim_image(fuzz)?))
    }

    pub fn width(&self) -> usize {
        self.wand.get_image_width()
    }

    pub fn height(&self) -> usize {
        self.wand.get_image_height()
    }

    pub fn set_quality(&mut self, quality: usize) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| Ok(self.wand.set_image_compression_quality(quality)?))
    }

    pub fn format(&self) -> ImgResult<ImageFormat> {
        self.locker
            .run_blocking(move || Ok(self.wand.get_image_format()?.parse()?))
    }

    pub fn set_format(&mut self, format: &ImageFormat) -> ImgResult<()> {
        let l = self.locker.clone();

        l.run_blocking(move || {
            self.wand.set_image_format(format.magick_str())?;
            if format == &ImageFormat::TIFF {
                // If we don't set that, some popular photo editing programs
                // might have problems with opening the file.
                self.wand.set_option("tiff:rows-per-strip", "2")?;
            } else if format == &ImageFormat::PDF {
                // For some reason, page geometry can assume incorrect values.
                // Resetting it seems to help.
                self.wand.reset_image_page("0x0")?;
                // Also, for some reason ImageMagick needs the wand resolution set for PDFs.
                let (x_dpi, y_dpi) = self.resolution()?;
                self.wand.set_resolution(x_dpi, y_dpi)?;
            }
            Ok(())
        })
    }

    pub fn colorspace(&self) -> Colorspace {
        self.wand.get_image_colorspace().into()
    }

    pub fn set_colorspace(&mut self, colorspace: &Colorspace) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| {
            Ok(self
                .wand
                .transform_image_colorspace(colorspace.to_owned().into())?)
        })
    }

    pub fn gravity(&self) -> Gravity {
        self.wand.get_gravity().into()
    }

    pub fn set_gravity(&mut self, gravity: &Gravity) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| Ok(self.wand.set_gravity(gravity.to_owned().into())?))
    }

    pub fn set_alpha_channel(&mut self, alpha_channel: &AlphaChannel) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| {
            Ok(self
                .wand
                .set_image_alpha_channel(alpha_channel.to_owned().into())?)
        })
    }

    pub fn compression(&self) -> CompressionType {
        self.wand.get_compression().into()
    }

    pub fn set_compression(&mut self, compression: &CompressionType) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| Ok(self.wand.set_compression(compression.to_owned().into())?))
    }

    pub fn transform_color_profile(
        &mut self,
        src_profile: &ColorProfile,
        dest_profile: &ColorProfile,
    ) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| {
            self.wand.profile_image("icc", src_profile)?;
            self.wand.profile_image("icc", dest_profile)?;
            Ok(())
        })
    }

    pub fn set_color_profile(&mut self, profile: &ColorProfile) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| Ok(self.wand.profile_image("icc", profile)?))
    }

    pub fn resample(&mut self, x_dpi: f64, y_dpi: f64, filter: &Filter) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(move || Ok(self.wand.resample_image(x_dpi, y_dpi, filter.into())?))
    }

    pub fn resolution(&self) -> Result<(f64, f64), ImageMagickError> {
        Ok(self.wand.get_image_resolution()?)
    }

    pub fn set_resolution(&mut self, x_dpi: f64, y_dpi: f64) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| {
            self.wand.profile_image("8bim", None)?;
            self.wand.set_image_resolution(x_dpi, y_dpi)?;
            Ok(())
        })
    }

    pub fn resolution_unit(&self) -> ResolutionUnit {
        self.wand.get_image_units().into()
    }

    pub fn set_resolution_unit(&mut self, unit: &ResolutionUnit) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| Ok(self.wand.set_image_units(unit.to_owned().into())?))
    }

    pub fn set_background_color(&self, color: &str) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(|| {
            let mut pw = PixelWand::new();
            pw.set_color(color)?;
            Ok(self.wand.set_image_background_color(&pw)?)
        })
    }

    pub fn flip(&mut self) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(move || Ok(self.wand.flip_image()?))
    }

    pub fn flop(&mut self) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(move || Ok(self.wand.flop_image()?))
    }

    pub fn sepia(&mut self, threshold: f64) -> ImgResult<()> {
        let l = self.locker.clone();
        l.run_blocking(move || Ok(self.wand.sepia_tone_image(threshold)?))
    }
}

fn init_magick() {
    START.call_once(|| {
        magick_wand_genesis();
    });
}

pub fn cleanup_magick() {
    magick_wand_terminus();
}
