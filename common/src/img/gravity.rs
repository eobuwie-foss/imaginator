use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unknown gravity type: {0}")]
pub struct UnknownGravity(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub enum Gravity {
    Undefined,
    NorthWest,
    North,
    NorthEast,
    West,
    Center,
    East,
    SouthWest,
    South,
    SouthEast,
}

impl FromStr for Gravity {
    type Err = UnknownGravity;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => Gravity::Undefined,
            "north_west" => Gravity::NorthWest,
            "north" => Gravity::North,
            "north_east" => Gravity::NorthEast,
            "west" => Gravity::West,
            "center" => Gravity::Center,
            "east" => Gravity::East,
            "south_west" => Gravity::SouthWest,
            "south" => Gravity::South,
            "south_east" => Gravity::SouthEast,
            _ => return Err(UnknownGravity(input.to_owned())),
        })
    }
}

impl From<Gravity> for magick_rust::GravityType {
    fn from(from: Gravity) -> magick_rust::GravityType {
        match from {
            Gravity::Undefined => magick_rust::GravityType::Undefined,
            Gravity::NorthWest => magick_rust::GravityType::NorthWest,
            Gravity::North => magick_rust::GravityType::North,
            Gravity::NorthEast => magick_rust::GravityType::NorthEast,
            Gravity::West => magick_rust::GravityType::West,
            Gravity::Center => magick_rust::GravityType::Center,
            Gravity::East => magick_rust::GravityType::East,
            Gravity::SouthWest => magick_rust::GravityType::SouthWest,
            Gravity::South => magick_rust::GravityType::South,
            Gravity::SouthEast => magick_rust::GravityType::SouthEast,
        }
    }
}

impl From<magick_rust::GravityType> for Gravity {
    fn from(from: magick_rust::GravityType) -> Self {
        match from {
            magick_rust::GravityType::Undefined => Gravity::Undefined,
            magick_rust::GravityType::NorthWest => Gravity::NorthWest,
            magick_rust::GravityType::North => Gravity::North,
            magick_rust::GravityType::NorthEast => Gravity::NorthEast,
            magick_rust::GravityType::West => Gravity::West,
            magick_rust::GravityType::Center => Gravity::Center,
            magick_rust::GravityType::East => Gravity::East,
            magick_rust::GravityType::SouthWest => Gravity::SouthWest,
            magick_rust::GravityType::South => Gravity::South,
            magick_rust::GravityType::SouthEast => Gravity::SouthEast,
        }
    }
}
