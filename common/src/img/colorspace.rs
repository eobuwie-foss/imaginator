use magick_rust;
use std::str::FromStr;
use thiserror::Error;

#[derive(PartialEq, Eq, Debug, Error)]
#[error("Unknown colorspace: {0}")]
pub struct UnknownColorspace(String);

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
#[allow(non_camel_case_types)]
pub enum Colorspace {
    Undefined,
    CMY,
    CMYK,
    GRAY,
    HCL,
    HCLp,
    HSB,
    HSI,
    HSL,
    HSV,
    HWB,
    Lab,
    LCH,
    LCHab,
    LCHuv,
    Log,
    LMS,
    Luv,
    OHTA,
    Rec601YCbCr,
    Rec709YCbCr,
    RGB,
    scRGB,
    sRGB,
    Transparent,
    xyY,
    XYZ,
    YCbCr,
    YCC,
    YDbDr,
    YIQ,
    YPbPr,
    YUV,
    LinearGRAY,
}

impl FromStr for Colorspace {
    type Err = UnknownColorspace;
    fn from_str(input: &str) -> Result<Self, Self::Err> {
        let lowercase = input.to_owned().to_lowercase();
        Ok(match lowercase.as_str() {
            "undefined" => Colorspace::Undefined,
            "cmy" => Colorspace::CMY,
            "cmyk" => Colorspace::CMYK,
            "gray" => Colorspace::GRAY,
            "hcl" => Colorspace::HCL,
            "hclp" => Colorspace::HCLp,
            "hsb" => Colorspace::HSB,
            "hsi" => Colorspace::HSI,
            "hsl" => Colorspace::HSL,
            "hsv" => Colorspace::HSV,
            "hwb" => Colorspace::HWB,
            "lab" => Colorspace::Lab,
            "lch" => Colorspace::LCH,
            "lchab" => Colorspace::LCHab,
            "lchuv" => Colorspace::LCHuv,
            "log" => Colorspace::Log,
            "lms" => Colorspace::LMS,
            "luv" => Colorspace::Luv,
            "ohta" => Colorspace::OHTA,
            "rec601ycbcr" => Colorspace::Rec601YCbCr,
            "rec709ycbcr" => Colorspace::Rec709YCbCr,
            "rgb" => Colorspace::RGB,
            "scrgb" => Colorspace::scRGB,
            "srgb" => Colorspace::sRGB,
            "transparent" => Colorspace::Transparent,
            "xyy" => Colorspace::xyY,
            "xyz" => Colorspace::XYZ,
            "ycbcr" => Colorspace::YCbCr,
            "ycc" => Colorspace::YCC,
            "ydbdr" => Colorspace::YDbDr,
            "yiq" => Colorspace::YIQ,
            "ypbpr" => Colorspace::YPbPr,
            "yuv" => Colorspace::YUV,
            "lineargray" => Colorspace::LinearGRAY,
            _ => return Err(UnknownColorspace(input.to_owned())),
        })
    }
}

impl From<Colorspace> for magick_rust::ColorspaceType {
    fn from(from: Colorspace) -> magick_rust::ColorspaceType {
        match from {
            Colorspace::Undefined => magick_rust::ColorspaceType::Undefined,
            Colorspace::CMY => magick_rust::ColorspaceType::CMY,
            Colorspace::CMYK => magick_rust::ColorspaceType::CMYK,
            Colorspace::GRAY => magick_rust::ColorspaceType::GRAY,
            Colorspace::HCL => magick_rust::ColorspaceType::HCL,
            Colorspace::HCLp => magick_rust::ColorspaceType::HCLp,
            Colorspace::HSB => magick_rust::ColorspaceType::HSB,
            Colorspace::HSI => magick_rust::ColorspaceType::HSI,
            Colorspace::HSL => magick_rust::ColorspaceType::HSL,
            Colorspace::HSV => magick_rust::ColorspaceType::HSV,
            Colorspace::HWB => magick_rust::ColorspaceType::HWB,
            Colorspace::Lab => magick_rust::ColorspaceType::Lab,
            Colorspace::LCH => magick_rust::ColorspaceType::LCH,
            Colorspace::LCHab => magick_rust::ColorspaceType::LCHab,
            Colorspace::LCHuv => magick_rust::ColorspaceType::LCHuv,
            Colorspace::Log => magick_rust::ColorspaceType::Log,
            Colorspace::LMS => magick_rust::ColorspaceType::LMS,
            Colorspace::Luv => magick_rust::ColorspaceType::Luv,
            Colorspace::OHTA => magick_rust::ColorspaceType::OHTA,
            Colorspace::Rec601YCbCr => magick_rust::ColorspaceType::Rec601YCbCr,
            Colorspace::Rec709YCbCr => magick_rust::ColorspaceType::Rec709YCbCr,
            Colorspace::RGB => magick_rust::ColorspaceType::RGB,
            Colorspace::scRGB => magick_rust::ColorspaceType::scRGB,
            Colorspace::sRGB => magick_rust::ColorspaceType::sRGB,
            Colorspace::Transparent => magick_rust::ColorspaceType::Transparent,
            Colorspace::xyY => magick_rust::ColorspaceType::xyY,
            Colorspace::XYZ => magick_rust::ColorspaceType::XYZ,
            Colorspace::YCbCr => magick_rust::ColorspaceType::YCbCr,
            Colorspace::YCC => magick_rust::ColorspaceType::YCC,
            Colorspace::YDbDr => magick_rust::ColorspaceType::YDbDr,
            Colorspace::YIQ => magick_rust::ColorspaceType::YIQ,
            Colorspace::YPbPr => magick_rust::ColorspaceType::YPbPr,
            Colorspace::YUV => magick_rust::ColorspaceType::YUV,
            Colorspace::LinearGRAY => magick_rust::ColorspaceType::LinearGRAY,
        }
    }
}

impl From<magick_rust::ColorspaceType> for Colorspace {
    fn from(from: magick_rust::ColorspaceType) -> Colorspace {
        match from {
            magick_rust::ColorspaceType::Undefined => Colorspace::Undefined,
            magick_rust::ColorspaceType::CMY => Colorspace::CMY,
            magick_rust::ColorspaceType::CMYK => Colorspace::CMYK,
            magick_rust::ColorspaceType::GRAY => Colorspace::GRAY,
            magick_rust::ColorspaceType::HCL => Colorspace::HCL,
            magick_rust::ColorspaceType::HCLp => Colorspace::HCLp,
            magick_rust::ColorspaceType::HSB => Colorspace::HSB,
            magick_rust::ColorspaceType::HSI => Colorspace::HSI,
            magick_rust::ColorspaceType::HSL => Colorspace::HSL,
            magick_rust::ColorspaceType::HSV => Colorspace::HSV,
            magick_rust::ColorspaceType::HWB => Colorspace::HWB,
            magick_rust::ColorspaceType::Lab => Colorspace::Lab,
            magick_rust::ColorspaceType::LCH => Colorspace::LCH,
            magick_rust::ColorspaceType::LCHab => Colorspace::LCHab,
            magick_rust::ColorspaceType::LCHuv => Colorspace::LCHuv,
            magick_rust::ColorspaceType::Log => Colorspace::Log,
            magick_rust::ColorspaceType::LMS => Colorspace::LMS,
            magick_rust::ColorspaceType::Luv => Colorspace::Luv,
            magick_rust::ColorspaceType::OHTA => Colorspace::OHTA,
            magick_rust::ColorspaceType::Rec601YCbCr => Colorspace::Rec601YCbCr,
            magick_rust::ColorspaceType::Rec709YCbCr => Colorspace::Rec709YCbCr,
            magick_rust::ColorspaceType::RGB => Colorspace::RGB,
            magick_rust::ColorspaceType::scRGB => Colorspace::scRGB,
            magick_rust::ColorspaceType::sRGB => Colorspace::sRGB,
            magick_rust::ColorspaceType::Transparent => Colorspace::Transparent,
            magick_rust::ColorspaceType::xyY => Colorspace::xyY,
            magick_rust::ColorspaceType::XYZ => Colorspace::XYZ,
            magick_rust::ColorspaceType::YCbCr => Colorspace::YCbCr,
            magick_rust::ColorspaceType::YCC => Colorspace::YCC,
            magick_rust::ColorspaceType::YDbDr => Colorspace::YDbDr,
            magick_rust::ColorspaceType::YIQ => Colorspace::YIQ,
            magick_rust::ColorspaceType::YPbPr => Colorspace::YPbPr,
            magick_rust::ColorspaceType::YUV => Colorspace::YUV,
            magick_rust::ColorspaceType::LinearGRAY => Colorspace::LinearGRAY,
            _ => Colorspace::Undefined, // Handle any undefined mappings, if necessary
        }
    }
}
