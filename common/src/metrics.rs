use prometheus::{
    self, register_counter, register_counter_vec, register_histogram_vec, Counter, CounterVec,
    HistogramVec, TextEncoder,
};

use lazy_static::lazy_static;

lazy_static! {
    pub static ref FILTER_DURATION: HistogramVec = register_histogram_vec!(
        "filter_duration",
        "Duration of filter execution",
        &["filter"],
        vec![]
    )
    .unwrap();
    pub static ref HTTP_DURATION: HistogramVec = register_histogram_vec!(
        "http_requests_duration_seconds",
        "Duration of HTTP requests",
        &["status"],
        vec![]
    )
    .unwrap();
    pub static ref HTTP_COUNTER: Counter =
        register_counter!("http_requests_total", "Total number of HTTP requests made",).unwrap();
    pub static ref CACHE_HIT_COUNTER: CounterVec = register_counter_vec!(
        "cache_ratio",
        "Total number of cache hits",
        &["result", "cache_name", "filter", "cache_type"],
    )
    .unwrap();
}

pub fn get_current_metrics() -> String {
    let encoder = TextEncoder::new();
    let metric_families = prometheus::gather();
    encoder.encode_to_string(&metric_families).unwrap()
}
