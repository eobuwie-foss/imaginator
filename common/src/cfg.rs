use std::any::{Any, TypeId};
use std::collections::HashMap;

pub static mut CONFIG: Option<HashMap<TypeId, Box<dyn Any>>> = None;

pub fn config<T: 'static>() -> Option<&'static T> {
    unsafe {
        CONFIG.as_ref().and_then(|cfg| {
            cfg.get(&TypeId::of::<T>())
                .and_then(|val| val.downcast_ref())
        })
    }
}

pub fn config_ref<T: 'static>() -> &'static T {
    unsafe {
        CONFIG
            .as_ref()
            .and_then(|cfg| {
                cfg.get(&TypeId::of::<T>())
                    .and_then(|val| val.downcast_ref())
            })
            .expect("Config must be loaded at the boot")
    }
}
