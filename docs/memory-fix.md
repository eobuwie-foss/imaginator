# Jakie prace zostały wykonane

Pierwszą ważną zmianą jest użycie re-używalnego klienta HTTP. Jest to istotne ze względu na ilość otwartych `socket`'ów oraz `pool` jest szybszy niż otwieranie nowych połączeń za każdym razem.

`rust_magick` został cofnięty. Z powodu ponad scope tego MR nowa paczka nie działa poprawnie. Najnowsza paczka zdaje się działać dużo wolniej. Zadanie przeniesione do [[IMAGINATOR] Sprawdzić najnowszy rust_magic](https://ccc-group.atlassian.net/browse/CORE-1542)

Zapobieganie blokowaniu na operacjach na pliku dzięki `tokio::fs::File` i `spawn_blocking` podczas odczytu i zapisu do pliku. Wszystkie operacje na plikach niestety są nie dość szybkie, żeby je wykonywać w async i trzeba je delegować do własnych wątków. W przeciwnym przypadku zapycha się wielowątkowość.

`Buffer`y są teraz prealokowane, żeby zapobiec re-alokacjom, które są bardzo kosztowne.

Został wprowadzony `rate limit` żeby zapobiec dodawaniu kolejnych i kolejnych wątków w przypadku zwiększonemu ruchowi.

# Wysycanie sprzętu

Najpierw poruszę banały.

Domyślnie `tokio` za każdym razem jak startuje wykorzystuje `num_cpus` który jednokrotnie wczytuje statystyki sprzętowe, żeby, wiedzieć się ile jest fizycznych rdzeni i wątków sprzętowych. Ilość wątków sprzętowych jest następnie wykorzystywana do utworzenia `Context`'ów, którzy posiadają własne kolejki zadań `Task` wykonywanymi w zbalansowanych odstępach czasu (tokio stara się, żeby każde zadanie otrzymało tyle samo czasu).

Za każdym razem jak tworzymy asynchroniczne zadanie trafia ono do kolejki po wywołaniu `.await` albo `spawn`. Każdy worker ma własną kolejkę i korzysta głównie z niej, małym wyjątkiem jest sytuacja, gdy `Context` A nie ma zadań, a `Context` B ma ich dużo. Wtedy zachodzi tzw. podbieranie zadań.


```
Worker A        Worker B        Worker C
                  
  [=]             [=]             [=]
  [=]             [=]             [=]
  [=]             [=]             [=]
  [=]                             [=]
                                  [=]
```

`await` dodatkowo sprawia, że obecne zadanie trafia z powrotem do kolejki pozwalając `Context`'owi przejąć wątek, na którym pracują zadania.

```rust
async fn exec() {
    for n in 1..10 {
        tokio::time::sleep(std::time::Duration::from_secs(3)).await; // Task will be parked for 3 seconds but Worker will will continue other tasks 
    }
}
```

Istotną częścią kodu powyżej jest `.await`, który przekaże kontrolę do `Context`. Jeśli zamiast tego wykonamy `std::thread::sleep` cały `Context` zostanie zablokowany i nic nie zostanie wykonane w tym czasie.

```rust
async fn exec() {
    for n in 1..10 {
        std::thread::sleep(std::time::Duration::from_secs(3)); // Entire worker is blocked
    }
}
```

Ważną rzeczą jest, żeby każde zadanie wykonywane w `async` nie trwało więcej niż 100ns w przeciwnym razie inne zadania `Context`u będą musiały długo czekać na zrobienie czegokolwiek, a wraz ze wzrostem ilości powolnych zadań może dojść do sytuacji, gdzie inne zadania będą wykonywane bardzo rzadko. To jest bardzo istotne dla web serwerów.

Dla przykładu w przypadku operacji na systemie plików wszystkie operacje są wolne i powinny być albo opakowane w `spawn_blocking`, `block_in_place` albo powinno się skorzystać z `tokio::fs::File`.

```rust
async fn spawn(buffer: Vec<u8>) {
    tokio::task::spawn_blocking(move || {
        let file = std::fs::File::open("./path").unwrap();
        file.write_all(&buffer);
    });
}

async fn in_place(buffer: &[u8]) {
    tokio::task::block_in_place(move || {
        let file = std::fs::File::open("./path").unwrap();
        file.write_all(&buffer);
    });
}

async fn tokio_example(buffer: Vec<u8>) {
    let file = tokio::fs::File::open("./path").await.unwrap();
    file.write_all(&buffer).await;
}
```

Na potrzeby tego, co się wydarzyło, musimy poznać jedynie mechanikę `spawn_blocking` i `block_in_place`. 

`spawn_blocking` wystartuje nowy `Context` z własnym wątkiem. Ja widzimy wyżej możemy na niego poczekać bez blokowania obecnego `Context`u. Wymaga ono jednak żeby wszystkie dane używane wewnątrz bloku musi mieć lifetime `'static` (tzn. znajdować się na heap).

`block_in_place` dla odmiany tworzy nowy `Context` z własnym wątkiem i kolejną zadań do wykonania, przenosi wszystko oprócz obecnego zadania do nowego `Context`u, wykonuje obecne zadanie, po czym próbuje zabrać zadania z nowego `Context`u.

## `cache` filter

W `imaginator` dostęp do `cache` był niestety blokujący na głównym wątku przy jednoczesnym tworzeniu filtrów:

* sprawdź `cache` dla przetworzonego obrazka
* jeśli jest pusty, wykonaj przetworzenie na:
  * sprawdź `cache` na pobrany obrazek
  * jeśli jest pustym, pobierz zasób

Tworzyło to sytuację typu:

```
REQUEST 1
    [===================] CACHE
                        [==] OPERATION
                            [==] OPERATION
                                [==] OPERATION
                                    [===================] CACHE
                                                        [==] OPERATION
                                                            [==] OPERATION
                                                                [==] OPERATION
REQUEST 2
    [===================] CACHE
                        [==] OPERATION
                            [==] OPERATION
                                [==] OPERATION
                                    [===================] CACHE
                                                        [==] OPERATION
                                                            [==] OPERATION
                                                                [==] OPERATION
REQUEST 3
    [===================] CACHE
                        [==] OPERATION
                            [==] OPERATION
                                [==] OPERATION
                                    [===================] CACHE
                                                        [==] OPERATION
                                                            [==] OPERATION
                                                                [==] OPERATION
```

Tokio posiada ograniczoną ilość `Context`ów tworzonych podczas startu. Istnieje możliwość stworzenia nowych, ale ze względów wydajnościowych zaleca się to tylko dla wolnych zadań. Przy małej ilości połączeń nie jest to problem jednak w sytuacji, gdy ilość połączeń znacząco przeważa ilość dostępnych zasobów systemowych, dochodzi do tzw. resource starvation gdzie `Context` posiada `N` połączeń i próbuje wykonać zapytanie, ale non stop trafia na wolny task `cache`.

```
WORKER 1
    ( 1) [===================] CACHE
    ( 2) [==] FAST
    ( 3) [==] FAST
    ( 4) [===================] CACHE
    ( 5) [=] FAST
    ( 6) [==] FAST
    ( 7) [===================] CACHE
    ( 9) [==] FAST
    (10) [===] FAST
```

`Context` w tym scenariuszu zawiśnie na zadaniu `1`, następnie sprawdzi gotowość `2`, to zadanie może oddać sterowanie wątkiem do `Context`u w trakcie i wtedy przechodzimy do `3` , następnie `4` i znowu zawiśnie bez możliwości zrobienia czegokolwiek. 

Rozwiązaniem na to jest skorzystanie z `spawn_blocking`, które przeniesie całość do własnego wątku, `block_in_place`, które zapobiegnie zablokowaniu innych zadań lub `tokio::fs`, które poprawnie implementuje dostęp do systemu plików dla zadań wielowątkowych.

### Wdrożone poprawki:

* Czytanie z pliku zostało zaimplementowane za pomocą `tokio::fs::File`
* Zapis do pliku został zaimplementowany za pomocą `block_in_place`

## Przetwarzanie obrazka

Wszystkie operacje na obrazkach są długie i wymagają dużo zasobów. To na szczęście zostało to zaimplementowane z wykorzystaniem `block_in_place`. Pojawił się jednak nieprzewidziany problem.

Jak opisałem wcześniej `block_in_place` Tokio stworzy nowy wątek, wykona pracę i spróbuje zabrać zadania z nowego wątku. Jeśli jednak zabranie zadań nie jest możliwe, nie ma wolnych wątków i wszystko jest zablokowane przez wolne zadania ilość nowych `Context`'ów zacznie rosnąć, aż do maksimum sprzętowego.

```
P   - Pending
D   - Done
=== - Long
*   - In progress

New block_in_place          | Working on block_in_place   | Second block_in_place in Worker 2   |                                               |                                                |

Worker 1          Worker 2  | Worker 1          Worker 2  | Worker 1          Worker 2          | Worker 1          Worker 2          Worker 3  |  Worker 1          Worker 2          Worker 3  |
  [P]       ---->           |  *[========]       *[P]     |  *[========]       *[P]             |  *[========]        [D]                       |   *[========]       *[========]        [D]     |
  [P]       ---->           |                     [P]     |                     [P]             |                     [P]         ->            |                                       *[P]     |
  [P]       ---->           |                     [P]     |                     [P]             |                     [P]         ->            |                                        [P]     |
  [P]       ---->           |                     [P]     |                     [P]             |                     [P]         ->            |                                                |
 *[========]                |                             |                     [========]      |                    *[========]                |                                        ...     |
```

`block_in_place` pod spodem tworzy nowy `Context`, który jest uruchamiany za pomocą `spawn_blocking`.
Pod koniec zadania w oryginalnym wątku budżet `Context`u (tj, ile zadań jest w stanie przetworzyć `Context`) jest ustawiany na `0` żeby zapobiec przyjmowaniu nowych zadań przez `Context`.

W tym momencie oryginalny `Context` stara się odzyskać przesunięte zadania, czyli takie, które zostały dodane przez inne zapytanie i nie zostały jeszcze zakończone. W tym procesie pośredniczy kolejka o małej pojemności.
Problem pojawia się jak mamy bardzo dużo bardzo wolnych zadań, które kolejkę powiększają bez kontroli. Normalnie, jeśli lista zadań jest za duża `tokio` spróbuje przesunąć tylko połowę.
To czasem jest dużo za dużo, więc zadanie jest wpychane na kolejkę `LIFO` `Context` co zapobiega usunięciu `Context`.

W pewnym momencie ilość ostatecznie spada do `0` i `Context` zostaje zaparkowany, tzn. nie robi nic, nie może przyjąć żadnych zadań i czeka na nowe zadania.

Każdy `Context` wymagał alokacji dużej porcji RAM w funkcji `app` (faktyczny handler http request), co było bardzo wolne i powodowało stały wyciek pamięci.
Poniższy wykres przedstawia ilość zarezerwowanej i uwolnionej pamięci w czasie. Znak `#` oznacza migawkę, w której ilość zarezerwowanej i wyzwolonej pamięci. W trakcie życia aplikacji powinniśmy widzieć wzrosty i spadki pamięci, ponieważ aplikacja podczas obsługi każdego zapytania rezerwuje pamięć na tworzenie filtrów `imaginator`'a. Po inicjalnym wzroście mamy dość płaski wykres, po czym spada.

```
siege -r 3 -c 40  http://localhost:3000/resize\(download\(pim:0/0/0/3/00037f83630ce64387382e633f78aa75155c341a_25_7628067374440.jpg\),50,50\)
```

```
    MB
173.5^         #
     |         #::
     |         #:: : : @      : :    :         ::                  :
     |         #:: ::: @:  @: : :::  :::  : :: :::: ::: ::  : : :  :
     |         #:: ::: @:  @: : :::  :::  : :: :::: ::: :: ::@: :: :
     |        :#@::::: @:  @: : :::  :::  : :: :::: ::: :: ::@: :::::   @
     |       @:#@:@::: @:  @: ::::: ::::  : :: :::: ::: :: ::@: :::::@: @
     |       @:#@:@::@:@:::@:@:::@:::::::@:@::@@:::::::::::::@:::::::@::@
     |       @:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@::::::@::@
     |      :@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@::@::@@:
     |      :@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@::@@@
     |      :@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@:@@@:
     |      :@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@:
     |      :@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@:
     |      :@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@::
     |      :@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@::
     |     @:@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@:@
     |   @@@:@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@:@
     |  :@@@:@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@@@
     | @:@@@:@:#@:@::@:@::@@:@:::@:@:::::@:@::@@:::::::@:@::@@@:::@:@@@@@@@@@@
   0 +----------------------------------------------------------------------->GB
     0                                                                   153.2
```

Podczas testów ilość RAM dla niewielkiej próbki 1200 zapytań potrafiła dochodzić do 3G.

Pierwsza tura zapytań powoduje wzrost zużycia RAM do 800 Mb, druga 922 Mb, trzecia 967 Mb.
Każda następna zwiększa zużycie, które nie spada. Po przejrzeniu alokacji pamięci większość pamięci jest alokowana i zwalniana przez obróbkę zdjęć (co jest oczekiwane) jednak znajdują się wpisy tokio core:

```bash
ms_print ./massif.out.tier1-resize | less
```

```
|   |     |     |       |                           ->11.57% (1,405,152B) 0x294DA5: tokio::runtime::task::core::CoreStage<T>::poll::{{closure}} (core.rs:161)
|   |     |     |       |                             ->11.57% (1,405,152B) 0x2AEB8F: tokio::loom::std::unsafe_cell::UnsafeCell<T>::with_mut (unsafe_cell.rs:14)
|   |     |     |       |                               ->11.57% (1,405,152B) 0x294AF6: tokio::runtime::task::core::CoreStage<T>::poll (core.rs:151)
```

`11.5%` to niezwykle duża ilość pamięci dla serwera przetwarzającego zdjęcia. Łącznie zostało to wykonane *41* razy w różnych wątkach.

Po analizie okazało się, że każdy nowy wątek utworzony za pomocą `block_in_place` tworzy własną kolejkę oraz tworzy nowy `ContextData` alokując tutaj sporą mapę danych.

Dla szybkich i małych zmian na obrazkach istniał niewielki wyciek pamięci, ale przy większych gdzie tworzony jest nowy wątek, przy każdej zmianie obrazka zazwyczaj długo żyjące wątki nie mogą zostać usunięte i zostają permanentnie.

```
siege --no-parser --no-follow -r 30 -c 40  http://localhost:3000/resize\(download\(pim:0/0/0/3/00037f83630ce64387382e633f78aa75155c341a_25_7628067374440.jpg\),50,50\)
```

Problem został zreplikowany za pomocą prostego programu:

```rust
async fn leak() {
  tokio::task::block_in_place(|| { // tworzy nowy `Context` z nową kolejką
    std::thread::sleep(std::time::Duration::from_millis(800)); // blokuje wątek kompletnie
  });
}

#[tokio::main]
async fn main() {
  for _ in 0..2_000_000 {
    tokio::spawn(leak());
  }
  std::thread::sleep(std::time::Duration::from_secs(8000));
}
```

```
ps huH p $(pidof test_program) | wc -l
```

W ten sposób zostało utworzone 545 aktywnych wątków, które nigdy nie znikają. Przy 1200 jest ich mniej, ale podczas rosnącego ruchu lub po wyczyszczeniu `cache`.

Po zmianach mniej pamięci jest zużywane, ilość zadań jest stabilna oraz zarezerwowana pamięć nie rośnie. Maksymalna pamięć podczas testów nie przekraczała 1800Mb, a wątki są zwalniane bardzo szybko, redukując zarezerwowaną pamięć do 450 Mb. 

```
    MB
143.9^                     :
     |                    #:             :
     |                    #:             :    :                 :
     |                    #:             :    :         ::      : ::
     |                    #:             :    ::        ::@     : ::
     |             :   :::#:  :@         :    ::        ::@     : :: :
     |          :  ::: :::#: ::@:       :::: :::        ::@   ::: :: :
     |          :::::: :::#@ ::@::@   : :::@:@::      ::::@:  :@@ @: :
     |          :::::: :::#@ ::@::@:: :::::@:@@::@: ::::::@:: :@@:@@ :
     |     :::  :::::: :::#@ ::@::@::::::::@:@@::@: ::::::@:: :@@:@@@::::
     |     ::@  ::::::@:::#@ ::@::@::::::::@:@@::@: ::::::@:: :@@:@@@@::::@
     |     ::@  :::::@@:@:#@:@:@::@::::::::@:@@::@: ::::::@:::@@@:@@@@:::@@@
     |     ::@  @:@::@@:@:#@:@:@::@::::::::@:@@::@: ::@@::@@:@@@@:@@@@:::@@@
     |     ::@:@@:@::@@:@:#@:@:@::@:@::@@::@:@@::@: @@@@@@@@@@@@@@@@@@:::@@@@@
     |     ::@@@@:@::@@:@:#@:@:@::@:@@:@@::@:@@:@@::@@@@@@@@@@@@@@@@@@:@:@@@@@
     |    :::@@@@:@::@@:@:#@:@:@::@:@@:@@::@:@@:@@::@@@@@@@@@@@@@@@@@@@@@@@@@@
     |   :@::@@@@:@::@@:@:#@:@:@::@:@@:@@::@:@@:@@::@@@@@@@@@@@@@@@@@@@@@@@@@@
     |  @:@::@@@@:@::@@:@:#@:@:@::@:@@:@@::@:@@:@@::@@@@@@@@@@@@@@@@@@@@@@@@@@
     | @@:@::@@@@:@::@@:@:#@:@:@::@:@@:@@::@:@@:@@::@@@@@@@@@@@@@@@@@@@@@@@@@@
     |:@@:@::@@@@:@::@@:@:#@:@:@::@:@@:@@::@:@@:@@::@@@@@@@@@@@@@@@@@@@@@@@@@@
   0 +----------------------------------------------------------------------->GB
     0                                                                   106.6
```

1. 442 Mb
2. 537 Mb
3. 476 Mb

# Czasy odpowiedzi

## Produkcja


1 porażka
799 sukcesów

```
Transactions:		         799 hits
Availability:		       99.88 %
Elapsed time:		      146.61 secs
Data transferred:	       23.13 MB
Response time:		        6.40 secs
Transaction rate:	        5.45 trans/sec
Throughput:		        0.16 MB/sec
Concurrency:		       34.90
Successful transactions:         799
Failed transactions:	           1
Longest transaction:	       97.92
Shortest transaction:	        0.49
```

## Fix

800 sukcesów

```
Transactions:		         800 hits
Availability:		      100.00 %
Elapsed time:		       31.13 secs
Data transferred:	       23.16 MB
Response time:		        1.43 secs
Transaction rate:	       25.70 trans/sec
Throughput:		        0.74 MB/sec
Concurrency:		       36.84
Successful transactions:         800
Failed transactions:	           0
Longest transaction:	        6.11
Shortest transaction:	        0.17
```

## Percentile czasu odpowiedzi

|             | 1 percentile | 10 percentile | 50 percentile | 95 percentile | 
|-------------|--------------|---------------|---------------|---------------|
| Produkcja   | 0.52         | 0.85          | 2.10          | 14.92         |
| Poprawione  | 0.74         | 1.4           | 1.64          | 2.01          |

Wykres czasów odpowiedzi

* oś Y to czas odpowiedzi
* oś X to kolejne zapytania
* produkcja jest oznaczona niebieskim kolorem
* poprawiony serwer jest oznaczony czerwonym kolorem

<img src="./time_per_req.png" alt="Time per req"/>

### Wdrożone poprawki:

* Każda instancja imaginator ma ograniczoną maksymalną ilość obsługiwanych zapytań

### OHA benches

Hardware

```
-------------
OS: Arch Linux x86_64
Host: MS-7C91 2.0
Kernel: 6.4.10-arch1-1
Uptime: 10 hours, 13 mins
Packages: 2159 (pacman), 191 (nix-user)
Shell: zsh 5.9
Resolution: 2560x1440
WM: sway
Theme: Adwaita [GTK2/3]
Icons: Adwaita [GTK2/3]
Terminal: alacritty
CPU: AMD Ryzen 9 3950X (32) @ 3.500GHz
GPU: AMD ATI Radeon RX 5600 OEM/5600 XT / 5700/5700 XT
Memory: 9079MiB / 64226MiB
```

produkcja

```
oha -n 3000 -c 100 'http://0.0.0.0:3000/eob_product_256w_256h(c/d/2/2/cd22b29a4d85c75313e4fd2e93b192f190352a98_01_0000302774906_KP.jpg,webp)'
```

```
Summary:
  Success rate:	100.00%
  Total:	228.6092 secs
  Slowest:	38.6418 secs
  Fastest:	0.5070 secs
  Average:	7.2866 secs
  Requests/sec:	13.1228

  Total data:	45.45 MiB
  Size/request:	15.51 KiB
  Size/sec:	203.58 KiB

Response time histogram:
   0.507 [1]    |
   4.321 [1219] |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
   8.134 [813]  |■■■■■■■■■■■■■■■■■■■■■
  11.947 [398]  |■■■■■■■■■■
  15.761 [250]  |■■■■■■
  19.574 [131]  |■■■
  23.388 [99]   |■■
  27.201 [38]   |
  31.015 [30]   |
  34.828 [14]   |
  38.642 [7]    |

Response time distribution:
  10% in 1.5378 secs
  25% in 2.1488 secs
  50% in 5.4573 secs
  75% in 10.0935 secs
  90% in 16.3580 secs
  95% in 20.9405 secs
  99% in 28.9630 secs

Details (average, fastest, slowest):
  DNS+dialup:	0.0293 secs, 0.0006 secs, 0.0343 secs
  DNS-lookup:	0.0015 secs, 0.0000 secs, 0.0106 secs

Status code distribution:
  [200] 2998 responses
  [500] 2 responses
```

```
oha -n 3000 -c 10 'http://0.0.0.0:3000/eob_product_256w_256h(c/d/2/2/cd22b29a4d85c75313e4fd2e93b192f190352a98_01_0000302774906_KP.jpg,webp)'
```

```
Summary:
  Success rate:	100.00%
  Total:	240.2526 secs
  Slowest:	2.7404 secs
  Fastest:	0.1604 secs
  Average:	0.7982 secs
  Requests/sec:	12.4869

  Total data:	45.48 MiB
  Size/request:	15.52 KiB
  Size/sec:	193.84 KiB

Response time histogram:
  0.160 [1]    |
  0.418 [468]  |■■■■■■■■■
  0.676 [225]  |■■■■
  0.934 [1515] |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  1.192 [622]  |■■■■■■■■■■■■■
  1.450 [110]  |■■
  1.708 [40]   |
  1.966 [14]   |
  2.224 [4]    |
  2.482 [0]    |
  2.740 [1]    |

Response time distribution:
  10% in 0.3658 secs
  25% in 0.6969 secs
  50% in 0.8395 secs
  75% in 0.9418 secs
  90% in 1.0690 secs
  95% in 1.2272 secs
  99% in 1.6148 secs

Details (average, fastest, slowest):
  DNS+dialup:	0.0033 secs, 0.0018 secs, 0.0057 secs
  DNS-lookup:	0.0027 secs, 0.0009 secs, 0.0045 secs

Status code distribution:
  [200] 3000 responses
```

fixed

```
oha -n 3000 -c 100 'http://0.0.0.0:3000/eob_product_256w_256h(c/d/2/2/cd22b29a4d85c75313e4fd2e93b192f190352a98_01_0000302774906_KP.jpg,webp)'
```

```
Summary:
  Success rate:	100.00%
  Total:	38.7246 secs
  Slowest:	2.5005 secs
  Fastest:	0.1478 secs
  Average:	1.2789 secs
  Requests/sec:	77.4702

  Total data:	16.30 MiB
  Size/request:	5.56 KiB
  Size/sec:	430.96 KiB

Response time histogram:
  0.148 [1]   |
  0.383 [15]  |
  0.618 [119] |■■■■■
  0.854 [319] |■■■■■■■■■■■■■
  1.089 [437] |■■■■■■■■■■■■■■■■■■
  1.324 [667] |■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  1.559 [744] |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  1.795 [466] |■■■■■■■■■■■■■■■■■■■■
  2.030 [168] |■■■■■■■
  2.265 [55]  |■■
  2.501 [9]   |

Response time distribution:
  10% in 0.7495 secs
  25% in 1.0251 secs
  50% in 1.3045 secs
  75% in 1.5405 secs
  90% in 1.7244 secs
  95% in 1.9020 secs
  99% in 2.1032 secs

Details (average, fastest, slowest):
  DNS+dialup:	0.0224 secs, 0.0006 secs, 0.0262 secs
  DNS-lookup:	0.0015 secs, 0.0000 secs, 0.0104 secs

Status code distribution:
  [429] 1927 responses
  [200] 1073 responses
```

```
oha -n 3000 -c 10 'http://0.0.0.0:3000/eob_product_256w_256h(c/d/2/2/cd22b29a4d85c75313e4fd2e93b192f190352a98_01_0000302774906_KP.jpg,webp)'
```

```
Summary:
  Success rate:	100.00%
  Total:	52.8950 secs
  Slowest:	0.3420 secs
  Fastest:	0.1187 secs
  Average:	0.1760 secs
  Requests/sec:	56.7161

  Total data:	45.48 MiB
  Size/request:	15.52 KiB
  Size/sec:	880.43 KiB

Response time histogram:
  0.119 [1]    |
  0.141 [47]   |
  0.163 [665]  |■■■■■■■■■■■■■■
  0.186 [1514] |■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■■
  0.208 [650]  |■■■■■■■■■■■■■
  0.230 [99]   |■■
  0.253 [11]   |
  0.275 [2]    |
  0.297 [4]    |
  0.320 [2]    |
  0.342 [5]    |

Response time distribution:
  10% in 0.1556 secs
  25% in 0.1640 secs
  50% in 0.1742 secs
  75% in 0.1862 secs
  90% in 0.1982 secs
  95% in 0.2062 secs
  99% in 0.2246 secs

Details (average, fastest, slowest):
  DNS+dialup:	0.0029 secs, 0.0003 secs, 0.0049 secs
  DNS-lookup:	0.0028 secs, 0.0000 secs, 0.0049 secs

Status code distribution:
  [200] 3000 responses
```
