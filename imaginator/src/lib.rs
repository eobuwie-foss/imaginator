#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate serde_derive;

extern crate imaginator_common as imaginator;
extern crate imaginator_plugins;

use anyhow::Context;
use imaginator::{systemd_creds::ReadCredentialDir, PluginInformation};
use std::collections::HashMap;
use tracing::trace;

use tracing_ecs::otel_tracing::TracingGuard;

mod app;
mod cfg;
mod correlation_context;
pub mod http;
mod url;

#[cfg(feature = "sentry")]
mod sentry;

static mut PLUGINS: Option<HashMap<String, PluginInformation>> = None;

pub fn plugins_mut() -> &'static mut HashMap<String, PluginInformation> {
    unsafe { PLUGINS.as_mut().unwrap() }
}

pub fn plugins_ref() -> &'static HashMap<String, PluginInformation> {
    unsafe { PLUGINS.as_ref().unwrap() }
}

pub fn init() -> TracingGuard {
    let guard = tracing_ecs::init();

    lazy_static::initialize(&cfg::CONFIG);

    unsafe {
        let plugins = imaginator_plugins::plugins();

        trace!("plugins len {}", plugins.keys().count());
        for p in plugins.keys() {
            trace!("plugin {:?}", p);
        }
        PLUGINS = Some(plugins);
    }
    tracing::debug!("Number of filters: {}", crate::app::FILTERS.len());
    for name in crate::app::FILTERS.keys() {
        trace!("Filter {name:?} mounted");
    }

    for (name, plugin) in plugins_mut().iter() {
        trace!("Initializing plugin {:?}", name);
        if let Some(init) = plugin.init {
            init().unwrap();
        } else {
            trace!(
                "Can't initialize {:?}, plugin doesn't have init closure",
                name
            );
        }
    }

    for (_, plugin) in plugins_mut().iter() {
        if let Some(post) = plugin.post_init {
            post();
        }
    }

    systemd::daemon::notify(false, [(systemd::daemon::STATE_READY, "1")].iter()).unwrap();

    guard
}

pub async fn boot() {
    let _guard = init();

    #[cfg(feature = "sentry")]
    let _sentry = crate::sentry::init(ReadCredentialDir);

    http::server().await;
    for (name, plugin) in plugins_mut() {
        if let Some(exit) = plugin.exit {
            exit()
                .context(format!("failed to run exit hook for plugin {name}"))
                .unwrap();
        }
    }
}
