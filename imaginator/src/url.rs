use crate::cfg::CONFIG;
use crate::imaginator::filter::Filter;
use crate::imaginator::url::filter;

use hmac::{Hmac, Mac};
use sha1::Sha1;

use base64::{engine::general_purpose, Engine as _};
use nom::combinator::{complete, opt, peek, verify};
use nom::error::ParseError;
use nom::sequence::terminated;
use nom::IResult;
use std::borrow::Borrow;
use thiserror::Error;

#[derive(Debug, Error, Clone)]
pub enum UrlParseError {
    #[error("Invalid signature.")]
    InvalidSignature,
    #[error("Url parse error. Unparsed data: {0}")]
    RemainingData(String),
    #[error("Incomplete url.")]
    IncompleteUrl,
    #[error("Url parse error: {0}")]
    ParseError(String),
    #[error("Url decoding error.")]
    UrlDecodingError,
}

// Create alias for HMAC-SHA1
type HmacSha1 = Hmac<Sha1>;

fn check_signature(input: &str, rest: &str) -> bool {
    let sig = match general_purpose::URL_SAFE.decode(input) {
        Ok(sig) => sig,
        // One way or another, the signature is invalid
        Err(_) => return false,
    };
    tracing::info!("{:?} {:?}", CONFIG.secret, rest);
    if let Some(ref secret) = CONFIG.secret {
        let mut mac =
            HmacSha1::new_from_slice(secret.as_bytes()).expect("HMAC can take key of any size");
        mac.update(rest.as_bytes());
        mac.verify_slice(&sig).is_ok()
    } else {
        true
    }
}

fn is_base64(input: &str) -> bool {
    general_purpose::URL_SAFE.decode(input).is_ok()
}

pub fn parse(input: &str) -> Result<Filter, UrlParseError> {
    let url = urlencoding::decode(input).map_err(|_| UrlParseError::UrlDecodingError)?;
    match full_url(&url) {
        Ok(("", (Some(false), _))) => Err(UrlParseError::InvalidSignature),
        Ok(("", (_, filter))) => Ok(filter),
        Ok((remaining, _)) => Err(UrlParseError::RemainingData(remaining.to_owned())),
        Err(nom::Err::Incomplete(_)) => Err(UrlParseError::IncompleteUrl),
        Err(e) => Err(UrlParseError::ParseError(format!("{:?}", e))),
    }
}

fn signature(input: &str) -> IResult<&str, bool, ::nom::error::Error<&str>> {
    let _z = 1;
    let res = terminated_comb(
        input,
        |i| verify_comb(i, |i| ::nom::bytes::streaming::take(28usize)(i), is_base64),
        |i| ::nom::bytes::streaming::tag("/")(i),
    );
    let (head, sig) = res?;
    let res = peek_comb(head, |current_str| {
        ::nom::bytes::streaming::is_not("")(current_str)
    });
    let (head, rest) = res?;
    Ok((head, (check_signature(sig, rest))))
}

fn full_url(input: &str) -> IResult<&str, (Option<bool>, Filter), ::nom::error::Error<&str>> {
    let (optional_signature, sig) =
        opt_comb(input, |pass_input| complete_comb(pass_input, signature))?;
    let (head, filter) = filter(optional_signature)?;
    Ok((head, (sig, filter)))
}

// ---------------------------------- Ports

/// Gets an object from the first parser,
/// then matches an object from the second parser and discards it.
///
/// # Arguments
/// * `input` The string input
/// * `first` The first parser to apply.
/// * `second` The second parser to match an object.
fn terminated_comb<I, O1, O2, E: ParseError<I>, F, G>(
    input: I,
    first: F,
    second: G,
) -> IResult<I, O1, E>
where
    F: Fn(I) -> IResult<I, O1, E>,
    G: Fn(I) -> IResult<I, O2, E>,
{
    terminated(first, second)(input)
}

fn verify_comb<I: Clone, O1, O2, E: ParseError<I>, F, G>(
    input: I,
    first: F,
    second: G,
) -> IResult<I, O1, E>
where
    F: Fn(I) -> IResult<I, O1, E>,
    G: Fn(&O2) -> bool,
    O1: Borrow<O2>,
    O2: ?Sized,
{
    verify(first, second)(input)
}

fn complete_comb<I: Clone, O, E: ParseError<I>, F>(input: I, f: F) -> IResult<I, O, E>
where
    F: Fn(I) -> IResult<I, O, E>,
{
    complete(f)(input)
}

fn peek_comb<I: Clone, O, E: ParseError<I>, F>(input: I, f: F) -> IResult<I, O, E>
where
    F: Fn(I) -> IResult<I, O, E>,
{
    peek(f)(input)
}

/// Optional parser, will return None on Err::Error.
/// To chain an error up, see cut.
fn opt_comb<I: Clone, O, E: ParseError<I>, F>(input: I, f: F) -> IResult<I, Option<O>, E>
where
    F: Fn(I) -> IResult<I, O, E>,
{
    opt(f)(input)
}

#[cfg(test)]
mod tests {
    use crate::url::full_url;
    use imaginator::filter::{Filter, FilterArg, SizeUnit};

    #[test]
    fn parse_resize() {
        let res = full_url("/resize(some,30,40)/alias-name");
        let (head, (signature, filter)) = res.unwrap();
        assert_eq!(head, "");
        assert_eq!(signature, None);
        assert_eq!(
            filter,
            Filter {
                name: "/resize".into(),
                args: vec![
                    FilterArg::String("some".into()),
                    FilterArg::Int(30, SizeUnit::None),
                    FilterArg::Int(40, SizeUnit::None)
                ]
            }
        );
    }

    #[test]
    fn parse_resize_with_size() {
        let res = full_url("/resize(some,30px,0.5w)/alias-name");
        let (head, (signature, filter)) = res.unwrap();
        assert_eq!(head, "");
        assert_eq!(signature, None);
        assert_eq!(
            filter,
            Filter {
                name: "/resize".into(),
                args: vec![
                    FilterArg::String("some".into()),
                    FilterArg::Int(30, SizeUnit::Px),
                    FilterArg::Float(0.5, SizeUnit::Width)
                ]
            }
        );
    }
}
