use crate::cfg::CONFIG;
use crate::correlation_context::CorrelationContext;
use crate::imaginator::filter::{self, FilterArg};
use crate::{plugins_ref, url};
use anyhow::{self, Result};
use chrono::prelude::*;
use futures::{future::BoxFuture, FutureExt};

use hyper::StatusCode;
use imaginator::{HardwareLocker, ImaginatorExtension, Response};
use regex::Regex;
use std::collections::HashMap;
use std::convert::Infallible;
use std::sync::{Arc, Mutex};
use thiserror::Error;
use tracing::{debug, error, trace, warn, Instrument};

type FilterMap = HashMap<
    &'static str,
    &'static (dyn for<'a> Fn(filter::Context, &'a filter::Args) -> filter::Future<'a> + Sync),
>;
lazy_static! {
    pub(crate) static ref FILTERS: FilterMap = {
        use imaginator_plugins;

        let mut map: FilterMap = HashMap::with_capacity(1_000);
        for (_, plugin) in imaginator_plugins::plugins().iter() {
            for (name, filter) in &plugin.filters {
                trace!("Adding imaginator filter: {name:?}");
                map.insert(name, *filter);
            }
        }
        map
    };
    static ref USED_CPU: Arc<Mutex<usize>> = Arc::new(Mutex::new(0));
}

#[derive(Debug, Error)]
#[error("Not enough arguments passed to {name}. Expected at least {min}, but you passed {actual}.")]
pub struct NotEnoughArguments {
    pub name: String,
    pub min: usize,
    pub actual: usize,
}
#[derive(Debug, Error)]
#[error("Wrong argument {num} type.")]
pub struct WrongArgumentType {
    pub num: usize,
}

fn apply_alias_args(mut filter: filter::Filter, args: &filter::Args) -> Result<filter::Filter> {
    lazy_static! {
        static ref RE_ARG: Regex = Regex::new(r"\{(\d+)\}").unwrap();
    }
    let mut new_args = Vec::with_capacity(filter.args.len());
    for arg in filter.args.into_iter() {
        match arg {
            FilterArg::Img(filter) => {
                new_args.push(FilterArg::Img(apply_alias_args(filter, args)?));
            }
            FilterArg::String(ref s) => {
                let mut new_arg = String::new();
                let mut last_pos = 0;
                for m in RE_ARG.captures_iter(s) {
                    new_arg.push_str(&s[last_pos..m.get(0).unwrap().start()]);
                    last_pos = m.get(0).unwrap().end();
                    let arg_num: usize = m[1].parse().unwrap();
                    if arg_num >= args.len() {
                        Err(NotEnoughArguments {
                            name: filter.name.clone(),
                            min: arg_num + 1,
                            actual: args.len(),
                        })?
                    }
                    if let Some(value) = args.get(arg_num).unwrap().as_string() {
                        new_arg.push_str(value.as_str());
                    } else {
                        Err(WrongArgumentType { num: arg_num + 1 })?
                    }
                }
                new_arg.push_str(&s[last_pos..]);
                new_args.push(FilterArg::String(new_arg));
            }
            _ => new_args.push(arg),
        }
    }
    filter.args = new_args;
    Ok(filter)
}

fn apply_filter_aliases(mut filter: filter::Filter) -> Result<filter::Filter> {
    debug!(
        "Appling aliases {:?}: args len {}",
        filter.name,
        filter.args.len()
    );
    let mut new_args = Vec::with_capacity(filter.args.len());
    for arg in filter.args.into_iter() {
        match arg {
            FilterArg::Img(filter) => new_args.push(FilterArg::Img(apply_filter_aliases(filter)?)),
            _ => new_args.push(arg),
        }
    }
    filter.args = new_args;
    if let Some(value) = CONFIG.aliases.get(&filter.name) {
        let new_filter = apply_alias_args(url::parse(value)?, &filter.args)?;
        Ok(new_filter)
    } else if CONFIG.allow_builtin_filters {
        Ok(filter)
    } else {
        Err(filter::UnknownFilter(filter.name))?
    }
}

type FilterResult = (
    HashMap<String, String>,
    Result<Box<dyn filter::FilterResult>>,
);

#[allow(non_fmt_panics)]
#[tracing::instrument]
pub fn exec_from_url<'a>(url: &str) -> BoxFuture<'a, FilterResult> {
    let filter = match url::parse(url) {
        Ok(res) => res,
        Err(e) => {
            return async move {
                debug!("Failed to parse url: {e}");
                (HashMap::with_capacity(1), Err(e.into()))
            }
            .in_current_span()
            .boxed()
        }
    };
    let filter = match apply_filter_aliases(filter) {
        Ok(filter) => filter,
        Err(e) => {
            return async move {
                debug!("Failed to apply filter aliases: {e}");
                (HashMap::with_capacity(1), Err(e))
            }
            .in_current_span()
            .boxed()
        }
    };
    let context = Arc::new(filter::ContextData::new(
        &FILTERS,
        &CONFIG.log_filters_header,
        HardwareLocker::new(CONFIG.connections_limit(), USED_CPU.clone()),
    ));
    async move {
        let result = filter::exec_filter(context.clone(), &filter)
            .in_current_span()
            .await;
        // At this point, context shouldn't have any references left, so we retrieve it's contents.
        // If it does, it's probably okay to crash – the other alternative would be a performance
        // regression due to clone().
        let context = Arc::try_unwrap(context).unwrap_or_else(|e| panic!("{e:?}"));
        (context._response_headers.into_inner().unwrap(), result)
    }
    .in_current_span()
    .boxed()
}

fn handle_failure(err: anyhow::Error) -> Result<Response, Infallible> {
    use imaginator_common::img::ImageMagickError;
    use imaginator_common::CensoredError;
    let response = hyper::Response::builder();

    fn censored_with_code(
        e: &impl CensoredError,
        code: impl Into<StatusCode>,
    ) -> Result<Response, Infallible> {
        Ok(hyper::Response::builder()
            .status(code.into())
            .body(e.censored_error().into())
            .expect("unexpected error during failure message construction"))
    }

    if let Some(error_response) = err.downcast_ref::<filter::ErrorResponse>() {
        let error_response: &dyn filter::FilterResult = error_response;
        Ok(response
            .header(
                "Content-Type",
                error_response
                    .content_type()
                    .unwrap_or("text/plain".to_owned()),
            )
            .status(error_response.status_code())
            .body(error_response.content().unwrap().into())
            .unwrap())
    } else if let Some(e @ imaginator_common::UnknownFilter(_)) = err.downcast_ref() {
        censored_with_code(e, StatusCode::PRECONDITION_FAILED)
    } else if let Some(ImageMagickError::UnknownImageFormat(
        e @ imaginator_common::UnknownImageFormat { .. },
    )) = err.downcast_ref()
    {
        censored_with_code(e, StatusCode::UNSUPPORTED_MEDIA_TYPE)
    } else if let Some(e @ imaginator_common::UnknownImageFormat { .. }) = err.downcast_ref() {
        censored_with_code(e, StatusCode::UNSUPPORTED_MEDIA_TYPE)
    } else if let Some(ImageMagickError::Hardware(e @ imaginator_common::CpuNotAvailable)) =
        err.downcast_ref()
    {
        censored_with_code(e, StatusCode::TOO_MANY_REQUESTS)
    } else {
        let code = err.downcast_ref::<url::UrlParseError>().map_or_else(
            || StatusCode::INTERNAL_SERVER_ERROR,
            |_| StatusCode::BAD_REQUEST,
        );

        Ok(response
            .header("Content-Type", "text/plain")
            .status(code)
            .body(code.to_string().into())
            .unwrap())
    }
}

#[tracing::instrument(skip(req))]
pub async fn app(
    mut req: hyper::Request<hyper::Body>,
) -> Result<hyper::Response<hyper::Body>, Infallible> {
    req.extensions_mut().insert(ImaginatorExtension {
        version: env!("CARGO_PKG_VERSION"),
    });

    let correlation_context = req.extensions().get::<CorrelationContext>().unwrap();

    let mut url = req.uri().path()[1..].to_owned();
    if let Some(query) = req.uri().query() {
        url.push('?');
        url.push_str(query);
    }
    let log_req = format!(
        "- - - [{}] \"{} {} {:?}\"",
        Utc::now().format("%d/%b/%Y:%H:%M:%S %z"),
        req.method(),
        req.uri(),
        req.version()
    );
    if req.method() != hyper::Method::GET {
        return Ok(hyper::Response::builder()
            .status(StatusCode::METHOD_NOT_ALLOWED)
            .body(hyper::Body::empty())
            .unwrap());
    }

    for (name, plugin) in plugins_ref().iter() {
        trace!("Checking plugin {name:?} middleware");
        if let Some(middleware) = plugin.middleware {
            trace!("Running plugin {name:?} middleware");
            match middleware(&req).await {
                Ok(Some(response)) => return Ok(response),
                Ok(None) => {}
                Err(e) => {
                    debug!("Middleware failure at uri {}: {}", req.uri(), e);
                    return handle_failure(e);
                }
            }
        }
    }

    let (headers, result) = exec_from_url(&url).await;
    let result = result
        .and_then(|img| {
            let content_type = img.content_type()?;
            let body = img.content()?;
            let mut response = hyper::Response::builder()
                .header("Content-Length", body.len() as u64)
                .header("Content-Type", content_type);
            for (name, value) in headers.iter() {
                response = response.header(name, value.as_str());
            }
            Ok(response.body(hyper::Body::from(body)).unwrap())
        })
        .or_else(|error| {
            let message = format!("Failure {error:#?}", error = error);
            warn!(
                message,
                "tags.Correlation-Id" = correlation_context.correlation_id(),
                "tags.Correlation-Origin" = correlation_context.correlation_origin()
            );
            handle_failure(error)
        });
    match result {
        Ok(response) => Ok(response),
        Err(response) => {
            error!("{} {}", log_req, 500);
            Err(response)
        }
    }
}
