use crate::imaginator::cfg::CONFIG as PLUGIN_CONFIG;

use std::any::Any;
use std::collections::HashMap;
use std::env;
use std::fs::File;

include!(concat!(env!("OUT_DIR"), "/cfg_plugins.rs"));

#[derive(Default, Copy, Clone, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum LimitConnectionConfig {
    #[default]
    Disabled,
    MaxThreads,
    MaxCores,
    Fixed(usize),
}

#[derive(Serialize, Deserialize)]
pub struct Config {
    pub secret: Option<String>,
    pub aliases: HashMap<String, String>,
    #[serde(flatten)]
    pub filters: Filters,
    pub allow_builtin_filters: bool,
    pub log_filters_header: Option<String>,
    /// Example:
    ///
    /// ```yaml
    /// limit_connections:
    ///     fixed: 6
    /// ```
    ///
    /// ```yaml
    /// limit_connections: disabled
    /// ```
    ///
    /// ```yaml
    /// limit_connections: max_threads
    /// ```
    ///
    /// ```yaml
    /// limit_connections: max_cores
    /// ```
    #[serde(default)]
    #[serde(with = "serde_yaml::with::singleton_map")]
    pub limit_connections: LimitConnectionConfig,
}

impl Config {
    pub fn connections_limit(&self) -> Option<usize> {
        Some(match &self.limit_connections {
            LimitConnectionConfig::Disabled => return None,
            LimitConnectionConfig::MaxCores => num_cpus::get_physical(),
            LimitConnectionConfig::MaxThreads => num_cpus::get(),
            LimitConnectionConfig::Fixed(n) => *n,
        })
    }
}

lazy_static! {
    pub static ref CONFIG: Box<Config> = {
        let config_path = std::env::var("CONFIG_PATH")
            .ok()
            .or_else(|| env::args().nth(1))
            .expect("The first argument must be a path to the config file.");
        let config: Box<Config> =
            Box::new(serde_yaml::from_reader(File::open(config_path).unwrap()).unwrap());
        let mut plugin_config = HashMap::with_capacity(1_000);
        config.filters.init_plugin_config(&mut plugin_config);
        unsafe {
            PLUGIN_CONFIG = Some(plugin_config);
        }
        config
    };
}
