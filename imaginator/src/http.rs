use crate::app::app;

use axum::extract::connect_info::Connected;
use axum::extract::ConnectInfo;
use axum::extract::MatchedPath;
use hyper::server::conn::AddrStream;
use hyper::service::service_fn;
use imaginator::metrics::{HTTP_COUNTER, HTTP_DURATION};
use std::net::SocketAddr;
use std::time::Duration;
use std::time::Instant;
use tower::ServiceBuilder;
use tower_http::classify::ServerErrorsFailureClass;
use tracing_ecs::HTTP_RESPONSE_CONTENT_LENGTH;
use tracing_ecs::HTTP_RESPONSE_LATENCY;
use tracing_ecs::HTTP_RESPONSE_MIME_TYPE;
use tracing_ecs::HTTP_RESPONSE_STATUS_CODE;

use opentelemetry::global;
use tracing_opentelemetry::OpenTelemetrySpanExt;

use axum::middleware::{self, map_request, Next};

use axum::http::{HeaderValue, Request};

use axum::http::header;
use axum::response::{IntoResponse, Response};
use tokio::signal;
use tower_http::trace::TraceLayer;

use tracing::{info_span, Span};

use super::correlation_context::CorrelationContext;

async fn add_correlation_context_extension<B>(mut request: Request<B>) -> Request<B> {
    let ctx = CorrelationContext::from_request(&request);
    request.extensions_mut().insert(ctx);

    request
}

async fn correlation_context<B>(req: Request<B>, next: Next<B>) -> impl IntoResponse {
    // there should be extension here because of order of calling layers, but just in case
    let correlation_context = if req.extensions().get::<CorrelationContext>().is_some() {
        req.extensions()
            .get::<CorrelationContext>()
            .unwrap()
            .clone()
    } else {
        // we must be in request at this point, this is serving purpose of null object
        CorrelationContext::from_request(&req)
    };

    let mut response = next.run(req).await;

    let headers = response.headers_mut();

    headers.insert(
        "X-Correlation-Id",
        HeaderValue::from_str(correlation_context.correlation_id()).unwrap(),
    );
    headers.insert(
        "X-Correlation-Origin",
        HeaderValue::from_str(correlation_context.correlation_origin()).unwrap(),
    );
    headers.insert(
        "X-Causation-Id",
        HeaderValue::from_str(correlation_context.causation_id()).unwrap(),
    );
    headers.insert(
        "X-Causation-Origin",
        HeaderValue::from_str(correlation_context.causation_origin()).unwrap(),
    );
    headers.insert(
        "X-Request-Id",
        HeaderValue::from_str(correlation_context.request_id()).unwrap(),
    );

    response
}

pub fn main_app(server_socket_addr: SocketAddr) -> axum::Router {
    axum::Router::new()
        .route_service("/*imaginator_wildcard", service_fn(app))
        .route_layer(middleware::from_fn(correlation_context))
        .route_layer(middleware::from_fn(track_metrics))
        .layer(
            ServiceBuilder::new()
                .layer(map_request(add_correlation_context_extension))
                .layer(
                    TraceLayer::new_for_http()
                        .make_span_with(move |request: &http::Request<_>| {
                            let ctx = request.extensions().get::<CorrelationContext>().unwrap();
                            let parent_context = global::get_text_map_propagator(|propagator| {
                                propagator.extract(&MyHeaderExtractor(request.headers()))
                            });

                            let remote_addr = request
                              .extensions()
                              .get::<ConnectInfo<MyConnectInfo>>();

                            // If route is not found, we will not have matched
                            let matched_path = request.extensions().get::<MatchedPath>().map(|mp| mp.as_str().to_string()).unwrap_or_default();
                            let server_addres = request
                                .headers()
                                .get("x-forwarded-host")
                                .and_then(|h| h.to_str().ok())
                                .or_else(|| request.headers().get(":authority").and_then(|h| h.to_str().ok()))
                                .or_else(|| request.headers().get("host").and_then(|h| h.to_str().ok()))
                                .unwrap_or_default()
                                .to_string();
                            // semantic names taken from https://opentelemetry.io/docs/specs/semconv/http/http-spans/
                            let http_span = info_span!(
                                "http_request",
                                "otel.name" = format!("{method} {path}", method = request.method(), path = matched_path),
                                "otel.kind" = "Server",
                                "otel.status_code" = tracing::field::Empty,
                                "http.request.method" = ?request.method(),
                                "http.response.status_code" = tracing::field::Empty,
                                "http.response.latency" = tracing::field::Empty,
                                "network.protocol.version" = ?request.version(),
                                "network.peer.address" = ?remote_addr.map(|addr| addr.remote_addr.ip().to_string()).unwrap_or_default(),
                                "network.peer.port" = ?remote_addr.map(|addr| addr.remote_addr.port()).unwrap_or_default(),
                                "network.protocol.name" = "http",
                                "network.local.address" = ?remote_addr.map(|addr| addr.local_addr.map(|addr| addr.ip().to_string())).unwrap_or_default(),
                                "network.local.port" = ?remote_addr.map(|addr| addr.local_addr.map(|addr| addr.port())).unwrap_or_default(), 
                                "url.path" = request.uri().path(),
                                "url.query" = ?request.uri().query(),
                                "server.address" = server_addres,
                                "server.port" = server_socket_addr.port(),
                                // sadly it's imposible to calculate this
                                // https://github.com/tokio-rs/axum/discussions/858
                                "url.scheme" = "http",                                
                                "http.route" = matched_path,
                                "client.address" = remote_addr.map(|addr| addr.remote_addr.ip().to_string()).unwrap_or_default(),
                                "client.port" = remote_addr.map(|addr| addr.remote_addr.port()).unwrap_or_default(),
                                "user_agent.original" = ?request.headers().get("user-agent"),
                                correlation_id = ctx.correlation_id(),
                                correlation_origin = ctx.correlation_origin(),
                                causation_id = ctx.causation_id(),
                                causation_origin = ctx.causation_origin(),
                                request_id = ctx.request_id(),
                            );
                            http_span.set_parent(parent_context);
                            http_span
                        })
                        .on_response(|response: &Response, latency: Duration, span: &Span| {
                            span.record(HTTP_RESPONSE_STATUS_CODE, tracing::field::debug(response.status()));
                            span.record(HTTP_RESPONSE_LATENCY, tracing::field::debug(latency));
                            let headers = response.headers();
                            let response_content_type = headers.get(header::CONTENT_TYPE);
                            if response_content_type.is_some() {
                                span.record(
                                    HTTP_RESPONSE_MIME_TYPE,
                                    tracing::field::debug(response_content_type.to_owned()),
                                );
                            }

                            let response_content_length = headers.get(header::CONTENT_LENGTH);
                            if response_content_length.is_some() {
                                span.record(
                                    HTTP_RESPONSE_CONTENT_LENGTH,
                                    tracing::field::debug(response_content_length.to_owned()),
                                );
                            }

                            if !response.status().is_success() {
                                tracing::info!("failed response");
                            } else {
                                tracing::debug!("response");
                            }
                        })
                        .on_failure(
                            |_error: ServerErrorsFailureClass, _latency: Duration, _span: &Span| {
                                // do nothing since we already log the error in the route
                                // search for "Error while routing" in this file
                            },
                        ),
                ),
        )
}

pub async fn server() {
    let port = std::env::var("HTTP_PORT")
        .ok()
        .and_then(|s| s.parse().ok())
        .unwrap_or(3000);
    let addr: SocketAddr = format!("[::]:{port}").parse().unwrap();
    let routes = main_app(addr);
    tracing::info!("Main server listening on {}", addr);

    axum::Server::bind(&addr)
        .serve(routes.into_make_service_with_connect_info::<MyConnectInfo>())
        .with_graceful_shutdown(shutdown_signal())
        .await
        .unwrap();
}

async fn track_metrics<B>(req: Request<B>, next: Next<B>) -> impl IntoResponse {
    let start = Instant::now();

    let response = next.run(req).await;

    {
        let latency = start.elapsed().as_secs_f64();
        let status = response.status().as_u16().to_string();
        let labels = [status.as_str()];
        HTTP_COUNTER.inc();
        HTTP_DURATION.with_label_values(&labels).observe(latency);
    }

    response
}

async fn shutdown_signal() {
    let ctrl_c = async {
        signal::ctrl_c()
            .await
            .expect("failed to install Ctrl+C handler");
    };

    #[cfg(unix)]
    let terminate = async {
        signal::unix::signal(signal::unix::SignalKind::terminate())
            .expect("failed to install signal handler")
            .recv()
            .await;
    };

    #[cfg(not(unix))]
    let terminate = std::future::pending::<()>();

    tokio::select! {
        _ = ctrl_c => {},
        _ = terminate => {},
    }

    tracing::info!("signal received, starting graceful shutdown");
}

// This has to be implemented by hand because opentelemtry relies on old http
pub struct MyHeaderExtractor<'a>(pub &'a http::HeaderMap);

impl<'a> opentelemetry::propagation::Extractor for MyHeaderExtractor<'a> {
    /// Get a value for a key from the HeaderMap.  If the value is not valid ASCII, returns None.
    fn get(&self, key: &str) -> Option<&str> {
        self.0.get(key).and_then(|value| value.to_str().ok())
    }

    /// Collect all the keys from the HeaderMap.
    fn keys(&self) -> Vec<&str> {
        self.0
            .keys()
            .map(|value| value.as_str())
            .collect::<Vec<_>>()
    }
}

#[derive(Clone, Debug)]
struct MyConnectInfo {
    /// The local address that this stream is bound to.
    local_addr: Option<SocketAddr>,
    /// The remote address that this stream is bound to.
    remote_addr: SocketAddr,
}

impl Connected<&AddrStream> for MyConnectInfo {
    fn connect_info(target: &AddrStream) -> Self {
        Self {
            local_addr: Some(target.local_addr()),
            remote_addr: target.remote_addr(),
        }
    }
}
