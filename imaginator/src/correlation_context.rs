use uuid::Uuid;

#[derive(Clone, Debug)]
pub struct CorrelationContext {
    request_id: String,
    correlation_id: String,
    correlation_origin: String,
    causation_id: String,
    causation_origin: String,
}

impl CorrelationContext {
    pub fn new(
        correlation_id: Option<String>,
        correlation_origin: Option<String>,
        causation_id: Option<String>,
        request_id: Option<String>,
    ) -> Self {
        CorrelationContext {
            request_id: match request_id {
                Some(id) => id,
                _ => Uuid::new_v4().to_string(),
            },
            correlation_id: match correlation_id {
                Some(id) => id,
                _ => Uuid::new_v4().to_string(),
            },
            correlation_origin: match correlation_origin {
                Some(origin) => origin,
                _ => "imaginator".to_string(),
            },
            causation_id: match causation_id {
                Some(id) => id,
                _ => Uuid::new_v4().to_string(),
            },
            causation_origin: "imaginator".to_string(),
        }
    }

    pub fn from_request<T>(req: &http::Request<T>) -> Self {
        let headers = req.headers();
        CorrelationContext::new(
            headers
                .get("x-correlation-id")
                .map(|h| String::from_utf8_lossy(h.as_bytes()).to_string()),
            headers
                .get("x-correlation-origin")
                .map(|h| String::from_utf8_lossy(h.as_bytes()).to_string()),
            headers
                .get("x-causation-id")
                .map(|h| String::from_utf8_lossy(h.as_bytes()).to_string()),
            None,
        )
    }

    pub fn request_id(&self) -> &str {
        &self.request_id
    }

    pub fn correlation_id(&self) -> &str {
        &self.correlation_id
    }

    pub fn correlation_origin(&self) -> &str {
        &self.correlation_origin
    }

    pub fn causation_id(&self) -> &str {
        &self.causation_id
    }

    pub fn causation_origin(&self) -> &str {
        &self.causation_origin
    }
}
