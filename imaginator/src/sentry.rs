use imaginator::systemd_creds::ReadCredentialError;
use imaginator_common::systemd_creds::ReadCredential;
use sentry::ClientInitGuard;

/// Try to connect to sentry, if it fails due to missing ENV VAR, just log it and continue
pub fn init(reader: impl ReadCredential) -> Option<ClientInitGuard> {
    let dsn = match reader.read_credential("sentry_dsn") {
        Ok(dsn) => Some(dsn),
        Err(ReadCredentialError::CredentialDirectoryNotSet) => {
            tracing::warn!("CREDS_DIR not set, will check for SENTRY_DSN ENV VAR");
            None
        }
        Err(ReadCredentialError::CredentialDoesNotExist(path)) => {
            tracing::warn!(
                "sentry_dsn credential does not exist at path: {}",
                path.to_string_lossy()
            );
            None
        }
        Err(e) => {
            panic!("Other error reading sentry_dsn: {}", e);
        }
    }
    .or(std::env::var("SENTRY_DSN").ok());

    let Some(dsn) = dsn else {
        tracing::warn!("Sentry not set up correctly, not logging to sentry");
        return None;
    };

    let guard = sentry::init((
        dsn,
        sentry::ClientOptions {
            environment: Some(
                std::env::var("ENV")
                    .unwrap_or_else(|_| "development".to_string())
                    .into(),
            ),
            release: sentry::release_name!(),
            sample_rate: 1.0,
            attach_stacktrace: true,
            ..Default::default()
        },
    ));

    Some(guard)
}
