use axum::Router;
use futures::{future::join_all, FutureExt};
use http::Request;
use hyper::Body;
use std::{path::Path, time::Duration};
use tower::{timeout::Timeout, Service, ServiceExt};

use crate::common::fake_socket_addr;

mod common;

/// This test ensure aborted connection does not affect counter in any way and prevents situation
/// when we have atomic counter which does not decrease on panic.
#[tokio::test(flavor = "multi_thread")]
async fn handle_abort() {
    let pwd = Path::new(&std::env::current_dir().unwrap()).to_owned();
    let server = httpmock::MockServer::start();
    common::setup_test_env(&pwd, &server, config_file_content);

    let _mock = server.mock(|when, then| {
        when.path("/img.jpg");
        then.body(include_bytes!("./large.jpg"));
    });

    imaginator::init();
    let app = imaginator::http::main_app(fake_socket_addr());

    // first 5 request should be aborted due to timeout
    let results = run_requests(5, app.clone(), Some(Duration::from_millis(1))).await;
    assert!(results.iter().all(|r| *r == 0)); // all timeouts

    // next 2 requests should be successful
    // next 3 requests should be failed due to CPU limit
    let mut results = run_requests(5, app, None).await;
    results.sort();

    assert_eq!(results, vec![200, 200, 429, 429, 429]);
}

async fn run_requests(req_count: usize, router: Router, timeout: Option<Duration>) -> Vec<u16> {
    let tasks = (0..req_count)
        .map(|_| {
            let request = Request::builder()
                .uri("/marketing_celebrity_785w_785h(img.jpg)")
                .body(Body::empty())
                .unwrap();

            let mut router = router.clone();
            async move {
                let service = router.ready().await.unwrap();
                let f = if let Some(timeout) = timeout {
                    tokio::spawn(Timeout::new(service, timeout).call(request).map(
                        |res| match res {
                            Ok(res) => panic!("Request should fail with timeout: {:#?}", res),
                            Err(_) => 0,
                        },
                    ))
                } else {
                    tokio::spawn(service.call(request).map(|res| match res {
                        Ok(res) => res.status().as_u16(),
                        Err(err) => panic!("Request failed: {:#?}", err),
                    }))
                };
                f.await.unwrap()
            }
        })
        .collect::<Vec<_>>();
    let mut v = join_all(tasks).await;
    v.sort();
    v
}

fn config_file_content(server_mock: &httpmock::MockServer) -> String {
    format!("
aliases:
  marketing_celebrity_785w_785h: resize(extend(fit-in(download(modivo:{{0}}),785,785),(1w-785px)/2,(1h-785px)/2,785-(785-1w)/2,785-(785-1h)/2),4000,4000)
caches:
  marketing:
    size: 1Mi
    dir: /tmp/imaginator/marketing
domains:
  modivo: {}/
  modivo2: https://modivo-img.s3.eu-central-1.amazonaws.com/
log_filters_header: X-Route
s3_caches: {{}}
allow_builtin_filters: true
accept_invalid_tls_certs: false
global_s3_access_key: hadyh87ayd
limit_connections:
  fixed: 2
", server_mock.base_url())
}
