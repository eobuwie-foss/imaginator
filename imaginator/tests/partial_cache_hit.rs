use tempfile::tempdir;

use http::Request;
use hyper::Body;
use tower::ServiceExt;

use crate::common::fake_socket_addr;

mod common;

#[tokio::test(flavor = "multi_thread")]

async fn run() -> anyhow::Result<()> {
    let cache_dir = tempdir()?;
    let conf_dir = tempdir()?;
    let conf_path = conf_dir.path().join("imaginator.conf");

    let server = httpmock::MockServer::start();

    let _mock = server.mock(|when, then| {
        when.path("/pexels-skylar-kang-6045293.jpg");
        then.body(include_bytes!("./large.jpg"));
    });

    let dir = cache_dir.path().to_str().unwrap();

    let input = format!(
        r#"
{{
  "accept_invalid_tls_certs": false,
  "aliases": {{
    "puredownload": "download(minio:{{0}}):cache(download):format({{1}})"
  }},
  "allow_builtin_filters": false,
  "caches": {{
    "download": {{
      "dir": "{dir}/download",
      "read_only": false,
      "size": "1G"
    }},
    "icon": {{
      "dir": "{dir}/icon",
      "read_only": false,
      "size": "1G"
    }}
  }},
  "domains": {{
    "minio": "{}/"
  }},
  "logLevel": "INFO",
  "log_filters_header": "filter",
  "loggerType": "tracing",
  "s3_caches": {{}},
  "sentry_dsn": ""
}}"#,
        &server.base_url()
    );
    std::fs::write(conf_path.clone(), input).unwrap();

    std::env::set_var("CONFIG_PATH", conf_path);
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "OFF");
    }

    imaginator::init();
    let app = imaginator::http::main_app(fake_socket_addr());

    let res = app
        .clone()
        .oneshot(
            Request::builder()
                .uri("/puredownload(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(res.status(), 200);
    let cache_header = res.headers().get("filter").unwrap().to_str().unwrap();
    assert!(cache_header.contains("cache_miss(download)"));

    let res = app
        .clone()
        .oneshot(
            Request::builder()
                .uri("/puredownload(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg")
                .body(Body::empty())
                .unwrap(),
        )
        .await
        .unwrap();

    assert_eq!(res.status(), 200);
    let cache_header = res.headers().get("filter").unwrap().to_str().unwrap();
    assert!(cache_header.contains("cache_hit(download)"));

    Ok(())
}
