use std::net::SocketAddr;
use std::path::Path;

#[allow(dead_code)] // code is only used in tests
pub fn setup_test_env(
    pwd: &Path,
    server_mock: &httpmock::MockServer,
    config_file_content: impl Fn(&httpmock::MockServer) -> String,
) {
    let tests_path = pwd.join("tests");
    let test_assets = tests_path.join("assets");
    let config_path = test_assets.join("no_limit.yml");

    std::env::set_var("CONFIG_PATH", config_path.to_str().unwrap());
    std::env::remove_var("CREDENTIALS_DIRECTORY");

    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "NONE");
    }

    std::fs::create_dir_all(&test_assets).ok();

    std::fs::write(&config_path, config_file_content(server_mock)).unwrap();
}

pub fn fake_socket_addr() -> SocketAddr {
    SocketAddr::from(([127, 0, 0, 1], 8080))
}
