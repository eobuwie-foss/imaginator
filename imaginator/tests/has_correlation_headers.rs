use axum::Router;
use common::fake_socket_addr;
use http::Request;
use hyper::Body;
use std::path::Path;
use tower::ServiceExt;

mod common;

#[tokio::test(flavor = "multi_thread")]
async fn has_correlation_headers() {
    let pwd = Path::new(&std::env::current_dir().unwrap()).to_owned();
    let server = httpmock::MockServer::start();
    common::setup_test_env(&pwd, &server, config_file_content);
    let _mock = server.mock(|when, then| {
        when.path("/img.jpg");
        then.body(include_bytes!("./large.jpg"));
    });

    imaginator::init();
    let app = imaginator::http::main_app(fake_socket_addr());

    passing_headers_test(app.clone()).await;
    no_passed_headers_test(app).await;
}

async fn no_passed_headers_test(app: Router) {
    let request = Request::builder()
        .uri("/marketing_celebrity_785w_785h(img.jpg)")
        .body(Body::empty())
        .unwrap();

    let response = app.oneshot(request).await.unwrap();

    let headers = response.headers();

    assert!(headers.get("X-Correlation-Id").is_some());
    assert!(headers.get("X-Correlation-Origin").is_some());
    assert!(headers.get("X-Causation-Id").is_some());
    assert_eq!(headers.get("X-Causation-Origin").unwrap(), "imaginator");
}

async fn passing_headers_test(app: Router) {
    let request = Request::builder()
        .uri("/marketing_celebrity_785w_785h(img.jpg)")
        .header("X-Correlation-Id", "some-random-correlation-id-value")
        .header("X-Correlation-Origin", "tokio_runtime")
        .header("X-Causation-Id", "has-correlation-header-test")
        .body(Body::empty())
        .unwrap();

    let response = app.oneshot(request).await.unwrap();

    let headers = response.headers();

    assert_eq!(
        headers.get("X-Correlation-Id").unwrap(),
        "some-random-correlation-id-value"
    );
    assert_eq!(
        headers.get("X-Correlation-Origin").unwrap(),
        "tokio_runtime"
    );
    assert_eq!(
        headers.get("X-Causation-Id").unwrap(),
        "has-correlation-header-test"
    );
    assert_eq!(headers.get("X-Causation-Origin").unwrap(), "imaginator");
}

fn config_file_content(server_mock: &httpmock::MockServer) -> String {
    format!("
aliases:
  marketing_celebrity_785w_785h: resize(extend(fit-in(download(modivo:{{0}}),785,785),(1w-785px)/2,(1h-785px)/2,785-(785-1w)/2,785-(785-1h)/2),4000,4000)
caches:
  marketing:
    size: 1Mi
    dir: /tmp/imaginator/marketing
domains:
  modivo: {}/
  modivo2: https://modivo-img.s3.eu-central-1.amazonaws.com/
log_filters_header: X-Route
s3_caches: {{}}
allow_builtin_filters: true
accept_invalid_tls_certs: false
global_s3_access_key: hadyh87ayd
limit_connections:
  fixed: 5
", server_mock.base_url())
}
