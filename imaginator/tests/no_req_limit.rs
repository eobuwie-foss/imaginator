use std::path::Path;

use axum::Router;
use futures::{future::join_all, FutureExt};
use http::Request;
use hyper::Body;
use tower::{Service, ServiceExt};

use crate::common::fake_socket_addr;

static REQ_COUNT: usize = 20;

fn expected_result() -> Vec<u16> {
    (0..REQ_COUNT).map(|_| 200).collect::<Vec<_>>()
}

mod common;

#[tokio::test(flavor = "multi_thread")]
async fn no_req_limit() {
    let pwd = Path::new(&std::env::current_dir().unwrap()).to_owned();
    let server = httpmock::MockServer::start();
    common::setup_test_env(&pwd, &server, config_file_content);

    let _mock = server.mock(|when, then| {
        when.path("/img.jpg");
        then.body(include_bytes!("./large.jpg"));
    });

    imaginator::init();
    let app = imaginator::http::main_app(fake_socket_addr());

    let results = run_requests(app.clone()).await;
    let expected = expected_result();
    assert_eq!(results, expected);
}

async fn run_requests(app: Router) -> Vec<u16> {
    let tasks = (0..REQ_COUNT)
        .map(|_n| {
            let request = Request::builder()
                .uri("/marketing_celebrity_785w_785h(img.jpg)")
                .body(Body::empty())
                .unwrap();

            let mut router = app.clone();
            async move {
                let service = router.ready().await.unwrap();
                let f = tokio::spawn(service.call(request).map(|res| match res {
                    Ok(res) => res.status().as_u16(),
                    Err(err) => panic!("Request failed: {:#?}", err),
                }));
                f.await.unwrap()
            }
        })
        .collect::<Vec<_>>();
    join_all(tasks).await
}

fn config_file_content(server_mock: &httpmock::MockServer) -> String {
    format!("
aliases:
  marketing_celebrity_785w_785h: extend(fit-in(cache(download(modivo:{{0}}),modivo_static),785,785),(1w-785px)/2,(1h-785px)/2,785-(785-1w)/2,785-(785-1h)/2)
caches:
  marketing:
    size: 1Mi
    dir: /tmp/imaginator/marketing
domains:
  modivo: {}/
  modivo2: https://modivo-img.s3.eu-central-1.amazonaws.com/
log_filters_header: X-Route
s3_caches: {{}}
allow_builtin_filters: true
accept_invalid_tls_certs: false
global_s3_access_key: hadyh87ayd
# limit_connections:
#   fixed: 2
", server_mock.base_url())
}
