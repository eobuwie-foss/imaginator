use common::fake_socket_addr;
use futures::{future::join_all, FutureExt};
use std::path::Path;

use axum::Router;
use http::Request;
use hyper::Body;
use tower::{Service, ServiceExt};

mod common;

fn expected_result(limit: usize, req_count: usize) -> Vec<u16> {
    (0..limit)
        .map(|_| 200)
        .chain((0..(req_count - limit)).map(|_| 429))
        .collect()
}

#[tokio::test(flavor = "multi_thread")]
async fn http_traffic_over_limit() {
    let pwd = Path::new(&std::env::current_dir().unwrap()).to_owned();
    let server = httpmock::MockServer::start();
    common::setup_test_env(&pwd, &server, config_file_content);
    let _mock = server.mock(|when, then| {
        when.path("/img.jpg");
        then.body(include_bytes!("./large.jpg"));
    });

    imaginator::init();
    let app = imaginator::http::main_app(fake_socket_addr());

    for req_count in 3..8 {
        assert_current_traffic_http_codes(app.clone(), req_count).await;
    }
}

/// Assert all http codes above limit are 429 (Cpu not available)
async fn assert_current_traffic_http_codes(router: Router, req_count: usize) {
    let results = run_requests(router, req_count).await;
    let expected = expected_result(2, req_count);
    assert_eq!(results, expected);
}

async fn run_requests(router: Router, req_count: usize) -> Vec<u16> {
    let tasks = (0..req_count)
        .map(|_n| {
            let request = Request::builder()
                .uri("/marketing_celebrity_785w_785h(img.jpg)")
                .body(Body::empty())
                .unwrap();
            let mut router = router.clone();
            async move {
                let service = router.ready().await.unwrap();
                let f = tokio::spawn(service.call(request).map(|res| match res {
                    Ok(res) => res.status().as_u16(),
                    Err(err) => panic!("Request failed: {:#?}", err),
                }));
                f.await.unwrap()
            }
        })
        .collect::<Vec<_>>();
    let mut v = join_all(tasks).await;
    v.sort();
    v
}

fn config_file_content(server_mock: &httpmock::MockServer) -> String {
    format!("
aliases:
  marketing_celebrity_785w_785h: resize(extend(fit-in(download(modivo:{{0}}),785,785),(1w-785px)/2,(1h-785px)/2,785-(785-1w)/2,785-(785-1h)/2),4000,4000)
caches:
  marketing:
    size: 1Mi
    dir: /tmp/imaginator/marketing
domains:
  modivo: {}/
  modivo2: https://modivo-img.s3.eu-central-1.amazonaws.com/
log_filters_header: X-Route
s3_caches: {{}}
allow_builtin_filters: true
accept_invalid_tls_certs: false
global_s3_access_key: hadyh87ayd
limit_connections:
  fixed: 2
", server_mock.base_url())
}
