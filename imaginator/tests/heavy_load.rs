use futures::{future::join_all, FutureExt};
use std::{path::Path, time::Duration};

use axum::Router;
use http::Request;
use hyper::Body;
use tower::{timeout::Timeout, Service, ServiceExt};

use crate::common::fake_socket_addr;

static LIMIT: usize = 5;
// this bases on ulimit value therefore 2k is too much
static REQ_COUNT: usize = 256;

mod common;

/// This test ensure aborted connection does not affect counter in any way and prevents situation
/// when we have atomic counter which does not decrease on panic.
#[tokio::test(flavor = "multi_thread")]
async fn heavy_load() {
    let pwd = Path::new(&std::env::current_dir().unwrap()).to_owned();
    let server = httpmock::MockServer::start();
    common::setup_test_env(&pwd, &server, config_file_content);
    let _mock = server.mock(|when, then| {
        when.path("/img.jpg");
        then.body(include_bytes!("./large.jpg"));
    });

    imaginator::init();
    let app = imaginator::http::main_app(fake_socket_addr());

    let results = run_requests(app, REQ_COUNT, None).await;

    let valid_count = results.iter().filter(|n| **n == 200).count();
    let invalid_count = results.iter().filter(|n| **n == 429).count();
    assert_eq!(valid_count + invalid_count, REQ_COUNT);

    assert!(valid_count >= LIMIT);
}

async fn run_requests(app: Router, req_count: usize, timeout: Option<Duration>) -> Vec<u16> {
    let tasks = (0..req_count)
        .map(|_| {
            let request = Request::builder()
                .uri("/marketing_celebrity_785w_785h(img.jpg)")
                .body(Body::empty())
                .unwrap();

            let mut router = app.clone();
            async move {
                let service = router.ready().await.unwrap();
                let f = if let Some(timeout) = timeout {
                    tokio::spawn(Timeout::new(service, timeout).call(request).map(
                        |res| match res {
                            Ok(res) => panic!("Request should fail with timeout: {:#?}", res),
                            Err(_) => 0,
                        },
                    ))
                } else {
                    tokio::spawn(service.call(request).map(|res| match res {
                        Ok(res) => res.status().as_u16(),
                        Err(err) => panic!("Request failed: {:#?}", err),
                    }))
                };
                f.await.unwrap()
            }
        })
        .collect::<Vec<_>>();
    let mut v = join_all(tasks).await;
    v.sort();
    v
}

fn config_file_content(server_mock: &httpmock::MockServer) -> String {
    format!("
aliases:
  marketing_celebrity_785w_785h: resize(extend(fit-in(download(modivo:{{0}}),785,785),(1w-785px)/2,(1h-785px)/2,785-(785-1w)/2,785-(785-1h)/2),4000,4000)
caches:
  marketing:
    size: 1Mi
    dir: /tmp/imaginator/marketing
domains:
  modivo: {}/
  modivo2: https://modivo-img.s3.eu-central-1.amazonaws.com/
log_filters_header: X-Route
s3_caches: {{}}
allow_builtin_filters: true
accept_invalid_tls_certs: false
global_s3_access_key: hadyh87ayd
limit_connections:
  fixed: {LIMIT}
", server_mock.base_url())
}
