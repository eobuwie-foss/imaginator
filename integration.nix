{ system
, pkgs
, makeTest
, self
, imaginator-e2e
, ...
}:
let
  # NixOS module shared between server and client
  sharedModule = {
    # Since it's common for CI not to have $DISPLAY available, we have to explicitly tell the tests "please don't expect any screen available"
    virtualisation.graphics = false;
  };
  filter = name: args: { inherit name args; };
  # Generic filter helpers
  download = prefix: filter "download" [ "${prefix}:{0}" ];
  cache = name: filter "cache" [ name ];
  format = filter "format" [ "{1}" ];
in
makeTest
{
  name = "integration";
  nodes = {
    minio = {
      networking.firewall.allowedTCPPorts = [ 9000 ];
      services.minio = {
        enable = true;
        region = "us-east-1";
      };
    };

    server = {
      systemd.services.secrets = {
        enable = true;
        wantedBy = [ "multi-user.target" ];
        after = [ "imaginator.target" ];
        path = [ pkgs.nix ];
        script = "
    mkdir -p /var/secrets/
    echo 'http://user@127.0.0.1:8000/19' > /var/secrets/sentry_dsn
    echo 'finished setting up credentials'
        ";
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
        };
      };

      systemd.services.imaginator.environment.MAGICK_THREAD_LIMIT = "1";

      virtualisation.memorySize = 4096;
      networking.enableIPv6 = true;
      networking.firewall.allowedTCPPorts = [ 3000 ];
      imports = [ sharedModule self.nixosModules.imaginator ];

      services.imaginator = {
        cleanupIndexes = true;
        enable = true;
        loggerType = "ECS";
        logLevel = "DEBUG";
        log_filters_header = "filter";
        allow_builtin_filters = true;
        caches = {
          download = {
            dir = "/tmp/imaginator/download";
          };
          icon = {
            dir = "/tmp/imaginator/icon";
          };
        };

        domains = {
          minio = "http://minio:9000/images/";
        };

        aliases = {
          icon = [ (download "minio") (cache "download") (filter "resize" [ 0 500 ]) format (cache "icon") ];
          productbg = [
            (download "minio")
            (cache "download")
            (filter "brightness_contrast" [ 1.02 1 ])
            (filter "bg" [ "white" ])
            (filter "fit-in" [ 500 500 ])
            (filter "extend-from-center" [ 500 500 ])
            (filter "compose" [ (filter "color_rect" [ "#FFC0CB" 500 500 ]) "multiply" 0 0 ])
          ];
        };
      };
    };

    client = {
      imports = [ sharedModule ];
      environment.sessionVariables = {
        IMAGINATOR_URL = "http://server:3000";
      };
    };
  };


  # Here are some relevant resources for writing integration tests in nix
  # https://nix.dev/tutorials/integration-testing-using-virtual-machines
  # https://nixos.org/manual/nixos/stable/#sec-nixos-tests
  testScript = ''
    client.start()

    minio.start()

    minio.wait_for_unit("minio.service")
    minio.wait_for_open_port(9000)


    minio.succeed("${pkgs.minio-client}/bin/mc alias set minio http://minio:9000 minioadmin minioadmin")
    minio.succeed("${pkgs.minio-client}/bin/mc admin info minio")

    # create a public bucket with images
    minio.succeed("${pkgs.minio-client}/bin/mc mb minio/images")
    minio.succeed("${pkgs.minio-client}/bin/mc anonymous set download minio/images")

    minio.succeed("${pkgs.minio-client}/bin/mc cp ${examples/s1}/* minio/images")
    minio.succeed("${pkgs.minio-client}/bin/mc ls --recursive minio/images")


    ##
    ## Test the s3 public bucket with images
    ##
    img_response = client.succeed(
        "${pkgs.curl}/bin/curl --write-out '%{http_code}' --silent http://minio:9000/images/pexels-skylar-kang-6045293.jpg -o /tmp/pexels-skylar-kang-6045293.jpg"
    )
    assert img_response == "200", f'expected 200 response code, got {img_response}'

    sha = client.succeed("${pkgs.coreutils}/bin/sha256sum /tmp/pexels-skylar-kang-6045293.jpg | cut -d ' ' -f 1")
    
    client.copy_from_vm("/tmp/pexels-skylar-kang-6045293.jpg", "")


    assert sha.strip() == "2d9e3ea1c9fa6bdfd513ef79ee422bcd58b4dcae256b2d4a213162fe2425dcbe", f'expected sha to be 2d9e3ea1c9fa6bdfd513ef79ee422bcd58b4dcae256b2d4a213162fe2425dcbe, got {sha}'

    # TESTING start with stale cache but remove it before starting the service using cleanupIndexes
    # the cache itself is not removed, only the index
    server.succeed("${pkgs.gnutar}/bin/tar -C /tmp/imaginator -xvf ${e2e/assets/cache.tar.gz}")
    server.succeed("chmod -R 777 /tmp/imaginator/")

    server.start()

    server.wait_for_unit("imaginator.service")
    server.wait_for_open_port(3000)

    client.succeed("${imaginator-e2e}/bin/check_image 'icon(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg' true")

    server.systemctl("stop imaginator.service")
    server.wait_for_closed_port(3000)


    # TESTING start with cache
    server.succeed("rm -rf /tmp/imaginator/*")
    server.succeed("${pkgs.gnutar}/bin/tar -C /tmp/imaginator -xvf ${e2e/assets/cache_2024-07-05.tar.gz}")
    server.succeed("chmod -R 777 /tmp/imaginator/")

    server.succeed("chmod -R 777 /tmp/imaginator/")

    server.systemctl("start imaginator.service")
    server.wait_for_unit("imaginator.service")
    server.wait_for_open_port(3000)

    # check if for all indexes creation date is as expected 2024-07-05 (the date when the cache was created)
    for index in ["icon", "download"]:
      command = f"stat -c %y /tmp/imaginator/{index}/index | cut -d ' ' -f 1"
      date = server.succeed(command).strip()
      assert date == "2024-07-05", f'expected date to be 2024-07-05, got {date}'

    
    client.succeed("${imaginator-e2e}/bin/check_image 'icon(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg' true")






    # RESTARTING with clean cache
    server.systemctl("stop imaginator.service")
    server.wait_for_closed_port(3000)
    server.succeed("rm -rf /tmp/imaginator/{icon,download}")
    server.succeed("mkdir -p /tmp/imaginator/")
    server.systemctl("start imaginator.service")
    server.wait_for_unit("imaginator.service")
    server.wait_for_open_port(3000)

    client.succeed("${imaginator-e2e}/bin/check_image 'icon(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg' false")


    # TEST floats as params and nested filters
    img_response = client.succeed(
      "${pkgs.curl}/bin/curl --write-out '%{http_code}' --silent 'http://server:3000/productbg(pexels-skylar-kang-6045293.jpg)/from_cache.jpg' -o /tmp/result_pink.jpg"
    )

    assert img_response == "200", f'expected 200 response code, got {img_response}'
    client.copy_from_vm("/tmp/result_pink.jpg", "") # copied so we can inspect in manually

    sha = client.succeed("${pkgs.coreutils}/bin/sha256sum /tmp/result_pink.jpg | cut -d ' ' -f 1")
    assert sha.strip() == "c6bc5b493c2d80af546251f6a97328ff998eba9a9eb6eb4861b15bd5a95bdf03", f'expected sha to be c6bc5b493c2d80af546251f6a97328ff998eba9a9eb6eb4861b15bd5a95bdf03, got {sha}'

    # no panics in for missing domain alias
    img_response = client.succeed(
      "${pkgs.curl}/bin/curl --write-out '%{http_code}' --silent -o /dev/null 'http://server:3000/download(pexels-skylar-kang-6045293.jpg)'"
    )

    assert img_response == "400", f'expected 400 response code, got {img_response}'
  '';
}
{
  inherit pkgs;
  inherit (pkgs) system;
}
