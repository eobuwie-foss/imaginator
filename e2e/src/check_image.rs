mod common;

use common::*;
#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let url = get_imaginator_url();

    let image_url = std::env::args().nth(1).expect("missing image url");
    let should_be_in_cache = std::env::args()
        .nth(2)
        .expect("missing cache status")
        .parse::<bool>()
        .expect("cache status must be a bool");

    let client = reqwest::Client::new();

    if should_be_in_cache {
        check_icon_in_cache(&client, &url, &image_url).await?;
    } else {
        check_icon_not_in_cache(&client, &url, &image_url).await?;
        // this should be a cache hit now
        check_icon_in_cache(&client, &url, &image_url).await?;
    }

    Ok(())
}
