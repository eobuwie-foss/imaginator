pub fn get_imaginator_url() -> String {
    let Ok(addr) = std::env::var("IMAGINATOR_URL") else {
        panic!("IMAGINATOR_URL env var not set");
    };
    addr.to_string()
}

pub async fn check_icon_in_cache(
    client: &reqwest::Client,
    url: &str,
    path: &str,
) -> anyhow::Result<()> {
    let res = client.get(format!("{url}/{path}")).send().await?;

    assert_eq!(res.status(), 200);

    let cache_header = res.headers().get("filter").unwrap().to_str().unwrap();

    if !cache_header.contains("cache_hit(icon)") {
        panic!("req to [{path}] should be a cache hit");
    };
    Ok(())
}

pub async fn check_icon_not_in_cache(
    client: &reqwest::Client,
    url: &str,
    path: &str,
) -> anyhow::Result<()> {
    let res = client.get(format!("{url}/{path}")).send().await?;

    assert_eq!(res.status(), 200);
    let cache_header = res.headers().get("filter").unwrap().to_str().unwrap();

    assert!(cache_header.contains("cache_miss(icon)"));
    assert!(cache_header.contains("cache_miss(download)"));
    Ok(())
}
