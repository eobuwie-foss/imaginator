{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-24.05";

    crane = {
      url = "github:ipetkov/crane";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    flake-utils.url = "github:numtide/flake-utils";

    advisory-db = {
      url = "github:rustsec/advisory-db";
      flake = false;
    };

    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
      inputs.rust-analyzer-src.follows = "";
    };

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };
  };

  outputs = { self, nixpkgs, crane, fenix, flake-utils, advisory-db, ... }: {
    nixosModules = {
      imaginator = (import ./module.nix self);
    };
    formatter.x86_64-linux = nixpkgs.legacyPackages.x86_64-linux.nixpkgs-fmt;
  } // flake-utils.lib.eachSystem [ "x86_64-linux" "aarch64-linux" ] (system: # we depend on systemd so we need to specify the system
    let

      pkgs = import nixpkgs {
        inherit system;
      };

      inherit (pkgs) lib;
      craneLib = (crane.mkLib nixpkgs.legacyPackages.${system}).overrideToolchain
        fenix.packages.${system}.stable.toolchain;

      jpgs = path: _type: builtins.match ".*\.jpg" path != null;
      color_profiles = path: _type: builtins.match ".*common/src/img/profiles/.*" path != null;
      template = path: _type: builtins.match ".*cfg_plugins.rs.template" path != null;
      assets = path: _type: builtins.match ".*assets/.*" path != null;


      myCustomFilters = path: type:
        (jpgs path type) || (color_profiles path type) || (assets path type) || (template path type) || (craneLib.filterCargoSources path type);

      src = lib.cleanSourceWith {
        src = ./.; # The original, unfiltered source
        filter = myCustomFilters;
      };

      imaginatorDeps = {
        inherit (craneLib.crateNameFromCargoToml { cargoToml = ./imaginator/Cargo.toml; }) pname version src;
        LIBCLANG_PATH = "${ pkgs.llvmPackages.libclang.lib }/lib";
        buildInputs = [
          pkgs.systemd
          pkgs.stdenv.cc.libc
        ];
        nativeBuildInputs = with pkgs; [ pkg-config imagemagick clang stdenv.cc.libc ];
      };
      commonArgs = {
        inherit src;
        inherit (imaginatorDeps) pname version;
        LIBCLANG_PATH = "${ pkgs.llvmPackages.libclang.lib }/lib";

        buildInputs = [
          pkgs.systemd
          pkgs.stdenv.cc.libc
          pkgs.pkg-config
          pkgs.rustc.llvmPackages.llvm
          pkgs.llvmPackages.libclang
          pkgs.llvmPackages.libcxxClang
          pkgs.clang
          pkgs.openssl # required by httpock only
          pkgs.imagemagick
        ];
        nativeBuildInputs = with pkgs; [ pkg-config imagemagick clang stdenv.cc.libc ];
      };

      cargoArtifacts = craneLib.buildDepsOnly commonArgs // {
        pname = "imaginator-deps";
      };
      imaginator = craneLib.buildPackage (commonArgs // {
        inherit cargoArtifacts;
        cargoExtraArgs = "--bin imaginator";
        pname = "imaginator";
        src = src;
        LIBCLANG_PATH = pkgs.llvmPackages.libclang.lib;
        doCheck = false;
        doNotLinkInheritedArtifacts = true;
      });
      imaginator-update-plugins = craneLib.buildPackage (commonArgs // {
        inherit cargoArtifacts;
        cargoExtraArgs = "--bin update-plugins";
        pname = "update-plugins";
        src = src;
        doCheck = false;
      });

      imaginator-e2e = craneLib.buildPackage (commonArgs // {
        inherit cargoArtifacts;
        cargoExtraArgs = "--package e2e --examples --bins";
        pname = "e2e";
        src = src;
        doCheck = false;
      });

      base = pkgs.dockerTools.buildImage {
        name = "base";
        tag = "latest";
        copyToRoot = pkgs.buildEnv {
          name = "image-root";
          paths = with pkgs; [ coreutils curl dockerTools.binSh dockerTools.usrBinEnv cacert gnutar]; 
          pathsToLink = [ "/bin" "/usr/bin" ];
        };
      };

      dockerImage = pkgs.dockerTools.buildImage {
        name = imaginatorDeps.pname;
        fromImage = base;
        tag = imaginatorDeps.version;
        # gnutar is needed for kubectl cp to work
        copyToRoot = with pkgs; [ imaginator cacert gnutar busybox bash curl ];
        created = "now";
        config = {
          WorkingDir = "/app";
          Cmd = [ "${imaginator}/bin/imaginator" ];
        };
      };
    in
    {
      checks = {
        #inherit imaginator;
        imaginator-clippy = craneLib.cargoClippy (commonArgs // {
          inherit src cargoArtifacts;
          cargoClippyExtraArgs = "--all-targets -- --deny warnings";
        });
        imaginator-doc = craneLib.cargoDoc (commonArgs // {
          inherit src cargoArtifacts;
        });
        imaginator-fmt = craneLib.cargoFmt {
          inherit (imaginatorDeps) pname version;
          inherit src;
        };
        imaginator-audit = craneLib.cargoAudit {
          inherit (imaginatorDeps) pname version;
          inherit src advisory-db;
        };
        imaginator-test = craneLib.cargoTest (commonArgs // {
          # disable logging from integration tests
          RUST_LOG = "NONE";
          doNotLinkInheritedArtifacts = true;
          inherit cargoArtifacts src;
        });

        imaginator-coverage = craneLib.cargoTarpaulin (commonArgs // {
          inherit src cargoArtifacts;
        });
      };

      packages = {
        default = imaginator;
        imaginator-update-plugins = imaginator-update-plugins;
        imaginator-e2e = imaginator-e2e;
        dockerImage = dockerImage;
      } // lib.optionalAttrs (system == "x86_64-linux") {
        integration = import ./integration.nix {
          makeTest = import (nixpkgs + "/nixos/tests/make-test-python.nix");
          inherit system;
          inherit pkgs;
          inherit imaginator;
          inherit self;
          inherit imaginator-e2e;
        };
        perf = import ./perf.nix {
          makeTest = import (nixpkgs + "/nixos/tests/make-test-python.nix");
          inherit system;
          inherit pkgs;
          inherit imaginator;
          inherit self;
          inherit imaginator-e2e;
          inherit lib;
        };
      };

      devShells.default = craneLib.devShell {
        LIBCLANG_PATH = "${ pkgs.llvmPackages.libclang.lib }/lib";

        # Inherit inputs from checks.
        checks = self.checks.${system};

        buildInputs = with pkgs; [
          systemd
          git-crypt
          git-cliff
          oha
          cargo-udeps
          cargo-audit
          cargo-edit
          openssl
          skopeo
        ];

        nativeBuildInputs = with pkgs; [ pkg-config imagemagick clang stdenv.cc.libc clang ];
      };

      apps.default = flake-utils.lib.mkApp {
        drv = imaginator;
      };
    }
  );
}
