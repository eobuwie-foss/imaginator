self:
{ config, pkgs, lib, ... }:
with lib; let
  cfg = config.services.imaginator;
  cleanupScript = pkgs.writeShellApplication {
    name = "cleanup";
    runtimeInputs = [ pkgs.coreutils ];
    text = ''
      ${builtins.concatStringsSep "\n" (map (cache: ''
        # test if file is older than ${toString cfg.refreshCacheIndicesOlderThan} date
        if [[ -f ${cache.dir}/index ]]; then
          index_date=$(date -r ${cache.dir}/index +%Y-%m-%d)
          if [[ "$index_date" < "${cfg.refreshCacheIndicesOlderThan}" ]]; then
            echo -e "\033[0;31mIndex for ${cache.dir} is older than ${cfg.refreshCacheIndicesOlderThan} ($index_date). Removing it.\033[0m"
            rm -f ${cache.dir}/index
          else
            echo -e "\033[0;32mIndex for ${cache.dir} is up to date.\033[0m"
          fi
        fi
      '') (attrValues cfg.caches))}
    '';
  };

  imaginator = self.outputs.packages.x86_64-linux.default;
  cacheSubmodule = { name, ... }: {
    options = {
      dir = mkOption {
        type = types.str;
        default = "/var/cache/imaginator/${name}";
      };

      size = mkOption {
        # TODO: Actually parse the value
        type = types.str;
        default = "1G";
      };

      read_only = mkOption {
        type = types.bool;
        default = false;
      };
    };
  };
  s3CacheSubmodule = { name, ... }: {
    options = {
      access_key = mkOption {
        type = types.str;
      };

      secret_key = mkOption {
        type = types.str;
      };

      bucket_name = mkOption {
        type = types.str;
      };

      endpoint = mkOption {
        type = types.str;
      };

      region = mkOption {
        type = types.str;
        default = "auto";
      };

      project_id = mkOption {
        type = types.str;
      };

      timeout = mkOption {
        type = types.str;
      };

      path_style_host = mkOption {
        type = types.bool;
        default = false;
      };
    };
  };

  filterSubmodule = {
    options = {
      name = mkOption {
        type = types.str;
      };

      args = mkOption {
        type = with types; anything;
      };
    };
  };

  filterToString = filter:
    let
      argsToStr = arg:
        if builtins.isAttrs arg then filterToString arg else toString arg;
    in
    "${filter.name}(${lib.concatStringsSep "," (map argsToStr filter.args)})";

  aliasToString = alias:
    if builtins.isList alias
    then lib.concatStringsSep ":" (map filterToString alias)
    else filterToString alias;

  configFile = pkgs.writeText "imaginator.yml" (
    builtins.toJSON (filterAttrs (n: v: n != "enable" && v != null) (cfg // {
      aliases = lib.mapAttrs (_: aliasToString) cfg.aliases;
    }))
  );

  filterAssertions = alias: { name, args }:
    if name == "download" then
      let url = head args; in {
        assertion = (lib.hasInfix ":" url) -> hasAttr (head (splitString ":" url)) cfg.domains;
        message = "Prefix ${head (splitString ":" (head args))} doesn't exist (in alias ${alias}).";
      } else if name == "cache" then {
      assertion = hasAttr (head args) cfg.caches;
      message = "Cache ${head args} doesn't exist (in alias ${alias}).";
    } else if name == "s3_cache" then {
      assertion = hasAttr (head args) cfg.s3_caches;
      message = "S3 Cache ${head args} doesn't exist (in alias ${alias}).";
    }
    else null;

  # Define the complex type for limitConnections
  limitConnectionsType = types.either (types.enum [ "disabled" "max_threads" "mac_cores" ])
    (types.submodule {
      options = {
        fixed = mkOption {
          type = types.int;
          description = "Fixed number of connections.";
        };
      };
    });

  # Validate the limitConnections setting
  validateLimitConnections = value:
    if isAttrs value && value.fixed < 0 then throw "The value for limit_connections.fixed must be a non-negative integer."
    else true;
in
{
  options.services.imaginator = {
    enable = mkEnableOption "imaginator";

    secret_dir = mkOption {
      type = types.str;
      default = "/var/secrets/";
    };

    allow_builtin_filters = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to allow using filters not defined in the `aliases` option";
    };

    loggerType = mkOption {
      type = types.str;
      default = "ECS";
    };
    logLevel = mkOption {
      type = types.str;
      default = "debug";
    };
    accept_invalid_tls_certs = mkOption {
      type = types.bool;
      default = false;
      description = "Whether to download images from hosts with invalid tls certificates";
    };

    log_filters_header = mkOption {
      type = types.nullOr types.str;
      default = null;
      description = "If present, the name of a response header that will contain the list of filters used to serve a request.";
    };

    caches = mkOption {
      type = types.attrsOf (types.submodule cacheSubmodule);
      default = { };
    };
    s3_caches = mkOption {
      type = types.attrsOf (types.submodule s3CacheSubmodule);
      default = { };
    };
    domains = mkOption {
      type = types.attrsOf types.str;
    };
    aliases = mkOption {
      type = types.attrsOf (types.listOf (types.submodule filterSubmodule));
      example = {
        download-and-cache = [
          { name = "download"; args = [ "prefix:{0}" ]; }
          { name = "cache"; args = [ "download" ]; }
        ];
      };
    };
    cleanupIndexes = mkOption {
      type = types.bool;
      default = false;
    };
    refreshCacheIndicesOlderThan = mkOption {
      type = types.strMatching "^[0-9]{4}-[0-9]{2}-[0-9]{2}$";
      description = "The date before which the cache indices should be refreshed (format: YYYY-MM-DD).";
      default = "2024-07-05";
    };
    limit_connections = mkOption {
      type = limitConnectionsType;
      default = "disabled";
      description = "Control the limit connections setting. 
          Can be either 'disabled', 'max_threads', 'mac_cores' or fixed number of connections.";
      example = 10;
      apply = value: if value == "enabled" then true else value;
    };
  };

  config = mkIf cfg.enable {
    assertions = flatten
      (mapAttrsToList
        (name: filters:
          filter (a: a != null) (map (filterAssertions name) filters)
        )
        cfg.aliases) ++ [
      {
        assertion = validateLimitConnections config.services.imaginator.limit_connections;
        message = "Invalid value for services.myService.limitConnections";
      }
    ];

    users.users.imaginator = {
      isSystemUser = true;
      group = "imaginator";
    };
    users.groups.imaginator = { };
    system.activationScripts.imaginator = {
      deps = [ "users" ];
      text = concatStringsSep "\n" (map (cache: "install -d -o imaginator -g imaginator ${cache.dir}") (attrValues cfg.caches));
    };

    systemd.services.imaginator = {
      serviceConfig = {
        ExecStart = "${imaginator}/bin/imaginator ${configFile}";
        ExecStartPre = if cfg.cleanupIndexes then "${cleanupScript}/bin/cleanup" else "";
        Type = "notify";
        User = "imaginator";
        Group = "imaginator";
        RestartSec = 5;
        Restart = "always";
        LoadCredential = [
          "sentry_dsn:${cfg.secret_dir}sentry_dsn"
        ];
        PermissionsStartOnly = true;
      };

      environment = {
        LOGGER_TYPE = cfg.loggerType;
        RUST_LOG = cfg.logLevel;
      };

      after = [ "network.target" ];
      wantedBy = [ "multi-user.target" ];
    };
  };
}
