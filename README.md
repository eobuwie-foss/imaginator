Imaginator
==========

Imaginator is a fast, safe web interface to imagemagick. While imaginator is relatively stable and is deployed in production at pixers, this document is a work in progress.

Environment
-----------

* `SENTRY_DSN` - optional sentry DSN
* `ENV` - `production` or `development`
* `CONFIG_PATH` - optional variable to set configuration file path, in most cases you should use first application parameter to set this path
* `HTTP_PORT` - optional server http port, default is 3000

Dependencies
------------

  * ImageMagick 7 (and whatever else [magick-rust](https://crates.io/crates/magick_rust) requires)
  * optional: cargo-script (run `cargo install cargo-script`)
  * optional: nix with flakes support enabled

Installation without nix
------------------------

First, install ImageMagick - magick_rust (and imaginator) needs it to automatically generate correct bindings.

Then update the list of plugins in the `plugin` directory by running `./update-plugins.rs` or by compiling imaginator-update-plugins from the `update-plugins` directory and running that. It's the same thing, just packaged differently.

Finally, build the binary using `cargo build --release`. Your binary will be in `target/release/imaginator`.
Note that it still depends on ImageMagick – you'll have to install the same version on the machine you want to run it at.

Installation with nix
---------------------

  1. If you changed the set of plugins, run `nix run .#imaginator-update-plugins`.
  2. Run `nix build` (or install the package, or just `nix run .` if you want to start imaginator immediately).
  3. Build docker image with `nix build -L .#dockerImage`

Usage
-----

Once started, imaginator listens on port 3000. Imaginator's API is rather simple, the only way to interact with it is to make a GET request to `/FILTER(arg1,arg2,...)`, where FILTER is a specification of what you want Imaginator to do. Because most of the filters will want to do something with the result of previous ones, there's a convenient way of chaining them: `/foo(arg1,arg2):bar(arg1,arg2)` – this is equivalent to `/bar(foo(arg1,arg2),arg1,arg2)`.

Filters can do a lot of things – and although there are none in the core crate, there are some filters in the imaginator-plugins-base crate.

For example, resizing an image can be done with the following query:

    curl http://127.0.0.1:3000/download(http://example.com/image.jpg):resize(0.5w,0.5h)

This will resize the image to 50% of its width, and 50% of its height, meaning that this will be translated to

    resize(download(http://example.com/image.jpeg),0.5w,0.5h)

For list of available operations check [this document](./plugins/base/README.md)

Bundled filters
---------------

`imaginator-plugins-base` contains the following filters.

### download

    /download(PREFIX:URL[, DPI])

Downloads an image from the network, and lazily decodes it.

Arguments:
  * PREFIX: A name of an origin, specified in the configuration.
  * URL: path to the image, will be concatenated to PREFIX's value.
  * DPI: If the file turns out to be a vector image, render it at a given dpi. Optional.

Configuration:
  * `domains`: A map from prefix names to url origins. Used to ensure that no requests to unauthorized domains are made. An example configuration of:

        domains:
            s3: 'https://some.s3.bucket/img/'
            local: 'http://assets.local/'

    would allow requests like `/download(s3:foo/bar.png)` that would download `https://some.s3.bucket/img/foo/bar.png`, or `/download(local:baz.svg)` that would download `http://assets.local/baz.svg`.
    If for some reason you need to allow all urls, the following configuration will work:

        domains:
          http: 'http://'
          https: 'https://'

    However, using this on internet-facing instances is discouraged for security reasons, as it allows anyone to have Imaginator make requests to arbitrary urls.

   * `image.supported_formats`: If the format isn't on this list, Imaginator will refuse to decode it. Currently available formats are: pdf, png, jpg, tiff, svg, ps (postscript).

         image:
            supported_formats: [jpg, png, svg]

   * `image.max_width`, `image.max_height`: Maximum width and height (in pixels) of an image this filter (and others) will work with. Imaginator will not try to decode larger images, to prevent easy DoS attacks.

         image:
            max_width: 1000
            max_height: 1000

### Resize

    /resize(IMAGE, WIDTH, HEIGHT)
    /download(...):resize(WIDTH, HEIGHT)

Resizes an image. Note that these are not two different forms, the second one is simply an example of the usual usage.

Arguments:
  * IMAGE: An image, usually a result of `download` or another filter.
  * WIDTH: The desired image width. If equal to 0, it will be inferred from height to preserve aspect ratio.
  * HEIGHT: The desired image height. If equal to 0, it will be inferred from width to preserve aspect ratio.

### Cache

    /cache(FILTER, NAME)

If FILTER is in cache called NAME, returns the cached copy. Otherwise runs the FILTER, then saves and returns the result.

Configuration:
  * `caches`: Every cache has a name, a path where it stores the images, and a maximum size in bytes it may use. When the cache is full, least recently used images are removed until a new one can fit.

        caches:
          download:
            dir: /var/cache/imaginator/download
            size: 10G

### S3 Cache

    /s3_cache(FILTER, NAME)

If FILTER is in cache called `NAME`, returns the cached copy, otherwise runs the FILTER, then saves to cache and returns the result.

Configuration
  * `s3_caches`: Every cache has a name, path to bucket and cloud provider with credentials. No limit or cache cycling method is implemented yet.




Example configuration for gcloud. For `access_key` and `secret_key` provide HMAC for service account that meet required permissions for writing and reading from Google Storage. Further information can be found here [Migrate from Amazon S3 to Cloud Storage](https://cloud.google.com/storage/docs/migrating)
```yaml
s3_caches:
  downloads:
    bucket_name: imaginator-test
    region: auto
    endpoint: https://storage.googleapis.com
    access_key:
    secret_key:
    project_id: trigger-prod
    timeout: 10s
```

    /s3_cache(FILTER, NAME, IMG_PATH)

If `IMG_PATCH` is present in cache (bucket) called `NAME`, then cached copy is returned. Double check for cache img by SHA copy is not being made.
Image Will be stored at IMG_PATH tho after executing `FILTER`.

Aliases
-------

In production deployments, it's often useful to have an alias for a pipeline, e.g. `/r(img:a.png, 200)` expanding to something like `/download(img:a.png):resize(200, 0):cache(result)`. This is useful both for shortening the urls, not exposing parameters that shouldn't be modified, and to control caching without changing the urls. Aliases can be specified in the configuration file:

    aliases:
      r: /download({0}):resize({1},0):cache(result)

Units
-----

Wherever you have to use pixel units, you can use a few others:

  * `px`: the default unit, pixels
  * `w`: percentage of the image's width. 0.5w is half of the images width, etc.
  * `h`: percentage of the image's height. 0.5h is half of the images height, etc.
  * `hcm`: centimeters, according to the horizontal resolution of the image
  * `vcm`: centimeters, according to the vertical resolution of the image
  * `hin`: inches, according to the horizontal resolution of the image
  * `vin`: inches, according to the vertical resolution of the image

Local testing
--------------

```bash
docker-compose up test
```

Performance tests
--------------

To run e2e performance tests run: `nix build .\#packages.x86_64-linux.perf -L` 
`result` directory will contain following results:

```
❯ tree result
result
├── cached.test             <- file system cached results
├── icon_s3.test            <- s3 cached results
└── not_cached.test         <- not cached filter

0 directories, 3 files
```


