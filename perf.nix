{ system
, pkgs
, makeTest
, self
, imaginator-e2e
, lib
, ...
}:
let
  # NixOS module shared between server and client
  sharedModule = {
    virtualisation = {
      # Since it's common for CI not to have $DISPLAY available, we have to explicitly tell the tests "please don't expect any screen available"
      graphics = false;
    };
  };
  filter = name: args: { inherit name args; };
  # Generic filter helpers
  download = prefix: filter "download" [ "${prefix}:{0}" ];
  cache = name: filter "cache" [ name ];
  s3Cache = name: filter "s3_cache" [ name ];
  format = filter "format" [ "{1}" ];
  core_count = 34;
in
makeTest
{
  name = "integration";
  nodes = {
    server = {

      networking.firewall.allowedTCPPorts = [ 9000 3000 8100 ];

      services.minio = {
        enable = true;
        region = "us-east-1";
      };

      environment.sessionVariables = {
        IMAGINATOR_URL = "http://127.0.0.1:3000";
      };

      environment.systemPackages = with pkgs; [
        oha
        minio-client
        imaginator-e2e
        fd
      ];


      systemd.services.secrets = {
        enable = true;
        wantedBy = [ "multi-user.target" ];
        after = [ "imaginator.target" ];
        path = [ pkgs.nix ];
        script = "
    mkdir -p /var/secrets/
    echo 'http://user@127.0.0.1:8000/19' > /var/secrets/sentry_dsn
    echo 'finished setting up credentials'
        ";
        serviceConfig = {
          Type = "oneshot";
          RemainAfterExit = true;
        };
      };

      virtualisation = {
        qemu.options = [
          "-net nic,model=virtio"
        ];
        cores = core_count;
        memorySize = 40 * 1024;
      };

      systemd.services.imaginator.environment = {
        MAGICK_THREAD_LIMIT = "1";
        #TOKIO_WORKER_THREADS = toString core_count;
      };


      imports = [ sharedModule self.nixosModules.imaginator ];

      services.imaginator = {
        enable = true;
        loggerType = "tracing";
        logLevel = "ERROR";
        log_filters_header = "filter";
        caches = {
          download = {
            dir = "/tmp/imaginator/download";
          };
          icon = {
            dir = "/tmp/imaginator/icon";
          };
        };

        s3_caches = {
          download = {
            access_key = "minioclient";
            bucket_name = "imaginator-cache";
            endpoint = "http://127.0.0.1:9000";
            project_id = "perf";
            region = "us-east-1";
            secret_key = "miniosecret";
            timeout = "3s";
            path_style_host = true;
          };
          icon = {
            access_key = "minioclient";
            bucket_name = "imaginator-cache";
            endpoint = "http://127.0.0.1:9000";
            project_id = "perf";
            region = "us-east-1";
            secret_key = "miniosecret";
            timeout = "3s";
            path_style_host = true;
          };
        };

        domains = {
          minio = "http://127.0.0.1:9000/images/";
        };

        aliases = {
          icon = [ (download "minio") (cache "download") (filter "resize" [ 0 500 ]) format (cache "icon") ];
          icon_not_cached = [ (download "minio") (filter "resize" [ 0 500 ]) format ];
          icon_s3 = [ (download "minio") (s3Cache "download") (filter "resize" [ 0 500 ]) format (cache "icon") (s3Cache "icon") ];
        };
      };
    };

  };


  # Here are some relevant resources for writing integration tests in nix
  # https://nix.dev/tutorials/integration-testing-using-virtual-machines
  # https://nixos.org/manual/nixos/stable/#sec-nixos-tests
  testScript = ''
    server.start()

    server.wait_for_unit("minio.service")
    server.wait_for_open_port(9000)


    server.succeed("mc alias set minio http://127.0.0.1:9000 minioadmin minioadmin")
    server.succeed("mc admin info minio")

    # create a public bucket with images
    server.succeed("mc mb minio/images")
    server.succeed("mc anonymous set download minio/images")

    server.succeed("mc cp ${examples/s1}/* minio/images")
    server.succeed("mc ls --recursive minio/images")

    # create a cache bucket
    server.succeed("mc mb minio/imaginator-cache")
    server.succeed("mc admin user add minio minioclient miniosecret")

    server.succeed("mc admin policy attach minio readwrite --user=minioclient")

    server.wait_for_unit("imaginator.service")

    server.wait_for_open_port(3000)
    
    server.succeed("check_image 'icon(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg' false")
    server.succeed("check_image 'icon_s3(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg' false")
    
    # warmup
    server.succeed("oha -z 10s 'http://127.0.0.1:3000/icon(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg'")
    server.succeed("oha -z 60s 'http://127.0.0.1:3000/icon(pexels-skylar-kang-6045293.jpg,jpg)/from_cache.jpg'  > cached.test")
    server.copy_from_vm("cached.test", "")

    server.succeed("oha -c ${toString core_count} -z 60s 'http://127.0.0.1:3000/icon_not_cached(pexels-skylar-kang-6045293.jpg,jpg)/not_from_cache.jpg'  > not_cached.test")
    server.copy_from_vm("not_cached.test", "")

    server.succeed("oha -z 60s 'http://127.0.0.1:3000/icon_s3(pexels-skylar-kang-6045293.jpg,jpg)/not_from_cache.jpg'  > icon_s3.test")
    server.copy_from_vm("icon_s3.test", "")
  '';
}
{
  inherit pkgs;
  inherit (pkgs) system;
}
