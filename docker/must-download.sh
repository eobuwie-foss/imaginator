set -x

must_succeed() {
  $*
  STATUS=$?
  if [[ ${STATUS} == 0 ]]; then
    echo "OK"
  else
    echo "Failed with code ${STATUS}"
    exit $STATUS
  fi
}

must_fail() {
  $*
  STATUS=$?
  if [[ ${STATUS} == 0 ]]; then
    echo Expect to failed but succed
    exit 1
  else
    echo "OK"
  fi
}

HOST=${IMAGINATOR_HOST:-http://0.0.0.0:3000}

echo "Checking download"

must_succeed curl -f ${HOST}/download\(tor:pexels-chokniti-khongchum-2280547.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-chokniti-khongchum-2280547.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-chokniti-khongchum-2280549.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-chokniti-khongchum-2280549.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-cottonbro-studio-3997379.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-cottonbro-studio-3997379.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-cottonbro-studio-4614203.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-cottonbro-studio-4614203.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-cottonbro-studio-4614204.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-cottonbro-studio-4614204.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-cottonbro-studio-6583367.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-cottonbro-studio-6583367.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-karolina-grabowska-4021773.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-karolina-grabowska-4021773.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-madison-inouye-1684880.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-madison-inouye-1684880.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-michael-burrows-7148060.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-michael-burrows-7148060.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-monstera-7691344.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-monstera-7691344.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-pixabay-34299.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-pixabay-34299.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-pixabay-159045.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-pixabay-159045.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-pixabay-276267.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-pixabay-276267.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-rfstudio-3825527.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-rfstudio-3825527.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-rfstudio-3825578.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-rfstudio-3825578.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-skylar-kang-6045258.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-skylar-kang-6045258.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(tor:pexels-skylar-kang-6045293.jpg\) --output /tmp/x
must_succeed diff ./examples/s1/pexels-skylar-kang-6045293.jpg /tmp/x

must_succeed curl -f ${HOST}/download\(uta:pexels-alexandra-maria-318236.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-alexandra-maria-318236.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-alexandra-maria-336372.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-alexandra-maria-336372.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-apostolos-vamvouras-2285500.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-apostolos-vamvouras-2285500.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-ingo-joseph-609771.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-ingo-joseph-609771.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-justus-menke-5214139.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-justus-menke-5214139.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-lukas-292999.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-lukas-292999.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-mnz-1598505.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-mnz-1598505.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-pixabay-267202.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-pixabay-267202.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-pixabay-267301.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-pixabay-267301.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-pixabay-267320.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-pixabay-267320.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-ray-piedra-1478442.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-ray-piedra-1478442.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-scott-webb-137603.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-scott-webb-137603.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-web-donut-19090.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-web-donut-19090.jpg /tmp/x
must_succeed curl -f ${HOST}/download\(uta:pexels-zain-ali-1027130.jpg\) --output /tmp/x
must_succeed diff ./examples/s2/pexels-zain-ali-1027130.jpg /tmp/x

must_fail curl -f ${HOST}/download\(uta:pexels-skylar-kang-6045293.jpg\) --output /tmp/x
must_fail curl -f ${HOST}/download\(tor:pexels-zain-ali-1027130.jpg\) --output /tmp/x
must_fail curl -f ${HOST}/download\(mer:pexels-zain-ali-1027130.jpg\) --output /tmp/x
must_fail curl -f ${HOST}/download\(cis:pexels-zain-ali-1027130.jpg\) --output /tmp/x

must_succeed curl -f ${HOST}/cache\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),downloads\) --output /tmp/x
must_succeed curl -f ${HOST}/cache\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),results\) --output /tmp/x

must_succeed curl -f ${HOST}/cache\(resize\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),50,100\),downloads\) --output /tmp/x
must_succeed diff /tmp/x ./examples/expected/img50x100.jpg

must_succeed curl -f ${HOST}/cache\(resize\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),500,100\),downloads\) --output /tmp/x
must_succeed diff /tmp/x ./examples/expected/img500x100.jpg

must_succeed curl -f ${HOST}/s3_cache\(resize\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),500,100\),downloads\) --output /tmp/x
must_succeed diff /tmp/x ./examples/expected/img500x100.jpg

must_succeed curl -f ${HOST}/s3_cache\(resize\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),500,100\),downloads\) --output /tmp/x
must_succeed diff /tmp/x ./examples/expected/img500x100.jpg

must_succeed curl -f ${HOST}/format\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),png\) --output /tmp/x
must_succeed curl -f ${HOST}/format\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),webp\) --output /tmp/x
must_fail curl -f ${HOST}/format\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),pg\) --output /tmp/x

echo "perf simple download"
must_succeed siege -r 20 -c 30 -v ${HOST}/download\(uta:pexels-zain-ali-1027130.jpg\)

echo "preparing s3 cache"
must_succeed curl ${HOST}/s3_cache\(format\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),png\),downloads\) --output /tmp/x

echo "perf s3 cached transformation"
must_succeed siege -r 20 -c 30 -v ${HOST}/s3_cache\(format\(download\(tor:pexels-chokniti-khongchum-2280547.jpg\),png\),downloads\)
